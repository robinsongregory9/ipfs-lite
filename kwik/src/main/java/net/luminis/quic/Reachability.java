package net.luminis.quic;

public enum Reachability {
    LOCAL, NONE, GLOBAL
}
