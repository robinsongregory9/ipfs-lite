/*
 * Copyright © 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Kwik, an implementation of the QUIC protocol in Java.
 *
 * Kwik is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Kwik is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.luminis.quic.stream;

import static net.luminis.quic.QuicConstants.TransportErrorCode.STREAM_LIMIT_ERROR;

import net.luminis.LogUtils;
import net.luminis.quic.EncryptionLevel;
import net.luminis.quic.ImplementationError;
import net.luminis.quic.QuicConnection;
import net.luminis.quic.QuicStream;
import net.luminis.quic.RawStreamData;
import net.luminis.quic.Role;
import net.luminis.quic.TransportError;
import net.luminis.quic.Version;
import net.luminis.quic.frame.MaxStreamsFrame;
import net.luminis.quic.frame.QuicFrame;
import net.luminis.quic.frame.ResetStreamFrame;
import net.luminis.quic.frame.StopSendingFrame;
import net.luminis.quic.frame.StreamFrame;
import net.luminis.quic.send.Sender;

import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Manages QUIC streams.
 * Note that Kwik cannot handle more than 2147483647 (<code>Integer.MAX_INT</code>) streams in one connection.
 */
public class StreamManager {
    private static final String TAG = StreamManager.class.getSimpleName();
    private final Map<Integer, QuicStreamImpl> streams = new ConcurrentHashMap<>();
    private final Version version;
    private final Sender sender;
    private final Role role;
    private final Semaphore openBidirectionalStreams;
    private final Semaphore openUnidirectionalStreams;
    private final AtomicInteger maxOpenStreamIdUni;
    private final AtomicInteger maxOpenStreamIdBidi;
    private final AtomicBoolean maxOpenStreamsUniUpdateQueued = new AtomicBoolean(false);
    private final AtomicBoolean maxOpenStreamsBidiUpdateQueued = new AtomicBoolean(false);
    private final AtomicInteger nextStreamId = new AtomicInteger(0);
    private final AtomicLong maxStreamsAcceptedByPeerBidi = new AtomicLong(0);
    private final AtomicLong maxStreamsAcceptedByPeerUni = new AtomicLong(0);
    private final FlowControl flowController;
    private final QuicConnection connection;
    private final Function<QuicStream, Consumer<RawStreamData>> streamDataConsumer;

    public StreamManager(QuicConnection connection, Version version, Sender sender,
                         FlowControl flowController,
                         Role role, int maxOpenStreamsUni, int maxOpenStreamsBidi,
                         Function<QuicStream, Consumer<RawStreamData>> streamDataConsumer) {
        this.connection = connection;
        this.version = version;
        this.sender = sender;
        this.flowController = flowController;
        this.role = role;
        this.maxOpenStreamIdUni = new AtomicInteger(computeMaxStreamId(
                maxOpenStreamsUni, role.other(), false));
        this.maxOpenStreamIdBidi = new AtomicInteger(computeMaxStreamId(
                maxOpenStreamsBidi, role.other(), true));
        openBidirectionalStreams = new Semaphore(0);
        openUnidirectionalStreams = new Semaphore(0);
        this.streamDataConsumer = streamDataConsumer;
    }

    private int computeMaxStreamId(int maxStreams, Role peerRole, boolean bidirectional) {
        // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-4.6
        // "Only streams with a stream ID less than (max_stream * 4 + initial_stream_id_for_type) can be opened; "
        // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-2.1
        //  | 0x0  | Client-Initiated, Bidirectional  |
        int maxStreamId = maxStreams * 4;
        //  | 0x1  | Server-Initiated, Bidirectional  |
        if (peerRole == Role.Server && bidirectional) {
            maxStreamId += 1;
        }
        //  | 0x2  | Client-Initiated, Unidirectional |
        if (peerRole == Role.Client && !bidirectional) {
            maxStreamId += 2;
        }
        //  | 0x3  | Server-Initiated, Unidirectional |
        if (peerRole == Role.Client && bidirectional) {
            maxStreamId += 3;
        }
        return maxStreamId;
    }


    public CompletableFuture<QuicStream> createStream(Consumer<RawStreamData> streamDataConsumer,
                                                      boolean bidirectional, long timeout, TimeUnit timeoutUnit) {
        return createStream(streamDataConsumer, bidirectional, timeout, timeoutUnit,
                QuicStreamImpl::new);
    }

    private CompletableFuture<QuicStream> createStream(Consumer<RawStreamData> streamDataConsumer, boolean bidirectional,
                                                       long timeout, TimeUnit unit, QuicStreamSupplier streamFactory) {
        CompletableFuture<QuicStream> future = new CompletableFuture<>();
        boolean acquired;
        try {
            if (bidirectional) {
                acquired = openBidirectionalStreams.tryAcquire(timeout, unit);
            } else {
                acquired = openUnidirectionalStreams.tryAcquire(timeout, unit);
            }
            if (!acquired) {
                future.completeExceptionally(new TimeoutException());
                return future;
            }
        } catch (InterruptedException e) {
            future.completeExceptionally(e);
            return future;
        }

        int streamId = generateStreamId(bidirectional);
        QuicStreamImpl stream = streamFactory.apply(connection, version, streamId, sender,
                flowController, streamDataConsumer);
        streams.put(streamId, stream);
        future.complete(stream);
        return future;
    }

    /**
     * Creates a quic stream that is able to send early data.
     * Note that this method will not block; if the stream cannot be created due to no stream credit, null is returned.
     */
    public EarlyDataStream createEarlyDataStream(Consumer<RawStreamData> streamDataConsumer, boolean bidirectional) {
        // todo make a CompletableFuture return
        try {
            return (EarlyDataStream) createStream(streamDataConsumer, bidirectional, 0, TimeUnit.MILLISECONDS,
                    EarlyDataStream::new).get();
        } catch (InterruptedException | ExecutionException e) {
            LogUtils.error(TAG, e);
            return null;
        }
    }

    private int generateStreamId(boolean bidirectional) {
        // https://tools.ietf.org/html/draft-ietf-quic-transport-17#section-2.1:
        // "0x0  | Client-Initiated, Bidirectional"
        // "0x1  | Server-Initiated, Bidirectional"
        int id = (nextStreamId.getAndIncrement() << 2) + (role == Role.Client ? 0x00 : 0x01);
        if (!bidirectional) {
            // "0x2  | Client-Initiated, Unidirectional |"
            // "0x3  | Server-Initiated, Unidirectional |"
            id += 0x02;
        }
        return id;
    }


    public void process(StreamFrame frame) throws TransportError {
        int streamId = frame.getStreamId();
        QuicStreamImpl stream = streams.get(streamId);
        if (stream != null) {
            stream.add(frame);
            // This implementation maintains a fixed maximum number of open streams, so when the peer closes a stream
            // it is allowed to open another.
            if (frame.isFinal() && isPeerInitiated(streamId)) {
                increaseMaxOpenStreams(streamId);
            }
        } else {
            if (isPeerInitiated(streamId)) {
                if (isUni(streamId) && streamId < maxOpenStreamIdUni.get() ||
                        isBidi(streamId) && streamId < maxOpenStreamIdBidi.get()) {
                    stream = new QuicStreamImpl(connection, version, streamId, sender,
                            flowController, streamDataConsumer.apply(stream));
                    streams.put(streamId, stream);
                    stream.add(frame);
                    if (frame.isFinal()) {
                        increaseMaxOpenStreams(streamId);
                    }
                } else {
                    // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-19.11
                    // "An endpoint MUST terminate a connection with a STREAM_LIMIT_ERROR error if a peer opens more
                    //  streams than was permitted."
                    throw new TransportError(STREAM_LIMIT_ERROR);
                }
            } else {
                LogUtils.error(TAG, "Receiving frame for non-existent stream " + streamId);
            }
        }
    }

    public void process(StopSendingFrame stopSendingFrame) {

        // https://www.rfc-editor.org/rfc/rfc9000.html#name-solicited-state-transitions
        // "A STOP_SENDING frame requests that the receiving endpoint send a RESET_STREAM frame."
        QuicStreamImpl stream = streams.get(stopSendingFrame.getStreamId());
        if (stream != null) {
            // "An endpoint SHOULD copy the error code from the STOP_SENDING frame to the RESET_STREAM frame it sends, ..."
            stream.stopSendingStream(stopSendingFrame.getErrorCode());
        }
    }

    public void process(ResetStreamFrame resetStreamFrame) {

        QuicStreamImpl stream = streams.get(resetStreamFrame.getStreamId()); // todo remove
        if (stream != null) {
            // "An endpoint SHOULD copy the error code from the STOP_SENDING frame to the RESET_STREAM frame it sends, ..."
            stream.terminateStream(resetStreamFrame.getErrorCode(), resetStreamFrame.getFinalSize());
        }
    }

    private void increaseMaxOpenStreams(int streamId) {

        if (isUni(streamId)) {
            maxOpenStreamIdUni.getAndAdd(4);
            if (!maxOpenStreamsUniUpdateQueued.getAndSet(true)) {
                sender.send(this::createMaxStreamsUpdateUni, 9,
                        EncryptionLevel.App, this::retransmitMaxStreams);  // Flush not necessary, as this method is called while processing received message.
            }
        } else {
            maxOpenStreamIdBidi.getAndAdd(4);
            if (!maxOpenStreamsBidiUpdateQueued.getAndSet(true)) {
                sender.send(this::createMaxStreamsUpdateBidi, 9,
                        EncryptionLevel.App, this::retransmitMaxStreams);  // Flush not necessary, as this method is called while processing received message.
            }
        }

    }

    private QuicFrame createMaxStreamsUpdateUni(int maxSize) {
        if (maxSize < 9) {
            throw new ImplementationError();
        }
        maxOpenStreamsUniUpdateQueued.set(false);
        // largest streamId < maxStreamId; e.g. client initiated: max-id = 6, server initiated: max-id = 7 => max streams = 1,
        return new MaxStreamsFrame(maxOpenStreamIdUni.get() / 4, false);
    }

    private QuicFrame createMaxStreamsUpdateBidi(int maxSize) {
        if (maxSize < 9) {
            throw new ImplementationError();
        }
        maxOpenStreamsBidiUpdateQueued.set(false);

        // largest streamId < maxStreamId; e.g. client initiated: max-id = 4, server initiated: max-id = 5 => max streams = 1,
        return new MaxStreamsFrame(maxOpenStreamIdBidi.get() / 4, true);
    }

    void retransmitMaxStreams(QuicFrame frame) {
        MaxStreamsFrame lostFrame = ((MaxStreamsFrame) frame);
        if (lostFrame.isAppliesToBidirectional()) {
            sender.send(createMaxStreamsUpdateBidi(Integer.MAX_VALUE),
                    EncryptionLevel.App, this::retransmitMaxStreams);
        } else {
            sender.send(createMaxStreamsUpdateUni(Integer.MAX_VALUE),
                    EncryptionLevel.App, this::retransmitMaxStreams);
        }
    }

    private boolean isPeerInitiated(int streamId) {
        return streamId % 2 == (role == Role.Client ? 1 : 0);
    }

    private boolean isUni(int streamId) {
        return streamId % 4 > 1;
    }

    private boolean isBidi(int streamId) {
        return streamId % 4 < 2;
    }

    public void process(MaxStreamsFrame frame) {
        if (frame.isAppliesToBidirectional()) {
            long streamsAcceptedByPeerBidi = maxStreamsAcceptedByPeerBidi.get();
            if (frame.getMaxStreams() > streamsAcceptedByPeerBidi) {
                int increment = (int) (frame.getMaxStreams() - streamsAcceptedByPeerBidi);
                LogUtils.debug(TAG, "increased max bidirectional streams with " +
                        increment + " to " + frame.getMaxStreams());
                maxStreamsAcceptedByPeerBidi.set(frame.getMaxStreams());
                openBidirectionalStreams.release(increment);
            }
        } else {
            long streamsAcceptedByPeerUni = maxStreamsAcceptedByPeerUni.get();
            if (frame.getMaxStreams() > streamsAcceptedByPeerUni) {
                int increment = (int) (frame.getMaxStreams() - streamsAcceptedByPeerUni);
                LogUtils.debug(TAG, "increased max unidirectional streams with " +
                        increment + " to " + frame.getMaxStreams());
                maxStreamsAcceptedByPeerUni.set(frame.getMaxStreams());
                openUnidirectionalStreams.release(increment);
            }
        }
    }

    /**
     * Set initial max bidirectional streams that the peer will accept.
     */
    public void setInitialMaxStreamsBidi(long initialMaxStreamsBidi) {
        if (initialMaxStreamsBidi >= maxStreamsAcceptedByPeerBidi.get()) {
            LogUtils.debug(TAG, "Initial max bidirectional stream: " + initialMaxStreamsBidi);
            maxStreamsAcceptedByPeerBidi.set(initialMaxStreamsBidi);
            if (initialMaxStreamsBidi > Integer.MAX_VALUE) {
                LogUtils.error(TAG, "Server initial max streams bidirectional is larger " +
                        "than supported; limiting to " + Integer.MAX_VALUE);
                initialMaxStreamsBidi = Integer.MAX_VALUE;
            }
            openBidirectionalStreams.release((int) initialMaxStreamsBidi);
        } else {
            LogUtils.error(TAG, "Attempt to reduce value of initial_max_streams_bidi from "
                    + maxStreamsAcceptedByPeerBidi + " to " + initialMaxStreamsBidi + "; ignoring.");
        }
    }

    /**
     * Set initial max unidirectional streams that the peer will accept.
     */
    public void setInitialMaxStreamsUni(long initialMaxStreamsUni) {
        if (initialMaxStreamsUni >= maxStreamsAcceptedByPeerUni.get()) {
            LogUtils.debug(TAG, "Initial max unidirectional stream: " + initialMaxStreamsUni);
            maxStreamsAcceptedByPeerUni.set(initialMaxStreamsUni);
            if (initialMaxStreamsUni > Integer.MAX_VALUE) {
                LogUtils.error(TAG, "Server initial max streams unidirectional is " +
                        "larger than supported; limiting to " + Integer.MAX_VALUE);
                initialMaxStreamsUni = Integer.MAX_VALUE;
            }
            openUnidirectionalStreams.release((int) initialMaxStreamsUni);
        } else {
            LogUtils.error(TAG, "Attempt to reduce value of initial_max_streams_uni from "
                    + maxStreamsAcceptedByPeerUni + " to " + initialMaxStreamsUni + "; ignoring.");
        }
    }

    public long getMaxBidirectionalStreams() {
        return maxStreamsAcceptedByPeerBidi.get();
    }

    public long getMaxUnirectionalStreams() {
        return maxStreamsAcceptedByPeerUni.get();
    }

    public void abortAll() {
        streams.values().forEach(QuicStreamImpl::terminate);
    }

    interface QuicStreamSupplier {
        QuicStreamImpl apply(QuicConnection connection, Version quicVersion, int streamId, Sender sender,
                             FlowControl flowController, Consumer<RawStreamData> streamDataConsumer);
    }
}

