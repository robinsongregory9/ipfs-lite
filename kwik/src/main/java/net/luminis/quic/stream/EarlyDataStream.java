/*
 * Copyright © 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Kwik, an implementation of the QUIC protocol in Java.
 *
 * Kwik is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Kwik is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.luminis.quic.stream;

import androidx.annotation.NonNull;

import net.luminis.LogUtils;
import net.luminis.quic.QuicConnection;
import net.luminis.quic.RawStreamData;
import net.luminis.quic.Version;
import net.luminis.quic.send.Sender;

import java.util.Arrays;
import java.util.Locale;
import java.util.function.Consumer;


/**
 * A quic stream that is capable of sending early data. When early data is offered but cannot be send as early data,
 * the data will be cached until it can be send.
 */
public class EarlyDataStream extends QuicStreamImpl {
    private static final String TAG = EarlyDataStream.class.getSimpleName();
    private boolean earlyDataIsFinalInStream;
    private byte[] earlyData = new byte[0];
    private byte[] remainingData = new byte[0];

    public EarlyDataStream(QuicConnection connection, Version quicVersion, int streamId, Sender sender,
                           FlowControl flowController, @NonNull Consumer<RawStreamData> streamDataConsumer) {
        super(connection, quicVersion, streamId, sender, flowController, streamDataConsumer);
    }

    /**
     * Write early data, keeping flow control limits into account. Data that cannot be sent as 0-rtt will be stored
     * for sending in 1-rtt packets (see writeRemaining).
     */
    public void writeEarlyData(byte[] earlyData, boolean fin, long earlyDataSizeLeft) {
        this.earlyData = earlyData;
        earlyDataIsFinalInStream = fin;
        long flowControlLimit = flowController.getFlowControlLimit(streamId);
        int earlyDataLength = (int) Long.min(earlyData.length, Long.min(earlyDataSizeLeft, flowControlLimit));
        if (earlyDataLength > 0) {
            LogUtils.info(TAG, String.format(Locale.US, "Sending %d bytes of early data on %s", earlyDataLength, this));
        } else {
            LogUtils.info(TAG, "Sending no early data because: fc limit is " + flowControlLimit + "; early data size left is " + earlyDataSizeLeft + " and early data length is " + earlyData.length);
        }

        writeOutput(earlyData);
        if (earlyDataLength == earlyData.length && earlyDataIsFinalInStream) {
            closeOutput();
        }
        remainingData = Arrays.copyOfRange(earlyData, earlyDataLength, earlyData.length);
    }

    public void writeRemaining(boolean earlyDataWasAccepted) {
        if (earlyDataWasAccepted) {
            if (remainingData.length > 0) {
                writeOutput(remainingData);
                closeOutput();
            }
        } else {
            // TODO reconsider creating new QuicStream object, or fix resetOutputStream to make it thread safe.
            // Also consider to pass encryption level in that constructor to get rit of getEncryptionLevel
            resetOutputStream();
            writeOutput(earlyData);
            if (earlyDataIsFinalInStream) {
                closeOutput();
            }
        }
    }

}

