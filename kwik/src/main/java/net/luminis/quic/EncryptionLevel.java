/*
 * Copyright © 2019, 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Kwik, an implementation of the QUIC protocol in Java.
 *
 * Kwik is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Kwik is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.luminis.quic;

import androidx.annotation.NonNull;

import java.util.List;
import java.util.Optional;

/**
 * https://tools.ietf.org/html/draft-ietf-quic-tls-29#section2.1
 * "Data is protected using a number of encryption levels:
 * Initial Keys
 * Early Data (0-RTT) Keys
 * Handshake Keys
 * Application Data (1-RTT) Keys"
 * <p>
 * https://tools.ietf.org/html/draft-ietf-quic-transport-29#section-12.2
 * "...order of increasing encryption levels (Initial, 0-RTT, Handshake, 1-RTT...)"
 */
public enum EncryptionLevel {

    Initial,
    ZeroRTT,
    Handshake,
    App;

    private static final List<EncryptionLevel> cached = List.of(EncryptionLevel.values());

    public static List<EncryptionLevel> encryptionLevels() {
        return cached;
    }

    public boolean higher(EncryptionLevel other) {
        return this.ordinal() > other.ordinal();
    }

    @NonNull
    public PnSpace relatedPnSpace() {
        switch (this) {
            case ZeroRTT:
            case App:
                return PnSpace.App;
            case Initial:
                return PnSpace.Initial;
            case Handshake:
                return PnSpace.Handshake;
            default:
                throw new RuntimeException("relatedPnSpace not defined");
        }
    }

    public Optional<EncryptionLevel> next() {
        switch (this) {
            case ZeroRTT:
                return Optional.of(Initial);
            case Initial:
                return Optional.of(Handshake);
            case Handshake:
                return Optional.of(App);
            case App:
                return Optional.empty();
            default:
                throw new ImplementationError();
        }
    }
}
