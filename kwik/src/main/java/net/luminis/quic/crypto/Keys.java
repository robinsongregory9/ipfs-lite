/*
 * Copyright © 2019, 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Kwik, an implementation of the QUIC protocol in Java.
 *
 * Kwik is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Kwik is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.luminis.quic.crypto;

import static net.luminis.quic.Role.Client;
import static net.luminis.quic.Role.Server;

import android.annotation.SuppressLint;

import net.luminis.LogUtils;
import net.luminis.quic.DecryptionException;
import net.luminis.quic.Role;
import net.luminis.tls.TrafficSecrets;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.crypto.AEADBadTagException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import at.favre.lib.crypto.HKDF;

public class Keys {

    private static final String TAG = Keys.class.getSimpleName();
    private final Role nodeRole;
    private final AtomicInteger keyUpdateCounter = new AtomicInteger(0);
    private final AtomicBoolean possibleKeyUpdateInProgresss = new AtomicBoolean(false);
    private final ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    private final AtomicReference<Keys> peerKeys = new AtomicReference<>();
    protected byte[] writeKey;
    protected byte[] newKey;
    protected byte[] writeIV;
    protected byte[] newIV;
    protected byte[] hp;
    private byte[] trafficSecret;
    private byte[] newApplicationTrafficSecret;

    public Keys(Role nodeRole) {
        this.nodeRole = nodeRole;
    }

    public Keys(byte[] initialSecret, Role nodeRole) {
        this.nodeRole = nodeRole;


        byte[] initialNodeSecret = hkdfExpandLabel(initialSecret,
                nodeRole == Client ? "client in" : "server in", (short) 32);

        computeKeys(initialNodeSecret, true, true);
    }

    // See https://tools.ietf.org/html/rfc8446#section-7.1 for definition of HKDF-Expand-Label.
    static byte[] hkdfExpandLabel(byte[] secret, String label, short length) {

        byte[] prefix;
        // https://tools.ietf.org/html/draft-ietf-quic-tls-17#section-5.1:
        // "The keys used for packet protection are computed from the TLS secrets using the KDF provided by TLS."
        prefix = "tls13 ".getBytes(StandardCharsets.ISO_8859_1);

        ByteBuffer hkdfLabel = ByteBuffer.allocate(2 + 1 + prefix.length +
                label.getBytes(StandardCharsets.ISO_8859_1).length + 1 +
                "".getBytes(StandardCharsets.ISO_8859_1).length);
        hkdfLabel.putShort(length);
        hkdfLabel.put((byte) (prefix.length + label.getBytes().length));
        hkdfLabel.put(prefix);
        hkdfLabel.put(label.getBytes(StandardCharsets.ISO_8859_1));
        hkdfLabel.put((byte) ("".getBytes(StandardCharsets.ISO_8859_1).length));
        hkdfLabel.put("".getBytes(StandardCharsets.ISO_8859_1));
        HKDF hkdf = HKDF.fromHmacSha256();
        return hkdf.expand(secret, hkdfLabel.array(), length);
    }

    public void computeZeroRttKeys(TrafficSecrets secrets) {
        readWriteLock.writeLock().lock();
        try {
            byte[] earlySecret = secrets.getClientEarlyTrafficSecret();
            computeKeys(earlySecret, true, true);
        } finally {
            readWriteLock.writeLock().unlock();
        }
    }

    public void computeHandshakeKeys(TrafficSecrets secrets) {
        readWriteLock.writeLock().lock();
        try {
            if (nodeRole == Client) {
                trafficSecret = secrets.getClientHandshakeTrafficSecret();
                computeKeys(trafficSecret, true, true);
            }
            if (nodeRole == Server) {
                trafficSecret = secrets.getServerHandshakeTrafficSecret();
                computeKeys(trafficSecret, true, true);
            }
        } finally {
            readWriteLock.writeLock().unlock();
        }
    }

    public void computeApplicationKeys(TrafficSecrets secrets) {
        readWriteLock.writeLock().lock();
        try {
            if (nodeRole == Client) {
                trafficSecret = secrets.getClientApplicationTrafficSecret();
                computeKeys(trafficSecret, true, true);
            }
            if (nodeRole == Server) {
                trafficSecret = secrets.getServerApplicationTrafficSecret();
                computeKeys(trafficSecret, true, true);
            }
        } finally {
            readWriteLock.writeLock().unlock();
        }
    }

    /**
     * Compute new keys. Note that depending on the role of this Keys object, computing new keys concerns updating
     * the write secrets (role that initiates the key update) or the read secrets (role that responds to the key update).
     *
     * @param selfInitiated true when this role initiated the key update, so updating write secrets.
     */
    public void computeKeyUpdate(boolean selfInitiated) {
        readWriteLock.writeLock().lock();
        try {
            newApplicationTrafficSecret = hkdfExpandLabel(getTrafficSecret(),
                    "quic ku", (short) 32);

            computeKeys(newApplicationTrafficSecret, false, selfInitiated);
            if (selfInitiated) {
                // If updating this Keys object was self initiated, the new keys can be installed immediately.
                trafficSecret = newApplicationTrafficSecret;
                keyUpdateCounter.incrementAndGet();
                newApplicationTrafficSecret = null;
            }
            // Else, updating this Keys object was initiated by receiving a packet with different key phase, and the new keys
            // can only be installed permanently if the decryption of the packet (that introduced the new key phase) has succeeded.
        } finally {
            readWriteLock.writeLock().unlock();
        }
    }

    /**
     * Confirm that, if a key update was in progress, it has been successful and thus the new keys can (and should) be
     * used for decrypting all incoming packets.
     */
    public void confirmKeyUpdateIfInProgress() {
        if (possibleKeyUpdateInProgresss.compareAndSet(true, false)) {
            readWriteLock.writeLock().lock();
            try {
                trafficSecret = newApplicationTrafficSecret;
                writeKey = newKey;
                writeIV = newIV;
                keyUpdateCounter.incrementAndGet();
                newApplicationTrafficSecret = null;
                newKey = null;
                newIV = null;
                checkPeerKeys();
            } finally {
                readWriteLock.writeLock().unlock();
            }
        }
    }

    /**
     * In case keys are updated, check if the peer keys are already updated too (which depends on who initiated the
     * key update).
     */
    private void checkPeerKeys() {
        Keys peerKeysCwnd = peerKeys.get();
        if (peerKeysCwnd != null) {
            if (peerKeysCwnd.keyUpdateCounter.get() < keyUpdateCounter.get()) {
                peerKeysCwnd.computeKeyUpdate(true);
            }
        }
    }

    /**
     * Confirm that, if a key update was in progress, it has been unsuccessful and thus the new keys should not be
     * used for decrypting all incoming packets.
     */
    public void cancelKeyUpdateIfInProgress() {
        if (possibleKeyUpdateInProgresss.compareAndSet(true, false)) {
            readWriteLock.writeLock().lock();
            try {
                newApplicationTrafficSecret = null;
                newKey = null;
                newIV = null;
            } finally {
                readWriteLock.writeLock().unlock();
            }
        }
    }

    private void computeKeys(byte[] secret, boolean includeHP, boolean replaceKeys) {

        String prefix;
        // https://tools.ietf.org/html/draft-ietf-quic-tls-17#section-5.1
        // "The current encryption level secret and the label "quic key" are
        //   input to the KDF to produce the AEAD key; the label "quic iv" is used
        //   to derive the IV, see Section 5.3.  The header protection key uses
        //   the "quic hp" label, see Section 5.4).  Using these labels provides
        //   key separation between QUIC and TLS, see Section 9.4."
        prefix = "quic ";

        // https://tools.ietf.org/html/rfc8446#section-7.3
        byte[] key = hkdfExpandLabel(secret, prefix + "key", getKeyLength());
        if (replaceKeys) {
            writeKey = key;
        } else {
            newKey = key;
        }

        byte[] iv = hkdfExpandLabel(secret, prefix + "iv", (short) 12);
        if (replaceKeys) {
            writeIV = iv;
        } else {
            newIV = iv;
        }

        if (includeHP) {
            // https://tools.ietf.org/html/draft-ietf-quic-tls-17#section-5.1
            // "The header protection key uses the "quic hp" label"
            hp = hkdfExpandLabel(secret, prefix + "hp", getKeyLength());
        }
    }

    protected short getKeyLength() {
        return 16;
    }

    public byte[] getTrafficSecret() {
        readWriteLock.readLock().lock();
        try {
            return trafficSecret;
        } finally {
            readWriteLock.readLock().unlock();
        }
    }

    public byte[] getWriteKey() {
        readWriteLock.readLock().lock();
        try {
            if (possibleKeyUpdateInProgresss.get()) {
                return newKey;
            }
            return writeKey;
        } finally {
            readWriteLock.readLock().unlock();
        }
    }

    public byte[] getWriteIV() {
        readWriteLock.readLock().lock();
        try {
            if (possibleKeyUpdateInProgresss.get()) {
                return newIV;
            }
            return writeIV;
        } finally {
            readWriteLock.readLock().unlock();
        }
    }

    public byte[] getHp() {
        readWriteLock.readLock().lock();
        try {
            return hp;
        } finally {
            readWriteLock.readLock().unlock();
        }
    }

    @SuppressLint("GetInstance")
    private Cipher getHeaderProtectionCipher() {
        Cipher hpCipher;

        try {
            // https://tools.ietf.org/html/draft-ietf-quic-tls-27#section-5.4.3
            // "AEAD_AES_128_GCM and AEAD_AES_128_CCM use 128-bit AES [AES] in electronic code-book (ECB) mode."
            hpCipher = Cipher.getInstance("AES/ECB/NoPadding");
            SecretKeySpec keySpec = new SecretKeySpec(getHp(), "AES");
            hpCipher.init(Cipher.ENCRYPT_MODE, keySpec);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            // Inappropriate runtime environment
            throw new RuntimeException(e);
        } catch (InvalidKeyException e) {
            // Programming error
            throw new RuntimeException();
        }

        return hpCipher;
    }

    public byte[] aeadEncrypt(byte[] associatedData, byte[] message, byte[] nonce) {
        try {
            SecretKeySpec secretKey = new SecretKeySpec(getWriteKey(), "AES");
            String AES_GCM_NOPADDING = "AES/GCM/NoPadding";
            Cipher aeadCipher = Cipher.getInstance(AES_GCM_NOPADDING);
            GCMParameterSpec parameterSpec = new GCMParameterSpec(128, nonce);   // https://tools.ietf.org/html/rfc5116#section-5.3: "the tag length t is 16"
            aeadCipher.init(Cipher.ENCRYPT_MODE, secretKey, parameterSpec);
            aeadCipher.updateAAD(associatedData);
            return aeadCipher.doFinal(message);
        } catch (InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException
                | BadPaddingException | NoSuchAlgorithmException | NoSuchPaddingException e) {
            // Programming error
            throw new RuntimeException(e);
        }
    }

    public byte[] aeadDecrypt(byte[] associatedData, byte[] message, byte[] nonce) throws DecryptionException {
        if (message.length <= 16) {
            // https://www.rfc-editor.org/rfc/rfc9001.html#name-aead-usage
            // "These cipher suites have a 16-byte authentication tag and produce an output 16 bytes larger than their input."
            throw new DecryptionException("ciphertext must be longer than 16 bytes");
        }
        try {
            SecretKeySpec secretKey = new SecretKeySpec(getWriteKey(), "AES");
            String AES_GCM_NOPADDING = "AES/GCM/NoPadding";
            Cipher aeadCipher = Cipher.getInstance(AES_GCM_NOPADDING);
            GCMParameterSpec parameterSpec = new GCMParameterSpec(128, nonce);   // https://tools.ietf.org/html/rfc5116#section-5.3: "the tag length t is 16"
            aeadCipher.init(Cipher.DECRYPT_MODE, secretKey, parameterSpec);
            aeadCipher.updateAAD(associatedData);
            return aeadCipher.doFinal(message);
        } catch (AEADBadTagException | NoSuchPaddingException | NoSuchAlgorithmException decryptError) {
            throw new DecryptionException();
        } catch (InvalidKeyException | InvalidAlgorithmParameterException |
                IllegalBlockSizeException | BadPaddingException e) {
            // Programming error
            LogUtils.error(TAG, e.getClass().getSimpleName());
            throw new RuntimeException();
        }
    }

    public byte[] createHeaderProtectionMask(byte[] sample) {
        try {
            Cipher hpCipher = getHeaderProtectionCipher();
            return hpCipher.doFinal(sample);
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            // Programming error
            throw new RuntimeException();
        }
    }

    public short getKeyPhase() {
        return (short) (keyUpdateCounter.get() % 2);
    }

    /**
     * Check whether the key phase carried by a received packet still matches the current key phase; if not, compute
     * new keys (to be used for decryption). Note that the changed key phase can also be caused by packet corruption,
     * so it is not yet sure whether a key update is really in progress (this will be sure when decryption of the packet
     * failed or succeeded).
     */
    public void checkKeyPhase(short keyPhaseBit) {
        if (getKeyPhase() != keyPhaseBit) {
            if (newKey == null) {
                computeKeyUpdate(false);

            }
            possibleKeyUpdateInProgresss.set(true);
        }
    }

    void setPeerKeys(Keys peerKeys) {
        this.peerKeys.set(peerKeys);
    }
}
