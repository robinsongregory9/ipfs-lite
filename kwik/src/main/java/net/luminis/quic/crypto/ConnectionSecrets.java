/*
 * Copyright © 2019, 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Kwik, an implementation of the QUIC protocol in Java.
 *
 * Kwik is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Kwik is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.luminis.quic.crypto;

import net.luminis.quic.EncryptionLevel;
import net.luminis.quic.Role;
import net.luminis.quic.Version;
import net.luminis.tls.CipherSuite;
import net.luminis.tls.TrafficSecrets;

import java.util.concurrent.locks.ReentrantReadWriteLock;

import at.favre.lib.crypto.HKDF;

public class ConnectionSecrets {

    // https://tools.ietf.org/html/draft-ietf-quic-tls-29#section-5.2
    public static final byte[] STATIC_SALT_DRAFT_29 = new byte[]{
            (byte) 0xaf, (byte) 0xbf, (byte) 0xec, (byte) 0x28, (byte) 0x99, (byte) 0x93, (byte) 0xd2, (byte) 0x4c,
            (byte) 0x9e, (byte) 0x97, (byte) 0x86, (byte) 0xf1, (byte) 0x9c, (byte) 0x61, (byte) 0x11, (byte) 0xe0,
            (byte) 0x43, (byte) 0x90, (byte) 0xa8, (byte) 0x99};
    public static final byte[] STATIC_SALT_V1 = new byte[]{
            (byte) 0x38, (byte) 0x76, (byte) 0x2c, (byte) 0xf7, (byte) 0xf5, (byte) 0x59, (byte) 0x34, (byte) 0xb3,
            (byte) 0x4d, (byte) 0x17, (byte) 0x9a, (byte) 0xe6, (byte) 0xa4, (byte) 0xc8, (byte) 0x0c, (byte) 0xad,
            (byte) 0xcc, (byte) 0xbb, (byte) 0x7f, (byte) 0x0a};

    private final Version quicVersion;
    private final Role ownRole;

    private final Keys[] clientSecrets = new Keys[EncryptionLevel.encryptionLevels().size()];
    private final Keys[] serverSecrets = new Keys[EncryptionLevel.encryptionLevels().size()];
    private final ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();

    public ConnectionSecrets(Version quicVersion, Role role) {
        this.quicVersion = quicVersion;
        this.ownRole = role;
    }

    /**
     * Generate the initial secrets
     */
    public void computeInitialKeys(byte[] destConnectionId) {

        // From https://tools.ietf.org/html/draft-ietf-quic-tls-16#section-5.2:
        // "The hash function for HKDF when deriving initial secrets and keys is SHA-256"
        HKDF hkdf = HKDF.fromHmacSha256();

        byte[] initialSalt = quicVersion == Version.QUIC_version_1 ? STATIC_SALT_V1 : STATIC_SALT_DRAFT_29;
        byte[] initialSecret = hkdf.extract(initialSalt, destConnectionId);

        readWriteLock.writeLock().lock();
        try {
            clientSecrets[EncryptionLevel.Initial.ordinal()] = new Keys(initialSecret, Role.Client);
            serverSecrets[EncryptionLevel.Initial.ordinal()] = new Keys(initialSecret, Role.Server);
        } finally {
            readWriteLock.writeLock().unlock();
        }
    }

    public void computeEarlySecrets(TrafficSecrets secrets) {
        readWriteLock.writeLock().lock();
        try {
            Keys zeroRttSecrets = new Keys(Role.Client);
            zeroRttSecrets.computeZeroRttKeys(secrets);
            clientSecrets[EncryptionLevel.ZeroRTT.ordinal()] = zeroRttSecrets;
        } finally {
            readWriteLock.writeLock().unlock();
        }
    }

    private void createKeys(EncryptionLevel level, CipherSuite selectedCipherSuite) {
        Keys clientHandshakeSecrets;
        Keys serverHandshakeSecrets;
        if (selectedCipherSuite == CipherSuite.TLS_AES_128_GCM_SHA256) {
            clientHandshakeSecrets = new Keys(Role.Client);
            serverHandshakeSecrets = new Keys(Role.Server);
        } else {
            throw new IllegalStateException("unsupported cipher suite " + selectedCipherSuite);
        }
        clientSecrets[level.ordinal()] = clientHandshakeSecrets;
        serverSecrets[level.ordinal()] = serverHandshakeSecrets;

        // Keys for peer and keys for self must be able to signal each other of a key update.
        clientHandshakeSecrets.setPeerKeys(serverHandshakeSecrets);
        serverHandshakeSecrets.setPeerKeys(clientHandshakeSecrets);
    }

    public void computeHandshakeSecrets(TrafficSecrets secrets,
                                        CipherSuite selectedCipherSuite) {
        readWriteLock.writeLock().lock();
        try {
            createKeys(EncryptionLevel.Handshake, selectedCipherSuite);

            clientSecrets[EncryptionLevel.Handshake.ordinal()].computeHandshakeKeys(secrets);
            serverSecrets[EncryptionLevel.Handshake.ordinal()].computeHandshakeKeys(secrets);
        } finally {
            readWriteLock.writeLock().unlock();
        }

    }

    public void computeApplicationSecrets(TrafficSecrets secrets,
                                          CipherSuite selectedCipherSuite) {
        readWriteLock.writeLock().lock();
        try {
            createKeys(EncryptionLevel.App, selectedCipherSuite);

            clientSecrets[EncryptionLevel.App.ordinal()].computeApplicationKeys(secrets);
            serverSecrets[EncryptionLevel.App.ordinal()].computeApplicationKeys(secrets);
        } finally {
            readWriteLock.writeLock().unlock();
        }

    }

    public Keys getClientSecrets(EncryptionLevel encryptionLevel) {
        readWriteLock.readLock().lock();
        try {
            return clientSecrets[encryptionLevel.ordinal()];
        } finally {
            readWriteLock.readLock().unlock();
        }
    }

    public Keys getServerSecrets(EncryptionLevel encryptionLevel) {
        readWriteLock.readLock().lock();
        try {
            return serverSecrets[encryptionLevel.ordinal()];
        } finally {
            readWriteLock.readLock().unlock();
        }
    }

    public Keys getPeerSecrets(EncryptionLevel encryptionLevel) {
        readWriteLock.readLock().lock();
        try {

            return (ownRole == Role.Client) ? serverSecrets[encryptionLevel.ordinal()] :
                    clientSecrets[encryptionLevel.ordinal()];
        } finally {
            readWriteLock.readLock().unlock();
        }
    }

    public Keys getOwnSecrets(EncryptionLevel encryptionLevel) {
        readWriteLock.readLock().lock();
        try {
            return (ownRole == Role.Client) ? clientSecrets[encryptionLevel.ordinal()] :
                    serverSecrets[encryptionLevel.ordinal()];
        } finally {
            readWriteLock.readLock().unlock();
        }
    }


}
