/*
 * Copyright © 2021, 2022 Peter Doornbosch
 *
 * This file is part of Kwik, an implementation of the QUIC protocol in Java.
 *
 * Kwik is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Kwik is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.luminis.quic.server;

import androidx.annotation.NonNull;

import net.luminis.LogUtils;
import net.luminis.quic.DecryptionException;
import net.luminis.quic.InvalidPacketException;
import net.luminis.quic.QuicStream;
import net.luminis.quic.RawStreamData;
import net.luminis.quic.Role;
import net.luminis.quic.Version;
import net.luminis.quic.crypto.ConnectionSecrets;
import net.luminis.quic.crypto.Keys;
import net.luminis.quic.packet.InitialPacket;
import net.luminis.tls.handshake.TlsServerEngineFactory;
import net.luminis.tls.util.ByteUtils;

import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.time.Instant;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * A server connection candidate: whether an initial packet causes a new server connection to be created cannot be
 * decided until the initial packet has been successfully parsed and decrypted (to avoid that corrupt packets change
 * server state).
 */
public class ServerConnectionCandidate implements ServerConnectionProxy {
    private static final String TAG = ServerConnectionCandidate.class.getSimpleName();

    private final Version quicVersion;
    private final InetSocketAddress clientAddress;
    private final byte[] dcid;
    private final ServerConnectionRegistry connectionRegistry;
    private final int connectionIdLength;
    private final TlsServerEngineFactory tlsServerEngineFactory;
    private final ApplicationProtocolRegistry applicationProtocolRegistry;
    private final DatagramSocket serverSocket;
    private final int initalRtt;
    private final Consumer<ServerConnectionImpl> closeCallback;
    private final boolean requireRetry;
    private final AtomicReference<ServerConnectionImpl> registeredConnection
            = new AtomicReference<>();
    private final Function<QuicStream, Consumer<RawStreamData>> streamDataConsumer;


    public ServerConnectionCandidate(
            int connectionIdLength, DatagramSocket serverSocket,
            TlsServerEngineFactory tlsServerEngineFactory, boolean requireRetry,
            ApplicationProtocolRegistry applicationProtocolRegistry, int initalRtt,
            ServerConnectionRegistry connectionRegistry,
            Consumer<ServerConnectionImpl> closeCallback,
            Function<QuicStream, Consumer<RawStreamData>> streamDataConsumer,
            Version version, InetSocketAddress clientAddress, byte[] dcid) {

        if (connectionIdLength > 20 || connectionIdLength < 0) {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-17.2
            // "In QUIC version 1, this value MUST NOT exceed 20 bytes"
            throw new IllegalArgumentException();
        }
        this.tlsServerEngineFactory = tlsServerEngineFactory;
        this.requireRetry = requireRetry;
        this.applicationProtocolRegistry = applicationProtocolRegistry;
        this.connectionIdLength = connectionIdLength;
        this.connectionRegistry = connectionRegistry;
        this.closeCallback = closeCallback;
        this.streamDataConsumer = streamDataConsumer;
        this.serverSocket = serverSocket;
        this.initalRtt = initalRtt;
        this.quicVersion = version;
        this.clientAddress = clientAddress;
        this.dcid = dcid;
    }

    @Override
    public byte[] getOriginalDestinationConnectionId() {
        return dcid;
    }

    @Override
    public void parsePackets(Instant timeReceived, ByteBuffer data) {
        // Execute packet parsing on separate thread, to make this method return a.s.a.p.
        ServerConnectionImpl conn = registeredConnection.get();
        if (conn != null) {
            conn.parsePackets(timeReceived, data);
            return;
        }

        try {
            InitialPacket initialPacket = parseInitialPacket(data);

            LogUtils.verbose(TAG, "Parsed packet with size " +
                    data.position() + "; " + data.remaining() + " bytes left.");

            // Packet is valid. This is the moment to create a real
            // server connection and continue processing.
            data.rewind();
            createAndRegisterServerConnection(initialPacket, timeReceived, data);
        } catch (InvalidPacketException | DecryptionException cannotParsePacket) {
            // Drop packet without any action (i.e. do not send anything; do not change state; avoid unnecessary processing)
            LogUtils.error(TAG, "Dropped invalid initial packet (no connection created)");
            connectionRegistry.deregisterConnection(this, dcid);
        } catch (Exception error) {
            LogUtils.error(TAG, "error while parsing or processing initial packet", error);
            connectionRegistry.deregisterConnection(this, dcid);
        }

    }

    private void createAndRegisterServerConnection(InitialPacket initialPacket,
                                                   Instant timeReceived,
                                                   ByteBuffer data) {
        Version quicVersion = initialPacket.getVersion();
        byte[] originalDcid = initialPacket.getDestinationConnectionId();
        ServerConnectionImpl conn = createNewConnection(quicVersion,
                clientAddress, initialPacket.getSourceConnectionId(), originalDcid,
                streamDataConsumer);
        registeredConnection.set(conn);

        LogUtils.info(TAG, "Creating new connection with version " + quicVersion
                + " for odcid " + ByteUtils.bytesToHex(originalDcid)
                + " with " + clientAddress.getAddress().getHostAddress() + ": "
                + ByteUtils.bytesToHex(conn.getInitialConnectionId()));

        // Pass the initial packet for processing, so it is processed on the server thread
        // (enabling thread confinement concurrency strategy)
        conn.parseAndProcessPackets(timeReceived, data, initialPacket);

        // Register new connection with the new connection id (the one generated by the server)
        connectionRegistry.registerConnection(conn, conn.getInitialConnectionId());
    }

    @Override
    public boolean isClosed() {
        return false;
    }

    @Override
    public void terminate() {
        // todo
    }

    /**
     * Creates new server connection.
     *
     * @param version       quic version used
     * @param clientAddress the address of the client
     * @param scid          the source connection id used by the client
     * @param originalDcid  the original destination id used by the client
     */
    public ServerConnectionImpl createNewConnection(
            Version version, InetSocketAddress clientAddress, byte[] scid, byte[] originalDcid,
            Function<QuicStream, Consumer<RawStreamData>> streamDataConsumer) {

        return new ServerConnectionImpl(version, serverSocket, clientAddress, scid, originalDcid,
                connectionIdLength, tlsServerEngineFactory, requireRetry,
                applicationProtocolRegistry, initalRtt, connectionRegistry,
                closeCallback, streamDataConsumer);
    }

    private InitialPacket parseInitialPacket(ByteBuffer data)
            throws InvalidPacketException, DecryptionException {
        // https://tools.ietf.org/html/draft-ietf-quic-transport-34#section-14.1
        // "A server MUST discard an Initial packet that is carried in a UDP datagram with a payload that is smaller than
        //  the smallest allowedmaximum datagram size of 1200 bytes."
        if (data.limit() < 1200) {
            throw new InvalidPacketException("Initial packets is carried in a datagram that is smaller than 1200 (" + data.limit() + ")");
        }
        // Note that the caller already has extracted connection id's from the raw data, so checking for minimal length is not necessary here.
        int flags = data.get();
        data.rewind();

        if ((flags & 0x40) != 0x40) {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-34#section-17.2
            // "Fixed Bit:  The next bit (0x40) of byte 0 is set to 1, unless the packet is a Version Negotiation packet.
            //  Packets containing a zero value for this bit are not valid packets in this version and MUST be discarded."
            throw new InvalidPacketException();
        }

        // https://tools.ietf.org/html/draft-ietf-quic-transport-34#section-17.2
        // "The most significant bit (0x80) of byte 0 (the first byte) is set to 1 for long headers."
        // https://tools.ietf.org/html/draft-ietf-quic-transport-34#section-17.2.2
        // "An Initial packet uses long headers with a type value of 0x0."
        if ((flags & 0xf0) == 0xc0) {  // 1100 0000
            InitialPacket packet = new InitialPacket(quicVersion);
            ConnectionSecrets connectionSecrets = new ConnectionSecrets(quicVersion, Role.Server);
            connectionSecrets.computeInitialKeys(dcid);
            Keys keys = connectionSecrets.getPeerSecrets(packet.getEncryptionLevel());
            packet.parse(data, keys, 0, 0);
            return packet;
        }
        throw new InvalidPacketException();
    }

    @NonNull
    @Override
    public String toString() {
        return "ServerConnectionCandidate[" + ByteUtils.bytesToHex(dcid) + "]";
    }
}
