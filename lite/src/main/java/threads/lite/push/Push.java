package threads.lite.push;


import threads.lite.core.Connection;

public class Push {
    private final Connection connection;
    private final String data;

    public Push(Connection connection, String data) {
        this.connection = connection;
        this.data = data;
    }

    public Connection getConnection() {
        return connection;
    }

    public String getData() {
        return data;
    }
}
