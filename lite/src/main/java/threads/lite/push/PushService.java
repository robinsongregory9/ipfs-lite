package threads.lite.push;

import androidx.annotation.NonNull;

import net.luminis.quic.QuicStream;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.core.Connection;
import threads.lite.core.Stream;
import threads.lite.core.StreamHandler;
import threads.lite.core.Transport;
import threads.lite.host.LiteTransport;
import threads.lite.utils.DataHandler;

public class PushService {

    private static final String TAG = PushService.class.getSimpleName();

    public static CompletableFuture<Void> notify(@NonNull Connection conn, @NonNull String content) {

        CompletableFuture<Void> done = new CompletableFuture<>();

        conn.createStream(new StreamHandler() {
            @Override
            public void throwable(Stream stream, Throwable throwable) {
                done.completeExceptionally(throwable);
            }

            @Override
            public void token(Stream stream, String token) throws Exception {
                if (!Arrays.asList(IPFS.STREAM_PROTOCOL, IPFS.PUSH_PROTOCOL).contains(token)) {
                    throw new Exception("Token " + token + " not supported");
                }
                if (Objects.equals(token, IPFS.PUSH_PROTOCOL)) {
                    stream.writeOutput(DataHandler.encode(content.getBytes()))
                            .thenApply(Stream::closeOutput);
                }
            }

            @Override
            public void fin() {
                done.complete(null);
            }

            @Override
            public void data(Stream stream, ByteBuffer data) {
                LogUtils.error(TAG, "data notify invoked");
            }

            @NonNull
            @Override
            public Transport getTransport(QuicStream quicStream) {
                return new LiteTransport();
            }

            @Override
            public void streamTerminated(QuicStream quicStream) {
                LogUtils.error(TAG, "todo stream terminated");
            }
        }).thenApply(quicStream -> quicStream.writeOutput(
                DataHandler.encodeTokens(IPFS.STREAM_PROTOCOL, IPFS.PUSH_PROTOCOL)));
        return done;
    }
}
