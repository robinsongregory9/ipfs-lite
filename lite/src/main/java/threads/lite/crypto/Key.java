package threads.lite.crypto;

import androidx.annotation.NonNull;

import crypto.pb.Crypto;

public interface Key {

    static PubKey unmarshalPublicKey(byte[] data) throws Exception {

        Crypto.PublicKey pms = Crypto.PublicKey.parseFrom(data);

        byte[] pubKeyData = pms.getData().toByteArray();

        switch (pms.getType()) {
            case RSA:
                return Rsa.unmarshalRsaPublicKey(pubKeyData);
            case ECDSA:
                return Ecdsa.unmarshalEcdsaPublicKey(pubKeyData);
            case Secp256k1:
                return Secp256k1.unmarshalSecp256k1PublicKey(pubKeyData);
            case Ed25519:
                return Ed25519.unmarshalEd25519PublicKey(pubKeyData);
            default:
                throw new Exception("BadKeyTypeException");
        }
    }

    @NonNull
    byte[] raw();

}


