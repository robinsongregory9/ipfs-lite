package threads.lite.holepunch;


import androidx.annotation.NonNull;

import java.net.ConnectException;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import threads.lite.IPFS;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.core.Connection;
import threads.lite.host.LiteConnection;
import threads.lite.host.LiteHandler;
import threads.lite.host.Session;
import threads.lite.ident.IdentityService;
import threads.lite.utils.DataHandler;
import threads.lite.utils.ExceptionHandler;


public class HolePunchService {

    @NonNull
    public static CompletableFuture<Connection> directConnect(
            @NonNull Session session, @NonNull Multiaddr relayAddress, @NonNull PeerId peerId,
            int timeout, int maxIdleTimeoutInSeconds, int initialMaxStreams,
            int initialMaxStreamData) throws Exception {

        CompletableFuture<Connection> done = new CompletableFuture<>();
        if (Objects.equals(peerId, session.getHost().self())) {
            done.completeExceptionally(new Exception("No connection to yourself"));
            return done;
        }

        // todo someday a completable will be returned
        LiteConnection conn = session.getHost().dial(new ExceptionHandler(), relayAddress,
                true, IPFS.CONNECT_TIMEOUT, IPFS.GRACE_PERIOD,
                0, IPFS.MESSAGE_SIZE_MAX);


        IdentityService.getPeerInfo(conn).whenComplete((peerInfo, throwable) -> {
            if (throwable != null) {
                conn.close();
                done.completeExceptionally(throwable);
            } else {
                if (!peerInfo.hasRelayHop()) {
                    conn.close();
                    done.completeExceptionally(new Exception("No relay hop protocol"));
                    return;
                }
                Multiaddr observed = peerInfo.getObserved();

                if (observed == null) {
                    conn.close();
                    done.completeExceptionally(new Exception("No observed address"));
                    return;
                }

                getDirectConnection(session, conn, observed, peerId, timeout,
                        maxIdleTimeoutInSeconds, initialMaxStreams, initialMaxStreamData)
                        .whenComplete((connection, throwable12) -> {
                            if (throwable12 != null) {
                                conn.close();
                                done.completeExceptionally(throwable12);
                            } else {
                                conn.close();
                                done.complete(connection);
                            }
                        });
            }

        });
        return done;
    }


    @NonNull
    public static CompletableFuture<Connection> getDirectConnection(
            @NonNull Session session, @NonNull Connection conn, @NonNull Multiaddr observed,
            @NonNull PeerId peerId, int timeout, int maxIdleTimeoutInSeconds,
            int initialMaxStreams, int initialMaxStreamData) {


        CompletableFuture<Connection> done = new CompletableFuture<>();

        try {
            // spec: https://github.com/libp2p/specs/blob/master/relay/DCUtR.md
            HolePunchHandler holePunchHandler = new HolePunchHandler(session, peerId,
                    observed, timeout, maxIdleTimeoutInSeconds, initialMaxStreams,
                    initialMaxStreamData, done);

            conn.createStream(holePunchHandler).whenComplete((stream, throwable) -> {
                if (throwable != null) {
                    done.completeExceptionally(throwable);
                } else {
                    stream.writeOutput(DataHandler.encodeTokens(IPFS.STREAM_PROTOCOL, IPFS.RELAY_PROTOCOL_HOP));
                }
            });

        } catch (Throwable throwable) {
            done.completeExceptionally(throwable);
        }

        return done;

    }


    @NonNull
    public static CompletableFuture<Connection> connect(
            @NonNull Session session, @NonNull Set<Multiaddr> multiaddrs,
            int timeout, int maxIdleTimeoutInSeconds,
            int initialMaxStreams, int initialMaxStreamData) {

        CompletableFuture<Connection> done = new CompletableFuture<>();

        if (multiaddrs.isEmpty()) {
            done.completeExceptionally(new ConnectException("no addresses left"));
            return done;
        }

        ExecutorService executor = Executors.newFixedThreadPool(
                Runtime.getRuntime().availableProcessors());
        AtomicInteger counter = new AtomicInteger(0);
        for (Multiaddr address : multiaddrs) {
            counter.incrementAndGet();
            executor.execute(() -> {
                try {
                    LiteConnection conn = session.getHost().dial(
                            new LiteHandler(session),
                            address, true,
                            timeout, maxIdleTimeoutInSeconds, initialMaxStreams,
                            initialMaxStreamData);
                    done.complete(conn);
                    executor.shutdownNow();
                } catch (Throwable throwable) {
                    if (counter.decrementAndGet() == 0) {
                        if (!done.isDone()) {
                            done.completeExceptionally(throwable);
                        }
                    }
                }
            });

        }
        executor.shutdown();
        return done;
    }
}
