package threads.lite.holepunch;

import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;
import com.southernstorm.noise.protocol.CipherStatePair;

import net.luminis.quic.QuicStream;

import java.nio.ByteBuffer;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

import circuit.pb.Circuit;
import holepunch.pb.Holepunch;
import identify.pb.IdentifyOuterClass;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.core.Connection;
import threads.lite.core.Stream;
import threads.lite.core.StreamHandler;
import threads.lite.core.Transport;
import threads.lite.host.LiteStream;
import threads.lite.host.LiteTransport;
import threads.lite.host.Session;
import threads.lite.mplex.Mplex;
import threads.lite.mplex.MuxFlag;
import threads.lite.mplex.MuxedStream;
import threads.lite.mplex.MuxedTransport;
import threads.lite.noise.Handshake;
import threads.lite.noise.Noise;
import threads.lite.noise.SecuredStream;
import threads.lite.noise.SecuredTransport;
import threads.lite.utils.DataHandler;

public class HolePunchHandler implements StreamHandler {
    private static final String TAG = HolePunchHandler.class.getSimpleName();
    private static final String TOKEN = "TOKEN";
    private static final String TRANSPORT = "TRANSPORT";
    private static final String TIMER = "TIMER";


    private final Set<String> protocols = Set.of(IPFS.STREAM_PROTOCOL, IPFS.RELAY_PROTOCOL_HOP,
            IPFS.IDENTITY_PROTOCOL, IPFS.NOISE_PROTOCOL, IPFS.MPLEX_PROTOCOL,
            IPFS.HOLE_PUNCH_PROTOCOL);

    private final Session session;
    private final Noise.NoiseState noiseInitiator;

    private final CompletableFuture<Connection> done;
    private final PeerId peerId;
    private final Multiaddr observed;
    private final int timeout;
    private final int maxIdleTimeoutInSeconds;
    private final int initialMaxStreams;
    private final int initialMaxStreamData;

    public HolePunchHandler(Session session, PeerId peerId, Multiaddr observed,
                            int timeout, int maxIdleTimeoutInSeconds,
                            int initialMaxStreams, int initialMaxStreamData,
                            CompletableFuture<Connection> done) throws NoSuchAlgorithmException {
        this.session = session;
        this.peerId = peerId;
        this.observed = observed;
        this.timeout = timeout;
        this.maxIdleTimeoutInSeconds = maxIdleTimeoutInSeconds;
        this.initialMaxStreams = initialMaxStreams;
        this.initialMaxStreamData = initialMaxStreamData;
        this.done = done;
        this.noiseInitiator = session.getHost().getNoise().getInitiator(peerId,
                session.getHost().getKeypair());

    }

    @Override
    public void throwable(Stream stream, Throwable throwable) {
        LogUtils.error(TAG, throwable);
        stream.getConnection().close();
        done.completeExceptionally(throwable);
    }

    @Override
    public void token(Stream stream, String token) throws Exception {

        LogUtils.error(TAG, "Token " + token +
                " streamId " + stream.getStreamId() + " initiator " + stream.isInitiator());
        stream.setAttribute(TOKEN, token);
        switch (token) {
            case IPFS.STREAM_PROTOCOL: {
                break;
            }
            case IPFS.RELAY_PROTOCOL_HOP: {

                Circuit.Peer dest = Circuit.Peer.newBuilder()
                        .setId(ByteString.copyFrom(peerId.getBytes()))
                        .build();

                Circuit.HopMessage message = Circuit.HopMessage.newBuilder()
                        .setType(Circuit.HopMessage.Type.CONNECT)
                        .setPeer(dest)
                        .build();
                stream.writeOutput(DataHandler.encode(message));
                break;
            }
            case IPFS.NOISE_PROTOCOL: {

                stream.writeOutput(Noise.encodeNoiseMessage(
                        noiseInitiator.getInitalMessage()));
                stream.setAttribute(TRANSPORT, new Handshake());
                LogUtils.error(TAG, "Transport set to Handshake");
                break;
            }
            case IPFS.MPLEX_PROTOCOL: {

                // make sure that the stream is secured stream
                if (stream instanceof SecuredStream) {
                    SecuredStream securedStream = (SecuredStream) stream;

                    // finished connection
                    stream.setAttribute(TRANSPORT, new MuxedTransport(
                            securedStream.getSender(), securedStream.getReceiver()));

                    LogUtils.error(TAG, "Transport set to MuxedTransport");

                    initiateHolePunch(securedStream);
                } else {
                    throw new Exception("wrong stream");
                }
                break;
            }
            case IPFS.IDENTITY_PROTOCOL: {
                // make sure that I am the "not" initiator

                IdentifyOuterClass.Identify response =
                        session.getHost().createIdentity(protocols,
                                session.getHost().networkListenAddresses(),
                                stream.getConnection().getRemoteAddress());
                LogUtils.error(TAG, "Write Identity ");
                stream.writeOutput(DataHandler.encodeTokens(IPFS.STREAM_PROTOCOL, IPFS.IDENTITY_PROTOCOL));
                stream.writeOutput(DataHandler.encode(response)).thenApply(Stream::closeOutput);

                break;
            }
            case IPFS.HOLE_PUNCH_PROTOCOL:
                // First off, A sends a Connect message to B through the relay.
                // That Connect message contains the addresses of A.
                // A sends to B a Connect message containing its observed (and possibly
                // predicted) addresses from identify and starts a timer to measure RTT of
                // the relay connection.
                Holepunch.HolePunch.Builder builder = Holepunch.HolePunch.newBuilder()
                        .setType(Holepunch.HolePunch.Type.CONNECT);
                // Note: observed is A's observed address
                builder.addObsAddrs(ByteString.copyFrom(observed.getBytes()));
                LogUtils.error(TAG, "[A] observed address " + observed);
                Holepunch.HolePunch message = builder.build();
                stream.setAttribute(TIMER, System.currentTimeMillis());
                stream.writeOutput(DataHandler.encode(message));
                break;
            default:
                if (!stream.isInitiator()) {
                    stream.writeOutput(DataHandler.encodeTokens(IPFS.STREAM_PROTOCOL, IPFS.NA));
                } else {
                    throw new Exception("Token " + token + " not supported");
                }
        }
    }

    @Override
    public void fin() {
        // nothing to do here
    }

    @Override
    public void data(Stream stream, ByteBuffer data) throws Exception {

        String token = (String) stream.getAttribute(TOKEN);
        LogUtils.error(TAG, "data streamId " + stream.getStreamId() +
                " token " + token + " initiator " + stream.isInitiator());
        Objects.requireNonNull(token);

        switch (token) {
            case IPFS.HOLE_PUNCH_PROTOCOL: {

                //  A measures the time between sending its and receiving B's Connect message
                //  and thereby determines the round trip time between A and B
                //  (on the relayed connection).
                Long timer = (Long) stream.getAttribute(TIMER); // timer set earlier
                Objects.requireNonNull(timer, "Timer not set on stream");
                long rtt = System.currentTimeMillis() - timer;

                // A receives the Connect message from B
                Holepunch.HolePunch msg = Holepunch.HolePunch.parseFrom(data.array());

                LogUtils.error(TAG, "[B] Request took " + rtt);
                Objects.requireNonNull(msg);


                if (msg.getType() != Holepunch.HolePunch.Type.CONNECT) {
                    throw new Exception("no connect type");
                }

                Set<Multiaddr> multiaddrs = Multiaddr.create(
                        msg.getObsAddrsList(), false);

                if (multiaddrs.size() == 0) {
                    throw new Exception("no observed addresses");
                }

                // now A sends a Sync message to B
                msg = Holepunch.HolePunch.newBuilder().
                        setType(Holepunch.HolePunch.Type.SYNC).build();
                stream.writeOutput(DataHandler.encode(msg));


                // Next, A sends a Sync message to B on the relayed connection. Once sent out,
                // A waits for half the round trip time, then it dials B via the addresses
                // received in B's Connect. This is a direct dial not using the relayed connection.
                Thread.sleep(rtt / 2);

                HolePunchService.connect(session, multiaddrs, timeout, maxIdleTimeoutInSeconds,
                                initialMaxStreams, initialMaxStreamData)
                        .whenComplete((connection, throwable) -> {
                            if (throwable != null) {
                                stream.closeOutput();
                                stream.closeInput();
                                done.completeExceptionally(throwable);
                            } else {
                                done.complete(connection);
                            }
                        });
                break;
            }

            case IPFS.RELAY_PROTOCOL_HOP: {
                Circuit.HopMessage msg = Circuit.HopMessage.parseFrom(data.array());
                Objects.requireNonNull(msg);
                if (msg.getType() != Circuit.HopMessage.Type.STATUS) {
                    throw new Exception(msg.getType().name());
                }

                if (msg.getStatus() != Circuit.Status.OK) {
                    throw new Exception(msg.getStatus().name());
                }

                if (msg.hasLimit()) {
                    Circuit.Limit limit = msg.getLimit();
                    if (limit.hasData()) {
                        LogUtils.debug(TAG, "Relay Limit Data " + limit.getData());
                    }
                    if (limit.hasDuration()) {
                        LogUtils.debug(TAG, "Relay Limit Duration " +
                                limit.getDuration());
                    }
                }

                stream.writeOutput(DataHandler.encodeTokens(IPFS.STREAM_PROTOCOL, IPFS.NOISE_PROTOCOL));
                return;
            }
            case IPFS.NOISE_PROTOCOL: {

                Noise.Response response = noiseInitiator.handshake(data.array());

                byte[] msg = response.getMessage();
                if (msg != null) {
                    stream.writeOutput(Noise.encodeNoiseMessage(msg));
                }

                CipherStatePair cipherStatePair = response.getCipherStatePair();
                if (cipherStatePair != null) {
                    stream.setAttribute(TRANSPORT, new SecuredTransport(
                            cipherStatePair.getSender(), cipherStatePair.getReceiver()));

                    LogUtils.error(TAG, "Transport set to SecuredTransport");

                    if (stream instanceof SecuredStream ||
                            stream instanceof MuxedStream) {
                        throw new Exception("not excepted stream");
                    }
                    LiteStream liteStream = (LiteStream) stream;
                    initiateMuxStream(liteStream, cipherStatePair);
                }
                return;
            }
            default:
                throw new Exception("not supported token " + token + " data " + data);
        }

    }

    private void initiateMuxStream(LiteStream stream, CipherStatePair cipherStatePair) {
        SecuredStream securedStream = new SecuredStream(
                stream.getQuicStream(),
                cipherStatePair.getSender(),
                cipherStatePair.getReceiver());

        securedStream.writeOutput(DataHandler.encodeTokens(IPFS.STREAM_PROTOCOL, IPFS.MPLEX_PROTOCOL));
    }


    private void initiateHolePunch(SecuredStream stream) {

        MuxedStream muxedStream = new MuxedStream(stream.getQuicStream(), stream.getSender(),
                stream.getReceiver(), Mplex.generateMuxId(), MuxFlag.OPEN);
        muxedStream.setMuxFlag(MuxFlag.DATA);
        muxedStream.writeOutput(String.valueOf(muxedStream.getMuxId().getStreamId()).getBytes());
        muxedStream.writeOutput(DataHandler.encodeTokens(IPFS.STREAM_PROTOCOL, IPFS.HOLE_PUNCH_PROTOCOL));
    }


    @NonNull
    @Override
    public Transport getTransport(QuicStream quicStream) {
        Transport transport = (Transport) quicStream.getAttribute(TRANSPORT);
        if (transport != null) {
            return transport;
        }
        return new LiteTransport();
    }

    @Override
    public void streamTerminated(QuicStream quicStream) {
        LogUtils.error(TAG, "todo stream is terminated ");
        // todo stream.getConnection().close();
    }
}
