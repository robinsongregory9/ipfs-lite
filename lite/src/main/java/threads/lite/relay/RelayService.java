package threads.lite.relay;

import androidx.annotation.NonNull;

import net.luminis.quic.QuicStream;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicReference;

import circuit.pb.Circuit;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.core.Connection;
import threads.lite.core.Stream;
import threads.lite.core.StreamHandler;
import threads.lite.core.Transport;
import threads.lite.host.LiteConnection;
import threads.lite.host.LiteTransport;
import threads.lite.host.Session;
import threads.lite.ident.IdentityService;
import threads.lite.utils.DataHandler;

public class RelayService {
    private static final String TAG = RelayService.class.getSimpleName();

    @NonNull
    public static CompletableFuture<Reservation> reservation(
            @NonNull Session session, @NonNull PeerId relayId, @NonNull Multiaddr relayAddress) {

        CompletableFuture<Reservation> reservationFuture = new CompletableFuture<>();

        try {

            AtomicReference<Multiaddr> observed = new AtomicReference<>(null);

            // todo not best solution, better completable
            LiteConnection conn = session.getHost().dial(
                    new RelayHandler(session) {
                        @NonNull
                        @Override
                        public Multiaddr getObserved() {
                            return Objects.requireNonNull(observed.get());
                        }
                    }, relayAddress,
                    true, IPFS.CONNECT_TIMEOUT, IPFS.GRACE_PERIOD_RESERVATION,
                    IPFS.MAX_STREAMS, IPFS.MESSAGE_SIZE_MAX);

            IdentityService.getPeerInfo(conn).whenComplete((peerInfo, throwable) -> {
                if (throwable != null) {
                    reservationFuture.completeExceptionally(throwable);
                } else {

                    if (!peerInfo.hasRelayHop()) {
                        reservationFuture.completeExceptionally(
                                new Exception("no relay hop"));
                        return;
                    }
                    Multiaddr multiaddr = peerInfo.getObserved();
                    if (multiaddr == null) {
                        reservationFuture.completeExceptionally(
                                new Exception("no observed address"));
                        return;
                    }
                    observed.set(multiaddr);

                    getReservation(session, conn, relayId, relayAddress)
                            .whenComplete((reservation, throwable1) -> {
                                if (throwable1 != null) {
                                    reservationFuture.completeExceptionally(throwable1);
                                } else {
                                    reservationFuture.complete(reservation);
                                }
                            });
                }
            });


        } catch (Throwable throwable) {
            reservationFuture.completeExceptionally(throwable);
        }
        return reservationFuture;
    }


    public static CompletableFuture<Reservation> getReservation(
            @NonNull Session session, @NonNull Connection conn,
            @NonNull PeerId relayId, @NonNull Multiaddr relayAddress) {


        CompletableFuture<Reservation> future = new CompletableFuture<>();
        // now to the reservation
        conn.createStream(new StreamHandler() {
            @Override
            public void throwable(Stream stream, Throwable throwable) {
                stream.getConnection().close();
                future.completeExceptionally(throwable);
            }

            @Override
            public void token(Stream stream, String token) throws Exception {

                if (!Arrays.asList(IPFS.STREAM_PROTOCOL,
                        IPFS.RELAY_PROTOCOL_HOP).contains(token)) {
                    throw new Exception("Token " + token + " not supported");
                }

                if (Objects.equals(token, IPFS.RELAY_PROTOCOL_HOP)) {
                    Circuit.HopMessage message = Circuit.HopMessage.newBuilder()
                            .setType(Circuit.HopMessage.Type.RESERVE).build();
                    stream.writeOutput(DataHandler.encode(message))
                            .thenApply(Stream::closeOutput);
                }
            }

            @Override
            public void fin() {

            }

            @Override
            public void data(Stream stream, ByteBuffer data) throws Exception {
                Circuit.HopMessage msg = Circuit.HopMessage.parseFrom(data.array());
                if (msg.getType() == Circuit.HopMessage.Type.STATUS) {
                    if (msg.getStatus() == Circuit.Status.OK) {

                        Reservation reservation = new Reservation(
                                relayId, stream.getConnection(), relayAddress,
                                Multiaddr.createCircuitAddress(relayAddress,
                                        session.getHost().self()),
                                msg.getReservation(), msg.getLimit());

                        LogUtils.error(TAG, "ipfs swarm connect " +
                                reservation.getCircuitAddress());

                        // todo keep conn alive

                        future.complete(reservation);
                    } else {
                        throw new Exception("no reservation");
                    }
                } else {
                    throw new Exception("no reservation");
                }
            }

            @NonNull
            @Override
            public Transport getTransport(QuicStream quicStream) {
                return new LiteTransport();
            }

            @Override
            public void streamTerminated(QuicStream quicStream) {
                LogUtils.error(TAG, "todo stream terminated");
            }
        }).thenApply(
                quicStream -> quicStream.writeOutput(
                        DataHandler.encodeTokens(IPFS.STREAM_PROTOCOL, IPFS.RELAY_PROTOCOL_HOP)));

        return future;
    }
}
