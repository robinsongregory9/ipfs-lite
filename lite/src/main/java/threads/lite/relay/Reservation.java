package threads.lite.relay;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.protobuf.ByteString;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import circuit.pb.Circuit;
import circuit.pb.Voucher;
import threads.lite.LogUtils;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.cid.ProtocolSupport;
import threads.lite.core.Connection;

public class Reservation {
    private static final String TAG = Reservation.class.getSimpleName();
    @NonNull
    private final PeerId relayId;
    @NonNull
    private final Connection conn;
    @NonNull
    private final Multiaddr relayAddress;
    @NonNull
    private final Multiaddr circuitAddress;
    @Nullable
    private final Circuit.Reservation reservation;
    @Nullable
    private final Circuit.Limit limit;

    public Reservation(@NonNull PeerId relayId,
                       @NonNull Connection conn,
                       @NonNull Multiaddr relayAddress,
                       @NonNull Multiaddr circuitAddress,
                       @Nullable Circuit.Reservation reservation,
                       @Nullable Circuit.Limit limit) {
        this.relayId = relayId;
        this.conn = conn;
        this.relayAddress = relayAddress;
        this.circuitAddress = circuitAddress;
        this.reservation = reservation;
        this.limit = limit;
        if (LogUtils.isDebug()) {
            // currently no reason to get the voucher
            debug();
        }
    }

    @NonNull
    public List<Multiaddr> getReservationAddresses(@NonNull ProtocolSupport protocolSupport) {
        List<Multiaddr> multiaddrs = new ArrayList<>();
        if (reservation != null) {
            for (ByteString raw : reservation.getAddrsList()) {
                try {
                    Multiaddr multiaddr = Multiaddr.create(raw);
                    if (multiaddr.protocolSupported(protocolSupport, true)) {
                        multiaddrs.add(Multiaddr.createCircuitAddress(multiaddr));
                    }
                } catch (Throwable ignore) {
                }
            }
        }
        return multiaddrs;
    }

    @NonNull
    public Multiaddr getCircuitAddress() {
        return circuitAddress;
    }


    @Nullable
    public Circuit.Limit getLimit() {
        return limit;
    }

    @NonNull
    public Multiaddr getRelayAddress() {
        return relayAddress;
    }

    @Nullable
    public Circuit.Reservation getReservation() {
        return reservation;
    }

    public long expireInMinutes() {
        if (reservation != null) {
            Date expire = new Date(reservation.getExpire() * 1000);
            Date now = new Date();
            long duration = expire.getTime() - now.getTime();
            return TimeUnit.MILLISECONDS.toMinutes(duration);
        }
        return 60;
    }


    @NonNull
    @Override
    public String toString() {
        return "Reservation{" +
                ", relayId=" + relayId +
                ", relayAddress=" + relayAddress +
                ", circuitAddress=" + circuitAddress +
                '}';
    }

    private void debug() {
        if (limit != null) {
            if (limit.hasData()) {
                LogUtils.error(TAG, "Data Limit " + limit.getData());
            }
            if (limit.hasDuration()) {
                LogUtils.error(TAG, "Data Duration " + limit.getDuration());
            }
        }
        if (reservation != null) {
            if (reservation.hasVoucher()) {
                try {
                    Voucher.ReservationVoucher voucher =
                            Voucher.ReservationVoucher.parseFrom(reservation.getVoucher());

                    LogUtils.error(TAG, "hasExpiration " + voucher.hasExpiration());
                    LogUtils.error(TAG, "Expiration " + voucher.getExpiration());

                    LogUtils.error(TAG, new String(voucher.getPeer().toByteArray()));
                    LogUtils.error(TAG, new String(voucher.getRelay().toByteArray()));
                    try {
                        LogUtils.error(TAG, "Relay " +
                                Multiaddr.create(voucher.getRelay()));
                        LogUtils.error(TAG, "Peer " +
                                Multiaddr.create(voucher.getPeer()));
                    } catch (Throwable ignore) {
                    }
                } catch (Throwable throwable) {
                    LogUtils.debug(TAG, "hasVoucher " + throwable.getMessage());
                }
            }
        }
    }

    @NonNull
    public PeerId getRelayId() {
        return relayId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Reservation that = (Reservation) o;
        return relayId.equals(that.relayId) &&
                conn.equals(that.conn) &&
                relayAddress.equals(that.relayAddress) &&
                circuitAddress.equals(that.circuitAddress) &&
                Objects.equals(reservation, that.reservation) &&
                Objects.equals(limit, that.limit);
    }

    @Override
    public int hashCode() {
        return Objects.hash(relayId, conn, relayAddress);
    }

    @NonNull
    public Connection getConnection() {
        return conn;
    }

}
