package threads.lite.relay;

import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;
import com.southernstorm.noise.protocol.CipherStatePair;

import net.luminis.quic.QuicStream;

import java.nio.ByteBuffer;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;
import java.util.Set;

import circuit.pb.Circuit;
import holepunch.pb.Holepunch;
import identify.pb.IdentifyOuterClass;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Multiaddrs;
import threads.lite.cid.PeerId;
import threads.lite.core.Stream;
import threads.lite.core.StreamHandler;
import threads.lite.core.Transport;
import threads.lite.host.LiteTransport;
import threads.lite.host.Session;
import threads.lite.mplex.MuxedTransport;
import threads.lite.noise.Handshake;
import threads.lite.noise.Noise;
import threads.lite.noise.SecuredStream;
import threads.lite.noise.SecuredTransport;
import threads.lite.utils.DataHandler;

public abstract class RelayHandler implements StreamHandler {
    private static final String TAG = RelayHandler.class.getSimpleName();
    private static final String TOKEN = "TOKEN";
    private static final String ADDRS = "ADDRS";
    private static final String TRANSPORT = "TRANSPORT";
    private static final String NOISE = "NOISE";

    private final Set<String> protocols = Set.of(IPFS.HOLE_PUNCH_PROTOCOL, IPFS.RELAY_PROTOCOL_STOP,
            IPFS.STREAM_PROTOCOL, IPFS.MPLEX_PROTOCOL,
            IPFS.IDENTITY_PROTOCOL, IPFS.NOISE_PROTOCOL);

    private final Session session;


    public RelayHandler(Session session) {
        this.session = session;
    }

    @NonNull
    public abstract Multiaddr getObserved();

    public Noise.NoiseState getNoise(Stream stream) throws NoSuchAlgorithmException {
        Noise.NoiseState state = (Noise.NoiseState) stream.getAttribute(NOISE);
        if (state == null) {
            state = session.getHost().getNoise().getResponder(
                    session.getHost().getKeypair());
            stream.setAttribute(NOISE, state);
        }
        return state;
    }


    @Override
    public void throwable(Stream stream, Throwable throwable) {
        LogUtils.error(TAG, throwable);
        stream.closeOutput();
        stream.closeInput();
    }

    @Override
    public void token(Stream stream, String token) throws Exception {

        LogUtils.error(TAG, "Token " + token +
                " streamId " + stream.getStreamId() + " initiator " + stream.isInitiator());

        stream.setAttribute(TOKEN, token);
        switch (token) {
            case IPFS.STREAM_PROTOCOL:
                break;
            case IPFS.HOLE_PUNCH_PROTOCOL: {
                // B accepts "/libp2p/dcutr" protocol from A
                stream.writeOutput(DataHandler.encodeTokens(IPFS.STREAM_PROTOCOL, IPFS.HOLE_PUNCH_PROTOCOL));
                break;
            }
            case IPFS.RELAY_PROTOCOL_STOP:
                stream.writeOutput(DataHandler.encodeTokens(IPFS.STREAM_PROTOCOL, IPFS.RELAY_PROTOCOL_STOP));
                break;
            case IPFS.IDENTITY_PROTOCOL:
                stream.writeOutput(DataHandler.encodeTokens(IPFS.STREAM_PROTOCOL, IPFS.IDENTITY_PROTOCOL));

                IdentifyOuterClass.Identify response =
                        session.getHost().createIdentity(protocols,
                                session.getHost().networkListenAddresses(),
                                stream.getConnection().getRemoteAddress());

                stream.writeOutput(DataHandler.encode(response)).thenApply(Stream::closeOutput);

                LogUtils.error(TAG, "Write Identity");
                break;
            case IPFS.MPLEX_PROTOCOL:

                stream.writeOutput(DataHandler.encodeTokens(IPFS.STREAM_PROTOCOL, IPFS.MPLEX_PROTOCOL));

                // make sure that the stream is secured stream
                if (stream instanceof SecuredStream) {
                    SecuredStream securedStream = (SecuredStream) stream;

                    // finished connection
                    stream.setAttribute(TRANSPORT, new MuxedTransport(
                            securedStream.getSender(), securedStream.getReceiver()));

                    LogUtils.error(TAG, "Transport set to MuxedTransport");

                } else {
                    throw new Exception("wrong stream");
                }
                break;
            case IPFS.NOISE_PROTOCOL:
                // make sure that I am the "not" initiator
                stream.writeOutput(DataHandler.encodeTokens(IPFS.STREAM_PROTOCOL, IPFS.NOISE_PROTOCOL));
                stream.setAttribute(TRANSPORT, new Handshake());
                LogUtils.error(TAG, "Transport set to Handshake");
                break;
            default:
                stream.writeOutput(DataHandler.encodeTokens(IPFS.STREAM_PROTOCOL, IPFS.NA));
        }
    }

    @Override
    public void fin() {

    }

    @Override
    public void data(Stream stream, ByteBuffer data) throws Exception {

        String token = (String) stream.getAttribute(TOKEN);
        LogUtils.error(TAG, "data streamId " + stream.getStreamId() +
                " token " + token + " initiator " + stream.isInitiator());
        Objects.requireNonNull(token);

        switch (token) {

            case IPFS.NOISE_PROTOCOL: {

                Noise.Response response = getNoise(stream).handshake(data.array());

                byte[] msg = response.getMessage();
                if (msg != null) {
                    stream.writeOutput(Noise.encodeNoiseMessage(msg));
                }

                CipherStatePair cipherStatePair = response.getCipherStatePair();
                if (cipherStatePair != null) {
                    stream.setAttribute(TRANSPORT, new SecuredTransport(
                            cipherStatePair.getSender(), cipherStatePair.getReceiver()));

                    LogUtils.error(TAG, "Transport set to SecuredTransport");
                }
                break;
            }

            case IPFS.HOLE_PUNCH_PROTOCOL: {
                Holepunch.HolePunch holePunch =
                        Holepunch.HolePunch.parseFrom(data.array());
                Objects.requireNonNull(holePunch);

                if (holePunch.getType() == Holepunch.HolePunch.Type.SYNC) {
                    // On the other end, as soon as B receives A's Sync message, it immediately
                    // directly dials A with the addresses provided in A's Connect message.
                    // Upon receiving the Sync, B immediately dials the addresses
                    // of A

                    Multiaddrs multiaddrs = (Multiaddrs) stream.getAttribute(ADDRS);
                    Objects.requireNonNull(multiaddrs, "no multiaddr defined");

                    LogUtils.error(TAG, "[B] Hole Punch Dial Address " + multiaddrs);

                    // Upon expiry of the timer, B starts to send UDP packets filled with
                    // random bytes to A's address.
                    session.holePunchConnect(multiaddrs);


                } else if (holePunch.getType() == Holepunch.HolePunch.Type.CONNECT) {
                    //  B receives the Connect message on the relayed connection and replies with a
                    //  Connect message containing its (non-relayed) addresses.
                    // Upon receiving the Connect, A responds back with a
                    // Connect message containing its observed (and possibly
                    // predicted) addresses;
                    Multiaddrs multiaddrs = Multiaddr.create(
                            holePunch.getObsAddrsList(), false);

                    if (multiaddrs.size() == 0) {
                        throw new Exception("[B] empty observed address from [A]");
                    }

                    stream.setAttribute(ADDRS, multiaddrs);

                    Multiaddr observed = getObserved();
                    LogUtils.error(TAG, "[B] observed address " + observed);
                    Holepunch.HolePunch.Builder builder =
                            Holepunch.HolePunch.newBuilder()
                                    .setType(Holepunch.HolePunch.Type.CONNECT);


                    builder.addObsAddrs(ByteString.copyFrom(observed.getBytes()));
                    for (Multiaddr multiaddr : session.getHost().networkListenAddresses()) {
                        builder.addObsAddrs(ByteString.copyFrom(multiaddr.getBytes()));
                    }

                    stream.writeOutput(DataHandler.encode(builder.build()));
                } else {
                    throw new Exception("not expected Holepunch Type");
                }
                break;
            }

            case IPFS.RELAY_PROTOCOL_STOP: {
                Circuit.StopMessage stopMessage = Circuit.StopMessage.parseFrom(data.array());
                Objects.requireNonNull(stopMessage);

                if (stopMessage.hasPeer()) {
                    PeerId peerId = PeerId.create(stopMessage.getPeer().toByteArray());
                    if (!session.isGated(peerId)) {
                        Circuit.StopMessage.Builder builder =
                                Circuit.StopMessage.newBuilder()
                                        .setType(Circuit.StopMessage.Type.STATUS);
                        builder.setStatus(Circuit.Status.OK);
                        stream.writeOutput(DataHandler.encode(builder.build()));
                    } else {
                        Circuit.StopMessage.Builder builder =
                                Circuit.StopMessage.newBuilder()
                                        .setType(Circuit.StopMessage.Type.STATUS);
                        builder.setStatus(Circuit.Status.RESERVATION_REFUSED);
                        stream.writeOutput(DataHandler.encode(builder.build()));
                    }
                } else {
                    Circuit.StopMessage.Builder builder =
                            Circuit.StopMessage.newBuilder()
                                    .setType(Circuit.StopMessage.Type.STATUS);
                    builder.setStatus(Circuit.Status.MALFORMED_MESSAGE);
                    stream.writeOutput(DataHandler.encode(builder.build()));
                }
                break;
            }
            default:
                throw new Exception("Token " + token + " not supported in data " + data);
        }

    }

    @NonNull
    @Override
    public Transport getTransport(QuicStream quicStream) {
        Transport transport = (Transport) quicStream.getAttribute(TRANSPORT);
        if (transport != null) {
            return transport;
        }
        return new LiteTransport();
    }

    @Override
    public void streamTerminated(QuicStream quicStream) {
        LogUtils.error(TAG, "todo stream is terminated ");
    }

}
