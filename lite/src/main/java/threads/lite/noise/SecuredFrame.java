package threads.lite.noise;

import androidx.annotation.NonNull;

import threads.lite.host.LiteFrame;

public class SecuredFrame extends LiteFrame {
    public SecuredFrame(int length) {
        super(length);
    }

    @NonNull
    @Override
    public String toString() {
        return "SecuredFrame{" +
                "length=" + getLength() +
                '}';
    }
}
