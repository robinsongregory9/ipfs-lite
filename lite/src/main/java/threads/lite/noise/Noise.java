package threads.lite.noise;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.protobuf.ByteString;
import com.southernstorm.noise.protocol.CipherState;
import com.southernstorm.noise.protocol.CipherStatePair;
import com.southernstorm.noise.protocol.DHState;
import com.southernstorm.noise.protocol.HandshakeState;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.crypto.BadPaddingException;
import javax.crypto.ShortBufferException;

import crypto.pb.Crypto;
import pb.Payload;
import threads.lite.LogUtils;
import threads.lite.cid.PeerId;
import threads.lite.crypto.Key;
import threads.lite.crypto.PubKey;


public class Noise {
    // payloadSigPrefix is prepended to our Noise static key before signing with
    // our libp2p identity key.
    public final static String NOISE_LIBP_2_P_STATIC_KEY = "noise-libp2p-static-key:";
    public final static String PROTOCOL_NAME = "Noise_XX_25519_ChaChaPoly_SHA256";
    private static final String TAG = Noise.class.getSimpleName();
    private static final int ENCODING = 2;
    private final byte[] localPrivateKey25519 = new byte[32];

    private Noise() {
        com.southernstorm.noise.protocol.Noise.random(localPrivateKey25519);
    }

    public static Noise getInstance() {
        return new Noise();
    }

    public static byte[] encrypt(@NonNull CipherState cipherState, byte[] plaintext) throws ShortBufferException {
        byte[] ciphertext = new byte[plaintext.length + cipherState.getMACLength()];
        int length = cipherState.encryptWithAd(null, plaintext, 0, ciphertext,
                0, plaintext.length);
        return Arrays.copyOf(ciphertext, length);
    }

    public static byte[] decrypt(@NonNull CipherState cipherState, byte[] ciphertext)
            throws ShortBufferException, BadPaddingException {
        byte[] plaintext = new byte[ciphertext.length];
        int length = cipherState.decryptWithAd(null, ciphertext,
                0, plaintext, 0, ciphertext.length);
        return Arrays.copyOf(plaintext, length);
    }

    @Nullable
    public static PeerId verifyPayload(@NonNull HandshakeState handshakeState,
                                       byte[] payload) throws Exception {
        // verify the signature of the remote's noise static public key once
        // the remote public key has been provided by the XX protocol
        DHState derivedRemotePublicKey = handshakeState.getRemotePublicKey();
        if (derivedRemotePublicKey.hasPublicKey()) {
            return Noise.verifyPayload(derivedRemotePublicKey, payload);
        }
        return null;
    }

    public static void verifyPayload(@NonNull HandshakeState handshakeState,
                                     @NonNull PeerId expectedRemotePeerId,
                                     byte[] payload) throws Exception {
        // verify the signature of the remote's noise static public key once
        // the remote public key has been provided by the XX protocol
        DHState derivedRemotePublicKey = handshakeState.getRemotePublicKey();
        if (derivedRemotePublicKey.hasPublicKey()) {
            PeerId remotePeerId = Noise.verifyPayload(derivedRemotePublicKey, payload);
            if (!Objects.equals(expectedRemotePeerId, remotePeerId)) {
                throw new Exception("InvalidRemotePubKey");
            }
        }
    }

    public static byte[] readNoiseMessage(@NonNull HandshakeState handshakeState, byte[] msg) throws Exception {
        byte[] payload = new byte[msg.length];
        int payloadLength =
                handshakeState.readMessage(msg, 0, msg.length, payload, 0);
        return Arrays.copyOf(payload, payloadLength);
    }

    public static byte[] getNoiseMessage(
            @NonNull HandshakeState handshakeState, @NonNull byte[] message)
            throws ShortBufferException {

        int msgLength = message.length;

        byte[] bytes = new byte[
                msgLength + (2 * (handshakeState.getLocalKeyPair().getPublicKeyLength()
                        + 16))]; // 16 is MAC length

        int length = handshakeState.writeMessage(bytes, 0,
                message, 0, msgLength);
        return Arrays.copyOfRange(bytes, 0, length);
    }

    public static PeerId verifyPayload(@NonNull DHState remotePublicKeyState,
                                       @NonNull byte[] payload) throws Exception {

        Payload.NoiseHandshakePayload noiseMsg = Payload.NoiseHandshakePayload.parseFrom(payload);
        PubKey publicKey = Key.unmarshalPublicKey(noiseMsg.getIdentityKey().toByteArray());
        byte[] signatureFromMessage = noiseMsg.getIdentitySig().toByteArray();

        publicKey.verify(Noise.noiseSignaturePhrase(remotePublicKeyState), signatureFromMessage);

        return PeerId.fromPubKey(publicKey);
    }

    public static byte[] generateHandshakePayload(@NonNull KeyPair keyPair, @NonNull DHState localNoiseState) throws Exception {

        // the payload consists of the identity public key, and the signature of the noise static public key
        // the actual noise static public key is sent later as part of the XX handshake
        // get identity public key
        byte[] pkBytes = Crypto.PublicKey.newBuilder().setType(Crypto.KeyType.RSA).
                setData(ByteString.copyFrom(keyPair.getPublic().getEncoded()))
                .build().toByteArray();

        // get noise static public key signature
        byte[] signedPayload = sign(keyPair.getPrivate(),
                Noise.noiseSignaturePhrase(localNoiseState));

        return Payload.NoiseHandshakePayload.newBuilder()
                .setIdentityKey(ByteString.copyFrom(pkBytes))
                .setIdentitySig(ByteString.copyFrom(signedPayload))
                .build().toByteArray();
    }

    private static byte[] sign(@NonNull PrivateKey key, byte[] data) throws Exception {
        Signature sha256withRSA = Signature.getInstance("SHA256withRSA");
        sha256withRSA.initSign(key);
        sha256withRSA.update(data);
        return sha256withRSA.sign();
    }

    public static byte[] noiseSignaturePhrase(@NonNull DHState dhState) {
        byte[] key = Noise.NOISE_LIBP_2_P_STATIC_KEY.getBytes();
        byte[] phrase = new byte[key.length + dhState.getPublicKeyLength()];
        System.arraycopy(key, 0, phrase, 0, key.length);
        dhState.getPublicKey(phrase, key.length);
        return phrase;
    }

    public static byte[] encodeNoiseMessage(byte[] noise) {
        byte[] bigEndian = shortToBigEndianBytes((short) noise.length);
        ByteBuffer out = ByteBuffer.allocate(noise.length + ENCODING);
        out.put(bigEndian);
        out.put(noise);
        return out.array();
    }

    public static byte[] decodeNoiseMessage(byte[] noise) {
        return Arrays.copyOfRange(noise, ENCODING, noise.length);
    }


    public static short bytesToBigEndianShort(ByteBuffer data) {
        byte[] lengthData = new byte[ENCODING];
        data.get(lengthData);
        return Noise.bytesToBigEndianShort(lengthData);
    }

    public static short bytesToBigEndianShort(byte[] bytes) {
        return ByteBuffer.wrap(bytes).order(ByteOrder.BIG_ENDIAN).getShort();
    }

    public static byte[] shortToBigEndianBytes(short value) {
        return ByteBuffer.allocate(ENCODING).order(ByteOrder.BIG_ENDIAN).putShort(value).array();
    }


    public NoiseState getInitiator(PeerId peerId, KeyPair keyPair) throws NoSuchAlgorithmException {
        return getNoiseState(peerId, keyPair, HandshakeState.INITIATOR);
    }

    public NoiseState getResponder(KeyPair keyPair) throws NoSuchAlgorithmException {
        return getNoiseState(null, keyPair, HandshakeState.RESPONDER);
    }

    private NoiseState getNoiseState(@Nullable PeerId peerId, KeyPair keyPair, int role) throws NoSuchAlgorithmException {
        DHState localNoiseState = com.southernstorm.noise.protocol.Noise.createDH("25519");

        // configure the localDHState with the private
        // which will automatically generate the corresponding public key
        localNoiseState.setPrivateKey(getPrivateKey(), 0);


        HandshakeState handshakeState = new HandshakeState(Noise.PROTOCOL_NAME, role);
        handshakeState.getLocalKeyPair().copyFrom(localNoiseState);
        handshakeState.start();
        return new NoiseState(peerId, localNoiseState, handshakeState, keyPair);
    }

    public byte[] getPrivateKey() {
        return localPrivateKey25519;
    }


    public static class Response {
        @Nullable
        private final CipherStatePair cipherStatePair;
        @Nullable
        private final byte[] message;

        public Response(@Nullable CipherStatePair cipherStatePair, @Nullable byte[] message) {
            this.cipherStatePair = cipherStatePair;
            this.message = message;
        }

        @Nullable
        public CipherStatePair getCipherStatePair() {
            return cipherStatePair;
        }

        @Nullable
        public byte[] getMessage() {
            return message;
        }
    }

    public static class NoiseState {
        private final AtomicBoolean sentNoiseKeyPayload = new AtomicBoolean(false);
        private final HandshakeState handshakeState;
        private final DHState localNoiseState;
        private final KeyPair keyPair;
        @Nullable
        private final PeerId peerId;

        public NoiseState(@Nullable PeerId peerId, DHState localNoiseState,
                          HandshakeState handshakeState, KeyPair keyPair) {
            this.peerId = peerId;
            this.handshakeState = handshakeState;
            this.localNoiseState = localNoiseState;
            this.keyPair = keyPair;
        }

        public HandshakeState getHandshakeState() {
            return handshakeState;
        }

        public DHState getLocalNoiseState() {
            return localNoiseState;
        }


        public byte[] getInitalMessage() throws ShortBufferException {
            return Noise.getNoiseMessage(getHandshakeState(), new byte[0]);
        }


        @NonNull
        public Response handshake(byte[] msg) throws Exception {

            CipherStatePair cipherStatePair = null;
            byte[] message = null;
            if (getHandshakeState().getAction() == HandshakeState.READ_MESSAGE) {
                byte[] payload = Noise.readNoiseMessage(getHandshakeState(), msg);

                // verify the signature of the remote's noise static public key once
                // the remote public key has been provided by the XX protocol
                if (peerId != null) {
                    verifyPayload(getHandshakeState(), peerId, payload);
                } else {
                    PeerId peerId = Noise.verifyPayload(getHandshakeState(), payload);
                    if (peerId != null) {
                        LogUtils.debug(TAG, "PeerId " + peerId.toBase58());
                    }
                }
            }


            // after reading messages and setting up state, write next message onto the wire
            if (getHandshakeState().getAction() == HandshakeState.WRITE_MESSAGE) {

                // only send the Noise static key once
                if (!sentNoiseKeyPayload.getAndSet(true)) {

                    // generate an appropriate protobuf element
                    byte[] noiseHandshakePayload =
                            Noise.generateHandshakePayload(keyPair, getLocalNoiseState());

                    // create the message with the signed payload -
                    // verification happens once the noise static key is shared

                    message = Noise.getNoiseMessage(getHandshakeState(), noiseHandshakePayload);
                }
            }

            if (getHandshakeState().getAction() == HandshakeState.SPLIT) {
                cipherStatePair = getHandshakeState().split();
            }
            return new Response(cipherStatePair, message);
        }
    }
}
