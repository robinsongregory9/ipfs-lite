package threads.lite.noise;

import net.luminis.quic.QuicStream;

import threads.lite.core.Frame;
import threads.lite.core.Stream;
import threads.lite.core.Transport;
import threads.lite.host.LiteStream;

public class Handshake implements Transport {
    @Override
    public Type getType() {
        return Type.HANDSHAKE;
    }

    @Override
    public Stream getStream(QuicStream quicStream, Frame frame) {
        return new LiteStream(quicStream);
    }
}
