package threads.lite.noise;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.southernstorm.noise.protocol.CipherState;

import net.luminis.quic.QuicStream;

import java.nio.ByteBuffer;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import threads.lite.LogUtils;
import threads.lite.core.Connection;
import threads.lite.core.Stream;
import threads.lite.host.LiteConnection;
import threads.lite.utils.DataHandler;

public class SecuredStream implements Stream {
    private static final String TAG = SecuredStream.class.getSimpleName();
    @NonNull
    private final QuicStream quicStream;
    @NonNull
    private final CipherState sender;
    @NonNull
    private final CipherState receiver;

    public SecuredStream(@NonNull QuicStream quicStream,
                         @NonNull CipherState sender,
                         @NonNull CipherState receiver) {
        this.quicStream = quicStream;
        this.sender = sender;
        this.receiver = receiver;
    }

    @NonNull
    public CipherState getSender() {
        return sender;
    }

    @NonNull
    public CipherState getReceiver() {
        return receiver;
    }

    @NonNull
    @Override
    public Connection getConnection() {
        return new LiteConnection(quicStream.getConnection());
    }

    @NonNull
    @Override
    public CompletableFuture<Stream> closeOutput() {
        CompletableFuture<Stream> stream = new CompletableFuture<>();
        quicStream.closeOutput().whenComplete((quicStream, throwable) -> {
            if (throwable != null) {
                stream.completeExceptionally(throwable);
            } else {
                stream.complete(new SecuredStream(quicStream, getSender(), getReceiver()));
            }
        });
        return stream;
    }

    @NonNull
    @Override
    public CompletableFuture<Stream> writeOutput(byte[] data) {
        CompletableFuture<Stream> stream = new CompletableFuture<>();
        try {
            LogUtils.debug(TAG, "WRITE (0) Noise data size " + data.length);
            byte[] encrypted = Noise.encrypt(sender, data);
            LogUtils.debug(TAG, "WRITE (1) Noise encrypted size " + encrypted.length);
            byte[] encoded = Noise.encodeNoiseMessage(encrypted); // todo not sure if required at all
            LogUtils.debug(TAG, "WRITE (2) Noise encoded size " + encoded.length);

            quicStream.writeOutput(encoded).whenComplete((quicStream, throwable) -> {
                if (throwable != null) {
                    stream.completeExceptionally(throwable);
                } else {
                    stream.complete(new SecuredStream(quicStream, getSender(), getReceiver()));
                }
            });

        } catch (Throwable throwable) {
            stream.completeExceptionally(throwable);
        }
        return stream;
    }

    @Override
    public int getStreamId() {
        return quicStream.getStreamId();
    }

    @Override
    public void closeInput() {
        quicStream.closeInput();
    }

    @Override
    public void setAttribute(String key, Object value) {
        quicStream.setAttribute(key, value);
    }

    @Nullable
    @Override
    public Object getAttribute(String key) {
        return quicStream.getAttribute(key);
    }

    @Override
    public boolean isInitiator() {
        return getStreamId() % 2 == 0;
    }

    @NonNull
    public List<ByteBuffer> readFrames(ByteBuffer data) throws Exception {
        LogUtils.debug(TAG, "READ (0) Noise encrypted size " + data.array().length);
        byte[] decrypted = Noise.decrypt(receiver, data.array());
        LogUtils.debug(TAG, "READ (1) Noise decrypted size " + decrypted.length);
        LogUtils.debug(TAG, "READ plain " + new String(decrypted));
        List<ByteBuffer> frames = DataHandler.decode(ByteBuffer.wrap(decrypted));
        LogUtils.debug(TAG, "Packages size " + frames.size());
        return frames;

    }

    @NonNull
    public QuicStream getQuicStream() {
        return quicStream;
    }
}
