package threads.lite.noise;

import androidx.annotation.NonNull;

import com.southernstorm.noise.protocol.CipherState;

import net.luminis.quic.QuicStream;

import threads.lite.core.Frame;
import threads.lite.core.Stream;
import threads.lite.core.Transport;

public class SecuredTransport implements Transport {
    @NonNull
    private final CipherState sender;
    @NonNull
    private final CipherState receiver;

    public SecuredTransport(@NonNull CipherState sender, @NonNull CipherState receiver) {
        this.sender = sender;
        this.receiver = receiver;
    }

    @NonNull
    public CipherState getSender() {
        return sender;
    }

    @NonNull
    public CipherState getReceiver() {
        return receiver;
    }

    @Override
    public Type getType() {
        return Type.SECURED;
    }

    @Override
    public Stream getStream(QuicStream quicStream, Frame frame) {
        return new SecuredStream(quicStream, getSender(), getReceiver());
    }
}
