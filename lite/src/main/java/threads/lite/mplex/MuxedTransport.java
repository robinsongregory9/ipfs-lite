package threads.lite.mplex;

import androidx.annotation.NonNull;

import com.southernstorm.noise.protocol.CipherState;

import net.luminis.quic.QuicStream;

import threads.lite.core.Frame;
import threads.lite.core.Stream;
import threads.lite.core.Transport;

public class MuxedTransport implements Transport {
    @NonNull
    private final CipherState sender;
    @NonNull
    private final CipherState receiver;

    public MuxedTransport(@NonNull CipherState sender, @NonNull CipherState receiver) {
        this.sender = sender;
        this.receiver = receiver;
    }

    @NonNull
    public CipherState getSender() {
        return sender;
    }

    @NonNull
    public CipherState getReceiver() {
        return receiver;
    }

    @Override
    public Type getType() {
        return Type.MUXED;
    }

    @Override
    public Stream getStream(QuicStream quicStream, Frame frame) {
        if (frame instanceof MuxedFrame) {
            MuxedFrame muxedFrame = (MuxedFrame) frame;
            return new MuxedStream(quicStream, getSender(), getReceiver(),
                    muxedFrame.getMuxId(), muxedFrame.getMuxFlag());
        }
        throw new IllegalStateException("invalid frame for muxed transport");
    }
}
