package threads.lite.mplex;

import android.util.Pair;

import androidx.annotation.NonNull;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

import threads.lite.LogUtils;
import threads.lite.utils.DataHandler;

public final class Mplex {
    private static final String TAG = Mplex.class.getSimpleName();
    private static final AtomicInteger GENERATOR = new AtomicInteger(0);


    public static MuxId defaultMuxId() {
        return new MuxId(0, true);
    }

    public static MuxId generateMuxId() {
        return new MuxId(GENERATOR.incrementAndGet(), true);
    }

    @NonNull
    public static MuxedFrame decode(byte[] data) throws IOException {
        Objects.requireNonNull(data);

        ByteBuffer wrap = ByteBuffer.wrap(data);
        int header = DataHandler.readUnsignedLeb128(wrap);
        int length = DataHandler.readUnsignedLeb128(wrap);
        Pair<MuxId, MuxFlag> pair = Mplex.decodeHeader(header);
        byte[] result = new byte[length];
        wrap.get(result);
        return new MuxedFrame(pair.first, pair.second, result);
    }

    /*
     * Encodes the given mplex frame into bytes and writes them into the output.
     * @see [https://github.com/libp2p/specs/tree/master/mplex]
     */
    public static byte[] encode(@NonNull MuxedFrame msg) {
        int header = encodeHeader(msg);
        byte[] data = msg.getData().array();
        int length = data.length;
        int headerLength = DataHandler.unsignedLeb128Size(header);
        int lengthLength = DataHandler.unsignedLeb128Size(length);
        ByteBuffer out = ByteBuffer.allocate(lengthLength + headerLength + length);
        DataHandler.writeUnsignedLeb128(out, header);
        DataHandler.writeUnsignedLeb128(out, length);
        out.put(data);
        return out.array();
    }

    public static int encodeHeader(@NonNull MuxedFrame msg) {
        int flag = MplexFlag.toMplexFlag(msg.getMuxFlag(), msg.getMuxId().isInitiator()).getValue();
        return msg.getMuxId().getStreamId() << 3 | flag;
    }

    public static Pair<MuxId, MuxFlag> decodeHeader(int header) {
        MplexFlag streamTag = MplexFlag.get(header & 0x07);
        LogUtils.debug(TAG, "Header Tag " + streamTag);
        int streamId = header >> 3;
        LogUtils.debug(TAG, "Header Id " + streamId);
        boolean initiator;
        if (streamTag == MplexFlag.NewStream) {
            initiator = false;
        } else {
            initiator = !MplexFlag.isInitiator(streamTag);
        }
        LogUtils.debug(TAG, "Header initiator " + initiator);

        return Pair.create(new MuxId(streamId, initiator), MplexFlag.toAbstractFlag(streamTag));
    }

}
