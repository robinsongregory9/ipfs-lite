package threads.lite.mplex;

import androidx.annotation.NonNull;

import java.nio.ByteBuffer;

import threads.lite.core.Frame;

public class MuxedFrame implements Frame {
    @NonNull
    private final MuxFlag muxFlag;
    @NonNull
    private final ByteBuffer data;
    private final int length;
    @NonNull
    private final MuxId muxId;

    public MuxedFrame(@NonNull MuxId muxId, @NonNull MuxFlag muxFlag, byte[] data) {
        this.muxId = muxId;
        this.data = ByteBuffer.wrap(data);
        this.length = data.length;
        this.muxFlag = muxFlag;
    }

    public MuxedFrame(@NonNull MuxId muxId, @NonNull MuxFlag muxFlag, int length) {
        this.muxId = muxId;
        this.length = length;
        this.data = ByteBuffer.allocate(length);
        this.muxFlag = muxFlag;
    }

    @NonNull
    public MuxFlag getMuxFlag() {
        return muxFlag;
    }

    @NonNull
    @Override
    public String toString() {
        return "MuxFrame{" +
                "flag=" + muxFlag +
                ", data=" + data +
                ", length=" + length +
                ", muxId=" + muxId +
                '}';
    }


    @NonNull
    public MuxId getMuxId() {
        return muxId;
    }

    @Override
    public int getLength() {
        return length;
    }

    @NonNull
    @Override
    public ByteBuffer getData() {
        return data;
    }

    @Override
    public void put(byte[] bytes) {
        data.put(bytes);
    }

    @Override
    public void put(byte value) {
        data.put(value);
    }

    @Override
    public int position() {
        return data.position();
    }
}
