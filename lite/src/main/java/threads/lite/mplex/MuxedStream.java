package threads.lite.mplex;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.southernstorm.noise.protocol.CipherState;

import net.luminis.quic.QuicStream;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import javax.crypto.BadPaddingException;
import javax.crypto.ShortBufferException;

import threads.lite.LogUtils;
import threads.lite.core.Connection;
import threads.lite.core.Stream;
import threads.lite.host.LiteConnection;
import threads.lite.host.LiteStream;
import threads.lite.noise.Noise;
import threads.lite.utils.DataHandler;

public class MuxedStream implements Stream {
    private static final String TAG = MuxedStream.class.getSimpleName();

    @NonNull
    private final MuxId muxId;
    @NonNull
    private final QuicStream quicStream;
    @NonNull
    private final CipherState sender;
    @NonNull
    private final CipherState receiver;
    @NonNull
    private MuxFlag muxFlag;

    public MuxedStream(@NonNull QuicStream quicStream,
                       @NonNull CipherState sender,
                       @NonNull CipherState receiver,
                       @NonNull MuxId muxId,
                       @NonNull MuxFlag muxFlag) {
        this.quicStream = quicStream;
        this.muxId = muxId;
        this.sender = sender;
        this.receiver = receiver;
        this.muxFlag = muxFlag;
    }

    public static byte[] getMuxDecodedData(byte[] data, CipherState receiver)
            throws ShortBufferException, BadPaddingException, IOException {
        MuxedFrame frame = Mplex.decode(data);
        return getDecodedData(frame.getData().array(), receiver);
    }

    public static byte[] getDecodedData(byte[] data, CipherState receiver)
            throws ShortBufferException, BadPaddingException {
        LogUtils.debug(TAG, "READ (1) Mux decoded size " + data.length);
        byte[] decrypted = Noise.decrypt(receiver, data);
        LogUtils.debug(TAG, "READ (2) Mux (Noise) decrypted size " + decrypted.length);
        byte[] decoded = Noise.decodeNoiseMessage(decrypted);
        LogUtils.debug(TAG, "READ (3) Mux (Noise) decoded size " + decoded.length);
        LogUtils.debug(TAG, "READ (3) Mux (Plain) text " + new String(decoded));
        return decoded;
    }

    public static byte[] getMuxEncodedData(byte[] data, CipherState sender, MuxId muxId,
                                           MuxFlag muxFlag) throws ShortBufferException {
        LogUtils.debug(TAG, "WRITE (0) Mux initiator " + muxId.isInitiator());
        LogUtils.debug(TAG, "WRITE (1) Mux (Plain) data size " + data.length);
        byte[] encoded = Noise.encodeNoiseMessage(data); // todo not sure if required at all
        LogUtils.debug(TAG, "WRITE (2) Noise encoded size " + encoded.length);
        byte[] encrypted = Noise.encrypt(sender, encoded);
        LogUtils.debug(TAG, "WRITE (2) Mux (Noise) encrypted size " + encrypted.length);
        byte[] muxEncoded = Mplex.encode(new MuxedFrame(muxId, muxFlag, encrypted));
        LogUtils.debug(TAG, "WRITE (3) Mux encoded size " + muxEncoded.length);
        return muxEncoded;
    }

    @NonNull
    public CipherState getSender() {
        return sender;
    }

    @NonNull
    public CipherState getReceiver() {
        return receiver;
    }

    @NonNull
    public MuxFlag getMuxFlag() {
        return muxFlag;
    }

    public void setMuxFlag(@NonNull MuxFlag muxFlag) {
        this.muxFlag = muxFlag;
    }

    @NonNull
    public MuxId getMuxId() {
        return muxId;
    }

    @NonNull
    @Override
    public Connection getConnection() {
        return new LiteConnection(quicStream.getConnection());
    }

    @NonNull
    @Override
    public CompletableFuture<Stream> closeOutput() {
        CompletableFuture<Stream> stream = new CompletableFuture<>();
        quicStream.closeOutput().whenComplete((quicStream, throwable) -> {
            if (throwable != null) {
                stream.completeExceptionally(throwable);
            } else {
                stream.complete(new LiteStream(quicStream));
            }
        });
        return stream;

    }

    @NonNull
    @Override
    public CompletableFuture<Stream> writeOutput(byte[] data) {
        CompletableFuture<Stream> stream = new CompletableFuture<>();
        try {
            byte[] muxEncoded = getMuxEncodedData(data, sender, muxId, muxFlag);
            quicStream.writeOutput(muxEncoded).whenComplete((quicStream, throwable) -> {
                if (throwable != null) {
                    stream.completeExceptionally(throwable);
                } else {
                    stream.complete(new MuxedStream(quicStream, getSender(), getReceiver(),
                            getMuxId(), getMuxFlag()));
                }
            });
        } catch (Throwable throwable) {
            stream.completeExceptionally(throwable);
        }
        return stream;
    }

    @Override
    public int getStreamId() {
        return muxId.getStreamId();
    }

    @Override
    public void closeInput() {
        quicStream.closeInput();
    }

    @Override
    public void setAttribute(String key, Object value) {
        quicStream.setAttribute(key, value);
    }

    @Nullable
    @Override
    public Object getAttribute(String key) {
        return quicStream.getAttribute(key);
    }

    @Override
    public boolean isInitiator() {
        return muxId.isInitiator();
    }

    @NonNull
    public List<ByteBuffer> readFrames(ByteBuffer data) throws Exception {
        LogUtils.debug(TAG, "READ (0) Mux initiator " + muxId.isInitiator());
        byte[] decrypted = getDecodedData(data.array(), receiver);
        List<ByteBuffer> frames = DataHandler.decode(ByteBuffer.wrap(decrypted));
        LogUtils.debug(TAG, "Packages size " + frames.size());
        return frames;
    }

    @NonNull
    @Override
    public String toString() {
        return "MuxedStream{" +
                "muxId=" + muxId +
                ", muxFlag=" + muxFlag.name() +
                '}';
    }
}
