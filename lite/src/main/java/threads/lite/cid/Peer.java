package threads.lite.cid;

import androidx.annotation.NonNull;

import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public final class Peer {
    private final PeerId peerId;
    private final SortedSet<Multiaddr> multiaddrs = new TreeSet<>();
    private final ID id;
    private long latency = Long.MAX_VALUE;  // temporary value
    private boolean replaceable = true; // temporary value

    private Peer(@NonNull PeerId peerId, @NonNull ID id, @NonNull List<Multiaddr> multiaddrs) {
        this.id = id;
        this.peerId = peerId;
        this.multiaddrs.addAll(multiaddrs);
    }

    public static Peer create(@NonNull PeerId peerId, @NonNull List<Multiaddr> multiaddrs) throws Exception {
        return new Peer(peerId, ID.convertPeerID(peerId), multiaddrs);
    }

    public ID getID() {
        return id;
    }

    public long getLatency() {
        return latency;
    }

    public void setLatency(long latency) {
        this.latency = latency;
    }

    public PeerId getPeerId() {
        return peerId;
    }

    public Set<Multiaddr> getMultiaddrs() {
        return multiaddrs;
    }

    public boolean hasAddresses() {
        return !multiaddrs.isEmpty();
    }

    @NonNull
    @Override
    public String toString() {
        return "Peer{" +
                "peerId=" + peerId +
                ", multiaddrs=" + multiaddrs +
                ", latency=" + latency +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Peer peer = (Peer) o;
        return peerId.equals(peer.peerId) && multiaddrs.equals(peer.multiaddrs);
    }

    @Override
    public int hashCode() {
        return peerId.hashCode();
    }

    public boolean isReplaceable() {
        return replaceable;
    }

    public void setReplaceable(boolean replaceable) {
        this.replaceable = replaceable;
    }

}
