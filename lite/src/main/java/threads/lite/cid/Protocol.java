package threads.lite.cid;

import android.util.SparseArray;

import androidx.annotation.NonNull;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

import threads.lite.utils.DataHandler;

public enum Protocol {

    IP4(4, 32, "ip4"),
    TCP(6, 16, "tcp"),
    IP6(41, 128, "ip6"),
    DNS(53, -1, "dns"),
    DNS4(54, -1, "dns4"),
    DNS6(55, -1, "dns6"),
    DNSADDR(56, -1, "dnsaddr"),
    UDP(273, 16, "udp"),
    P2P(421, -1, "p2p"),
    P2PCIRCUIT(290, 0, "p2p-circuit"),
    QUIC(460, 0, "quic");

    public static final String IPV4_REGEX = "\\A(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)(\\.(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)){3}\\z";
    private static final Map<String, Protocol> byName = new HashMap<>();
    private static final SparseArray<Protocol> byCode = new SparseArray<>();

    static {
        for (Protocol t : Protocol.values()) {
            byName.put(t.getType(), t);
            byCode.put(t.code(), t);
        }
    }

    private final int code, size;
    private final String type;
    private final byte[] encoded;

    Protocol(int code, int size, String type) {
        this.code = code;
        this.size = size;
        this.type = type;
        this.encoded = encode(code);
    }

    public static Protocol get(String name) {
        if (byName.containsKey(name))
            return byName.get(name);
        throw new IllegalStateException("No protocol with name: " + name);
    }

    public static Protocol get(int code) {
        Protocol protocol = byCode.get(code);
        if (protocol == null) {
            throw new IllegalStateException("No protocol with code: " + code);
        }
        return protocol;
    }

    static byte[] encode(int code) {
        byte[] varint = new byte[(32 - Integer.numberOfLeadingZeros(code) + 6) / 7];
        putUvarint(varint, code);
        return varint;
    }

    static void putUvarint(byte[] buf, long x) {
        int i = 0;
        while (x >= 0x80) {
            buf[i] = (byte) (x | 0x80);
            x >>= 7;
            i++;
        }
        buf[i] = (byte) x;
    }


    public void appendCode(OutputStream out) throws IOException {
        out.write(encoded);
    }

    public int size() {
        return size;
    }

    public int code() {
        return code;
    }

    public String getType() {
        return type;
    }

    @NonNull
    @Override
    public String toString() {
        return name();
    }

    public byte[] addressToBytes(String addr) throws Exception {

        switch (this) {
            case IP4:
                if (!addr.matches(IPV4_REGEX))
                    throw new IllegalStateException("Invalid IPv4 address: " + addr);
                return Inet4Address.getByName(addr).getAddress();
            case IP6:
                return Inet6Address.getByName(addr).getAddress();
            case UDP:
                int x = Integer.parseInt(addr);
                if (x > 65535)
                    throw new IllegalStateException("Failed to parse " + name() + " address " + addr + " (> 65535");
                return new byte[]{(byte) (x >> 8), (byte) x};
            case P2P: {
                byte[] hashBytes = PeerId.fromBase58(addr).getBytes();

                try (ByteArrayOutputStream bout = new ByteArrayOutputStream()) {
                    byte[] varint = new byte[(32 - Integer.numberOfLeadingZeros(hashBytes.length) + 6) / 7];
                    putUvarint(varint, hashBytes.length);
                    bout.write(varint);
                    bout.write(hashBytes);
                    return bout.toByteArray();
                }
            }
            case DNS:
            case DNS4:
            case DNS6:
            case DNSADDR: {
                try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
                    byte[] hashBytes = addr.getBytes();
                    byte[] varint = new byte[(32 - Integer.numberOfLeadingZeros(hashBytes.length) + 6) / 7];
                    putUvarint(varint, hashBytes.length);
                    outputStream.write(varint);
                    outputStream.write(hashBytes);
                    return outputStream.toByteArray();
                }
            }
            default:
                throw new IllegalStateException("Unknown multiaddr type: " + name());
        }

    }

    public String readAddress(ByteBuffer in) throws Exception {
        int sizeForAddress = sizeForAddress(in);
        byte[] buf;
        switch (this) {
            case IP4:
            case IP6:
                buf = new byte[sizeForAddress];
                in.get(buf);
                return InetAddress.getByAddress(buf).toString().substring(1);
            case UDP:
                int a = in.get() & 0xFF;
                int b = in.get() & 0xFF;
                return Integer.toString((a << 8) | b);
            case P2P:
                buf = new byte[sizeForAddress];
                in.get(buf);
                return PeerId.create(buf).toBase58();
            case DNS:
            case DNS4:
            case DNS6:
            case DNSADDR:
                buf = new byte[sizeForAddress];
                in.get(buf);
                return new String(buf);
        }
        throw new IllegalStateException("Unimplemented protocol type: " + type);
    }

    public int sizeForAddress(ByteBuffer in) throws IOException {
        if (size > 0)
            return size / 8;
        if (size == 0)
            return 0;
        return DataHandler.readUnsignedLeb128(in);
    }
}
