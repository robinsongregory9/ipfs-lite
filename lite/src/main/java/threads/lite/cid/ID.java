package threads.lite.cid;

import androidx.annotation.NonNull;

import com.google.common.primitives.UnsignedBytes;

import java.security.MessageDigest;
import java.util.Arrays;


public final class ID implements Comparable<ID> {
    public final byte[] data;

    public ID(@NonNull byte[] data) {
        this.data = data;
    }

    @NonNull
    public static ID convertPeerID(@NonNull PeerId id) throws Exception {
        return convertKey(id.getBytes());
    }

    @NonNull
    public static ID convertKey(@NonNull byte[] id) throws Exception {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        return new ID(digest.digest(id));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ID id = (ID) o;
        return Arrays.equals(data, id.data);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(data);
    }

    @Override
    public int compareTo(@NonNull ID o) {
        return UnsignedBytes.lexicographicalComparator().compare(this.data, o.data);
    }
}
