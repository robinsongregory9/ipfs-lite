package threads.lite.cid;

import androidx.annotation.NonNull;
import androidx.room.TypeConverter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Objects;

import threads.lite.utils.DataHandler;

public final class Cid implements Comparable<Cid> {

    public static final int IDENTITY = 0x00;
    public static final int Raw = 0x55;
    public static final int DagProtobuf = 0x70;
    public static final int Libp2pKey = 0x72;

    private final byte[] multihash;

    private Cid(byte[] multihash) {
        this.multihash = multihash;
    }

    @TypeConverter
    public static Cid fromArray(byte[] data) {
        if (data == null) {
            throw new IllegalStateException("data is null");
        }
        return new Cid(data);
    }

    @TypeConverter
    public static byte[] toArray(Cid cid) {
        if (cid == null) {
            throw new IllegalStateException("cid is null");
        }
        return cid.bytes();
    }


    @NonNull
    public static Cid nsToCid(@NonNull String ns) throws Exception {
        return createCidV1(Raw, ns.getBytes(StandardCharsets.UTF_8));
    }

    @NonNull
    public static Cid decode(@NonNull String name) throws Exception {
        if (name.length() < 2) {
            throw new Exception("invalid cid");
        }

        if (name.length() == 46 && name.startsWith("Qm")) {
            Multihash hash = Multihash.fromBase58(name);
            Objects.requireNonNull(hash);
            return new Cid(hash.toBytes());
        }

        byte[] data = Multibase.decode(name);

        ByteBuffer wrap = ByteBuffer.wrap(data);

        int version = DataHandler.readUnsignedLeb128(wrap);
        if (version != 1) {
            throw new Exception("invalid version");
        }
        int codecType = DataHandler.readUnsignedLeb128(wrap);
        if (!(codecType == Cid.DagProtobuf || codecType == Cid.Raw ||
                codecType == Cid.Libp2pKey)) {
            throw new Exception("not supported codec");
        }

        Multihash mh = Multihash.deserialize(wrap);
        Objects.requireNonNull(mh);
        return new Cid(data);


    }

    @NonNull
    public static Cid createCidV0(byte[] data) throws Exception {

        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] hash = digest.digest(data);
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            Multihash.putUvarint(out, Multihash.Type.sha2_256.index);
            Multihash.putUvarint(out, hash.length);
            out.write(hash);
            return new Cid(out.toByteArray());
        }

    }

    @NonNull
    public static Cid createCidV1(int codec, byte[] data) throws Exception {

        MessageDigest digest = MessageDigest.getInstance("SHA-256");

        byte[] hash = digest.digest(data);

        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            Multihash.putUvarint(out, 1);
            Multihash.putUvarint(out, codec);
            Multihash.putUvarint(out, Multihash.Type.sha2_256.index);
            Multihash.putUvarint(out, hash.length);
            out.write(hash);
            return new Cid(out.toByteArray());
        }

    }

    @NonNull
    public static Cid newCidV1(int codec, byte[] mhash) throws IOException {

        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            Multihash.putUvarint(out, 1);
            Multihash.putUvarint(out, codec);
            out.write(mhash);
            return new Cid(out.toByteArray());
        }
    }

    @NonNull
    public static Multihash encode(byte[] hash, Multihash.Type type) {
        return new Multihash(type, hash);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cid cid = (Cid) o;
        return Arrays.equals(multihash, cid.multihash);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(multihash);
    }

    @NonNull
    public String String() {
        switch (getVersion()) {
            case 0:
                return Base58.encode(multihash);
            case 1:
                return Multibase.encode(Multibase.Base.Base32, multihash);
            default:
                throw new IllegalStateException("not supported version");
        }
    }

    public int getVersion() {
        byte[] bytes = multihash;
        if (bytes.length == 34 && bytes[0] == 18 && bytes[1] == 32) {
            return 0;
        }
        return 1;
    }

    public int getCodec() throws IOException {
        return getPrefix().getCodec();
    }

    public byte[] bytes() {
        return multihash;
    }


    @NonNull
    public Prefix getPrefix() throws IOException {
        if (getVersion() == 0) {
            return new Prefix(DagProtobuf, 32, Multihash.Type.sha2_256.index, 0);
        }
        return Prefix.getPrefixFromBytes(bytes());
    }

    @Override
    public int compareTo(Cid o) {
        return Integer.compare(this.hashCode(), o.hashCode());
    }

    // todo check if it is really needed getHash versus bytes
    public byte[] getHash() throws IOException {

        if (getVersion() == 0) {
            return multihash;
        } else {
            ByteBuffer wrap = ByteBuffer.wrap(bytes());
            DataHandler.readUnsignedLeb128(wrap); // skip version
            DataHandler.readUnsignedLeb128(wrap); // skip codec
            byte[] data = new byte[wrap.capacity() - wrap.position()];
            wrap.get(data);
            return data;
        }
    }

    public boolean isSupported() {
        try {
            return getPrefix().isSha2556();
        } catch (Throwable ignore) {
            return false;
        }
    }
}