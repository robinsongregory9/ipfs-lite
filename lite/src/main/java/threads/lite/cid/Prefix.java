package threads.lite.cid;

import androidx.annotation.NonNull;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Objects;

import threads.lite.utils.DataHandler;


public final class Prefix {

    private final int version;
    private final int codec;
    private final int mhType;
    private final int mhLength;

    public Prefix(int codec, int mhLength, int mhType, int version) {
        this.version = version;
        this.codec = codec;
        this.mhType = mhType;
        this.mhLength = mhLength;
    }

    public static Prefix getPrefixFromBytes(byte[] buf) throws IOException {
        ByteBuffer wrap = ByteBuffer.wrap(buf);

        int version = DataHandler.readUnsignedLeb128(wrap);
        if (version != 1 && version != 0) {
            throw new IOException("invalid version");
        }
        int codec = DataHandler.readUnsignedLeb128(wrap);
        if (!(codec == Cid.DagProtobuf || codec == Cid.Raw || codec == Cid.Libp2pKey)) {
            throw new IOException("not supported codec");
        }

        int mhtype = DataHandler.readUnsignedLeb128(wrap);

        int mhlen = DataHandler.readUnsignedLeb128(wrap);

        return new Prefix(codec, mhlen, mhtype, version);


    }

    @NonNull
    public static Cid sum(Prefix prefix, byte[] data) throws Exception {


        if (prefix.version == 0 && (!prefix.isSha2556()) ||
                (prefix.mhLength != 32 && prefix.mhLength != -1)) {
            throw new Exception("Invalid v0 Prefix");
        }

        if (!prefix.isSha2556()) {
            throw new IOException("Type Multihash " +
                    Multihash.Type.lookup(prefix.mhType).name() + " is not supported");
        }

        switch (prefix.version) {
            case 0:
                return Cid.createCidV0(data);
            case 1:
                return Cid.createCidV1(prefix.codec, data);
            default:
                throw new Exception("Invalid cid version");
        }


    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Prefix prefix = (Prefix) o;
        return version == prefix.version && codec == prefix.codec && mhType == prefix.mhType && mhLength == prefix.mhLength;
    }

    @Override
    public int hashCode() {
        return Objects.hash(version, codec, mhType, mhLength);
    }

    public Multihash.Type getType() {
        return Multihash.Type.lookup(mhType);
    }

    public boolean isSha2556() {
        return Multihash.Type.lookup(mhType) == Multihash.Type.sha2_256;
    }

    @NonNull
    @Override
    public String toString() {
        return "Prefix{" +
                "version=" + version +
                ", codec=" + codec +
                ", mhType=" + Multihash.Type.lookup(mhType).name() +
                ", mhLength=" + mhLength +
                '}';
    }

    public byte[] bytes() {

        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            Multihash.putUvarint(out, version);
            Multihash.putUvarint(out, codec);
            Multihash.putUvarint(out, mhType);
            Multihash.putUvarint(out, mhLength);
            return out.toByteArray();
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }

    public int getVersion() {
        return version;
    }

    public int getCodec() {
        return codec;
    }
}
