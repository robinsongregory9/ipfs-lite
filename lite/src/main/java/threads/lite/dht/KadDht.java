package threads.lite.dht;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.protobuf.ByteString;

import net.luminis.quic.QuicStream;

import java.net.ConnectException;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;

import dht.pb.Dht;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Cid;
import threads.lite.cid.ID;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Peer;
import threads.lite.cid.PeerId;
import threads.lite.cid.Protocol;
import threads.lite.core.Cancellable;
import threads.lite.core.Connection;
import threads.lite.core.Stream;
import threads.lite.core.StreamHandler;
import threads.lite.core.Transport;
import threads.lite.core.Validator;
import threads.lite.host.LiteHost;
import threads.lite.host.LiteTransport;
import threads.lite.ipns.IpnsService;
import threads.lite.utils.DataHandler;


public class KadDht {

    private static final String TAG = KadDht.class.getSimpleName();
    public final LiteHost host;
    public final PeerId self;
    @NonNull
    public final RoutingTable routingTable = new RoutingTable();
    @NonNull
    private final Set<PeerId> motherfuckers = ConcurrentHashMap.newKeySet();
    private final Validator validator;

    @NonNull
    private final ReentrantLock lock = new ReentrantLock();

    public KadDht(@NonNull LiteHost host, @NonNull Validator validator) {
        this.host = host;
        this.validator = validator;
        this.self = host.self();
    }

    public void clear() {
        motherfuckers.clear();
        routingTable.clear();
    }

    void bootstrap() {
        // Fill routing table with currently connected peers that are DHT servers
        if (routingTable.isEmpty()) {
            try {
                lock.lock();


                Set<String> peerIds = new HashSet<>(IPFS.DHT_BOOTSTRAP_NODES);

                Set<Multiaddr> bootstrap = host.getBootstrap();

                for (String name : peerIds) {
                    try {
                        PeerId peerId = PeerId.fromBase58(name);
                        Objects.requireNonNull(peerId);

                        List<Multiaddr> multiaddrs = new ArrayList<>();

                        for (Multiaddr addr : bootstrap) {
                            String cmpPeerId = addr.getStringComponent(Protocol.P2P).get(0);
                            if (Objects.equals(cmpPeerId, peerId.toBase58())) {
                                multiaddrs.add(addr);
                            }
                        }

                        if (!multiaddrs.isEmpty()) {
                            Peer peer = Peer.create(peerId, multiaddrs);
                            peer.setReplaceable(false);
                            routingTable.addPeer(peer);
                        }
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                }
            } finally {
                lock.unlock();
            }
        }
    }


    @NonNull
    private List<Peer> evalClosestPeers(@NonNull Dht.Message pms) {

        List<Peer> peers = new ArrayList<>();
        List<Dht.Message.Peer> list = pms.getCloserPeersList();
        for (Dht.Message.Peer entry : list) {
            List<Multiaddr> multiAddresses = new ArrayList<>();
            List<ByteString> addresses = entry.getAddrsList();
            for (ByteString address : addresses) {
                Multiaddr multiaddr = createMultiaddr(address);
                if (multiaddr != null) {
                    if (multiaddr.protocolSupported(host.protocol.get(), true)) {
                        if (!multiaddr.isCircuitAddress()) {
                            multiAddresses.add(multiaddr);
                        }
                    }
                }
            }

            if (!multiAddresses.isEmpty()) {
                try {
                    PeerId peerId = PeerId.create(entry.getId().toByteArray());
                    if (!motherfuckers.contains(peerId)) {
                        peers.add(Peer.create(peerId, multiAddresses));
                    }
                } catch (Throwable ignore) {
                    // peerId might be invalid
                }
            } else {
                LogUtils.info(TAG, "Ignore evalClosestPeers : " + multiAddresses);
            }
        }
        return peers;
    }


    private CompletableFuture<Void> getClosestPeers(@NonNull Cancellable cancellable,
                                                    @NonNull byte[] target,
                                                    @NonNull Consumer<List<Peer>> channel) {
        CompletableFuture<Void> result = new CompletableFuture<>();
        if (target.length == 0) {
            result.completeExceptionally(new RuntimeException("can't lookup empty key"));
            return result;
        }
        try {
            ID key = ID.convertKey(target);
            runQuery(cancellable, key, (ctx1, p) -> {

                Dht.Message pms = findPeerSingle(ctx1, p, target);

                List<Peer> peers = evalClosestPeers(pms);

                channel.accept(peers);

                return peers;
            }).whenComplete((unused, throwable) -> {
                if (throwable != null) {
                    result.completeExceptionally(throwable);
                } else {
                    result.complete(unused);
                }
            });

        } catch (Throwable throwable) {
            result.completeExceptionally(throwable);
        }
        return result;

    }

    public CompletableFuture<Void> putValue(@NonNull Cancellable cancellable,
                                            @NonNull byte[] key,
                                            @NonNull byte[] value) {

        CompletableFuture<Void> result = new CompletableFuture<>();


        try {
            bootstrap();

            // don't allow local users to put bad values.
            IpnsService.Entry entry = validator.validate(key, value);
            Objects.requireNonNull(entry);


            @SuppressLint("SimpleDateFormat") String format = new SimpleDateFormat(
                    IPFS.TIME_FORMAT_IPFS).format(new Date());

            Dht.Message.Record rec = Dht.Message.Record.newBuilder().setKey(ByteString.copyFrom(key))
                    .setValue(ByteString.copyFrom(value))
                    .setTimeReceived(format).build();

            Dht.Message pms = Dht.Message.newBuilder()
                    .setType(Dht.Message.MessageType.PUT_VALUE)
                    .setKey(rec.getKey())
                    .setRecord(rec)
                    .setClusterLevelRaw(0).build();


            Set<PeerId> handled = ConcurrentHashMap.newKeySet();

            getClosestPeers(cancellable, key, peers -> {


                ExecutorService service = Executors.newFixedThreadPool(
                        Runtime.getRuntime().availableProcessors());

                for (Peer peer : peers) {
                    if (peer.hasAddresses()) {
                        if (!handled.contains(peer.getPeerId())) {
                            handled.add(peer.getPeerId());
                            service.execute(() -> {
                                try {
                                    sendRequest(cancellable, peer, pms);
                                } catch (Throwable ignore) {
                                }
                            });
                        }
                    }
                }
                service.shutdown();
                try {
                    // waiting forever should never happen
                    boolean done = service.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
                    if (!done) {
                        service.shutdownNow();
                    }
                } catch (Throwable ignore) {
                }
            }).whenComplete((unused, throwable) -> {
                if (throwable != null) {
                    result.completeExceptionally(throwable);
                } else {
                    result.complete(unused);
                }
            });
        } catch (Throwable throwable) {
            result.completeExceptionally(throwable);
        }
        return result;

    }

    public CompletableFuture<Void> findProviders(@NonNull Cancellable cancellable,
                                                 @NonNull Consumer<Multiaddr> consumer,
                                                 @NonNull Cid cid) {
        CompletableFuture<Void> result = new CompletableFuture<>();


        try {
            bootstrap();

            byte[] target = cid.getHash();
            ID key = ID.convertKey(target);
            Set<Multiaddr> handledAddrs = ConcurrentHashMap.newKeySet();
            runQuery(cancellable, key, (ctx, p) -> {

                Dht.Message pms = findProvidersSingle(ctx, p, target);

                List<Dht.Message.Peer> list = pms.getProviderPeersList();
                for (Dht.Message.Peer entry : list) {
                    try {
                        PeerId peerId = PeerId.create(entry.getId().toByteArray());
                        List<ByteString> addresses = entry.getAddrsList();
                        for (ByteString address : addresses) {
                            Multiaddr multiaddr = createMultiaddr(address, peerId);
                            if (multiaddr != null) {
                                if (multiaddr.protocolSupported(
                                        host.protocol.get(), false)) {
                                    if (!handledAddrs.contains(multiaddr)) {
                                        handledAddrs.add(multiaddr);
                                        LogUtils.error(TAG, "findProviders " +
                                                " Cid Version : " + cid.getVersion() +
                                                " peer " + multiaddr);

                                        consumer.accept(multiaddr);
                                    }
                                }
                            }
                        }
                    } catch (Throwable ignore) {
                        // peerId might be invalid
                    }
                }
                return evalClosestPeers(pms);

            }).whenComplete((unused, throwable) -> {
                if (throwable != null) {
                    result.completeExceptionally(throwable);
                } else {
                    result.complete(unused);
                }
            });
        } catch (Throwable throwable) {
            result.completeExceptionally(throwable);
        }
        return result;

    }

    public void addToRouting(@NonNull QueryPeer peer) {
        routingTable.addPeer(peer);
    }

    public void removeFromRouting(QueryPeer peer) {
        boolean result = routingTable.removePeer(peer);
        if (result) {
            LogUtils.info(TAG, "Remove from routing " + peer);
        }
    }


    public CompletableFuture<Void> provide(@NonNull Cancellable cancellable, @NonNull Cid cid) {

        CompletableFuture<Void> result = new CompletableFuture<>();

        try {
            bootstrap();

            byte[] key = cid.getHash();

            Set<Multiaddr> addresses = host.listenAddresses();

            if (addresses.isEmpty()) {
                result.completeExceptionally(
                        new Exception("nothing to do here, no addresses to offer"));
                return result;
            }

            Dht.Message.Builder builder = Dht.Message.newBuilder()
                    .setType(Dht.Message.MessageType.ADD_PROVIDER)
                    .setKey(ByteString.copyFrom(key))
                    .setClusterLevelRaw(0);

            Dht.Message.Peer.Builder peerBuilder = Dht.Message.Peer.newBuilder()
                    .setId(ByteString.copyFrom(self.getBytes()));
            for (Multiaddr ma : addresses) {
                peerBuilder.addAddrs(ByteString.copyFrom(ma.getBytes()));
            }
            builder.addProviderPeers(peerBuilder.build());

            Dht.Message message = builder.build();


            Set<PeerId> handled = ConcurrentHashMap.newKeySet();

            getClosestPeers(cancellable, key, peers -> {

                List<Peer> notHandled = new ArrayList<>();
                for (Peer peer : peers) {
                    if (peer.hasAddresses()) {
                        if (!handled.contains(peer.getPeerId())) {
                            handled.add(peer.getPeerId());
                            notHandled.add(peer);
                        }
                    }
                }

                ExecutorService service = Executors.newFixedThreadPool(
                        Runtime.getRuntime().availableProcessors());

                for (Peer peer : notHandled) {
                    service.execute(() -> {

                        try {
                            if (motherfuckers.contains(peer.getPeerId())) {
                                return;
                            }

                            Connection connection = host.connect(host.createSession(), peer.getMultiaddrs(),
                                            IPFS.CONNECT_TIMEOUT, IPFS.CONNECT_TIMEOUT,
                                            0, 20480)
                                    .get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);

                            try {
                                sendMessage(connection, message).
                                        get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
                            } catch (Throwable ignore) {
                                // ignore
                            } finally {
                                connection.close();
                            }

                        } catch (ExecutionException | TimeoutException exception) {
                            motherfuckers.add(peer.getPeerId());
                        } catch (InterruptedException ignore) {
                        }
                    });

                }
                service.shutdown();
                try {
                    // waiting forever should never happen
                    boolean done = service.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
                    if (!done) {
                        service.shutdownNow();
                    }
                } catch (Throwable ignore) {
                }

            }).whenComplete((unused, throwable) -> {
                if (throwable != null) {
                    result.completeExceptionally(throwable);
                } else {
                    result.complete(unused);
                }
            });
        } catch (Throwable throwable) {
            result.completeExceptionally(throwable);
        }
        return result;
    }

    private CompletableFuture<Connection> sendMessage(@NonNull Connection connection,
                                                      @NonNull Dht.Message message) {
        CompletableFuture<Connection> done = new CompletableFuture<>();

        connection.createStream(new StreamHandler() {
                    @Override
                    public void throwable(Stream stream, Throwable throwable) {
                        done.completeExceptionally(throwable);
                    }

                    @Override
                    public void token(Stream stream, String token) throws Exception {
                        if (!Arrays.asList(IPFS.STREAM_PROTOCOL,
                                IPFS.DHT_PROTOCOL).contains(token)) {
                            throw new Exception("Token " + token + " not supported");
                        }
                        if (Objects.equals(token, IPFS.DHT_PROTOCOL)) {
                            stream.writeOutput(DataHandler.encode(message))
                                    .thenApply(Stream::closeOutput)
                                    .thenRun(() -> done.complete(connection)); // this is not 100% correct
                        }
                    }

                    @Override
                    public void fin() {
                        LogUtils.error(TAG, "fin sendMessage invoked");
                    }

                    @Override
                    public void data(Stream stream, ByteBuffer data) {
                        LogUtils.error(TAG, "data sendMessage invoked");
                    }

                    @NonNull
                    @Override
                    public Transport getTransport(QuicStream quicStream) {
                        return new LiteTransport();
                    }

                    @Override
                    public void streamTerminated(QuicStream quicStream) {
                        LogUtils.error(TAG, "todo stream terminated");
                    }

                })
                .thenApply(quicStream -> quicStream.writeOutput(
                        DataHandler.encodeTokens(IPFS.STREAM_PROTOCOL, IPFS.DHT_PROTOCOL)));

        return done;
    }

    private Dht.Message sendRequest(@NonNull Connection connection, @NonNull Peer peer,
                                    @NonNull Dht.Message message)
            throws CancellationException, ConnectException {

        try {
            AtomicLong latency = new AtomicLong(System.currentTimeMillis());
            CompletableFuture<Dht.Message> done = new CompletableFuture<>();

            connection.createStream(new StreamHandler() {
                @Override
                public void throwable(Stream stream, Throwable throwable) {
                    done.completeExceptionally(throwable);
                }

                @Override
                public void token(Stream stream, String token) throws Exception {
                    if (!Arrays.asList(IPFS.STREAM_PROTOCOL, IPFS.DHT_PROTOCOL).contains(token)) {
                        throw new Exception("Token " + token + " not supported");
                    }
                    if (Objects.equals(token, IPFS.DHT_PROTOCOL)) {
                        stream.writeOutput(DataHandler.encode(message))
                                .thenApply(Stream::closeOutput);
                        peer.setLatency(System.currentTimeMillis() - latency.get());
                    }
                }

                @Override
                public void fin() {
                    // nothing to do here
                }

                @Override
                public void data(Stream stream, ByteBuffer data) throws Exception {
                    done.complete(Dht.Message.parseFrom(data.array()));
                }

                @NonNull
                @Override
                public Transport getTransport(QuicStream quicStream) {
                    return new LiteTransport();
                }

                @Override
                public void streamTerminated(QuicStream quicStream) {
                    LogUtils.error(TAG, "todo stream terminated");
                }

            }).thenApply(quicStream ->
                    quicStream.writeOutput(
                            DataHandler.encodeTokens(IPFS.STREAM_PROTOCOL, IPFS.DHT_PROTOCOL)));

            Dht.Message msg = done.get(IPFS.DHT_REQUEST_READ_TIMEOUT, TimeUnit.SECONDS);
            Objects.requireNonNull(msg);

            return msg;
        } catch (ExecutionException | TimeoutException exception) {
            LogUtils.debug(TAG, "Request " + connection.getRemoteAddress() + " : " +
                    exception.getClass().getSimpleName() +
                    " : " + exception.getMessage());
            throw new ConnectException(exception.getClass().getSimpleName());
        } catch (InterruptedException interruptedException) {
            throw new CancellationException(interruptedException.getMessage());
        } finally {
            connection.close();
        }
    }

    private Dht.Message sendRequest(@NonNull Cancellable cancellable, @NonNull Peer peer,
                                    @NonNull Dht.Message message)
            throws CancellationException, ConnectException {

        CompletableFuture<Dht.Message> done = new CompletableFuture<>();
        if (cancellable.isCancelled()) {
            throw new CancellationException();
        }
        if (motherfuckers.contains(peer.getPeerId())) {
            throw new ConnectException("peer can not be connected too");
        }
        try {
            host.connect(host.createSession(), peer.getMultiaddrs(), IPFS.CONNECT_TIMEOUT,
                            IPFS.CONNECT_TIMEOUT, 0, IPFS.MESSAGE_SIZE_MAX)
                    .whenComplete((connection, throwable) -> {
                        if (throwable != null) {
                            motherfuckers.add(peer.getPeerId());
                            done.completeExceptionally(throwable);
                        } else {
                            try {
                                done.complete(sendRequest(connection, peer, message));
                            } catch (Throwable error) {
                                done.completeExceptionally(error);
                            }
                        }

                    });
            return done.get(IPFS.DHT_REQUEST_READ_TIMEOUT + IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
        } catch (InterruptedException interruptedException) {
            throw new CancellationException(interruptedException.getMessage());
        } catch (Throwable throwable) {
            throw new ConnectException(throwable.getMessage());
        }
    }


    private Dht.Message getValueSingle(@NonNull Cancellable ctx, @NonNull Peer peer, @NonNull byte[] key)
            throws CancellationException, ConnectException {
        Dht.Message pms = Dht.Message.newBuilder()
                .setType(Dht.Message.MessageType.GET_VALUE)
                .setKey(ByteString.copyFrom(key))
                .setClusterLevelRaw(0).build();
        return sendRequest(ctx, peer, pms);
    }

    private Dht.Message findPeerSingle(@NonNull Cancellable ctx, @NonNull Peer peer, @NonNull byte[] key)
            throws CancellationException, ConnectException {
        Dht.Message pms = Dht.Message.newBuilder()
                .setType(Dht.Message.MessageType.FIND_NODE)
                .setKey(ByteString.copyFrom(key))
                .setClusterLevelRaw(0).build();

        return sendRequest(ctx, peer, pms);
    }

    private Dht.Message findProvidersSingle(@NonNull Cancellable ctx, @NonNull Peer peer, @NonNull byte[] key)
            throws CancellationException, ConnectException {
        Dht.Message pms = Dht.Message.newBuilder()
                .setType(Dht.Message.MessageType.GET_PROVIDERS)
                .setKey(ByteString.copyFrom(key))
                .setClusterLevelRaw(0).build();
        return sendRequest(ctx, peer, pms);
    }


    @Nullable
    private Multiaddr createMultiaddr(@NonNull ByteString address) {
        try {
            return Multiaddr.create(address);
        } catch (Throwable ignore) {
            // ignore
        }
        return null;
    }

    @Nullable
    private Multiaddr createMultiaddr(@NonNull ByteString raw, @NonNull PeerId peerId) {
        try {
            return Multiaddr.create(raw, peerId);
        } catch (Throwable ignore) {
            // ignore
        }
        return null;
    }


    public CompletableFuture<Void> findPeer(@NonNull Cancellable cancellable,
                                            @NonNull Consumer<Multiaddr> consumer,
                                            @NonNull PeerId id) {

        CompletableFuture<Void> result = new CompletableFuture<>();

        try {
            bootstrap();

            byte[] target = id.getBytes();
            ID key = ID.convertKey(target);

            Set<Multiaddr> handled = ConcurrentHashMap.newKeySet();
            runQuery(cancellable, key, (ctx, p) -> {

                Dht.Message pms = findPeerSingle(ctx, p, target);

                List<Dht.Message.Peer> list = pms.getCloserPeersList();
                for (Dht.Message.Peer entry : list) {
                    try {
                        PeerId peerId = PeerId.create(entry.getId().toByteArray());
                        if (Objects.equals(peerId, id)) {
                            List<ByteString> addresses = entry.getAddrsList();
                            for (ByteString address : addresses) {
                                Multiaddr multiaddr = createMultiaddr(address, peerId);
                                if (multiaddr != null) {
                                    if (multiaddr.protocolSupported(
                                            host.protocol.get(), false)) {
                                        if (!handled.contains(multiaddr)) {
                                            handled.add(multiaddr);
                                            consumer.accept(multiaddr);
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Throwable ignore) {
                        // peerId might be invalid
                    }
                }
                return evalClosestPeers(pms);

            }).whenComplete((unused, throwable) -> {
                if (throwable != null) {
                    result.completeExceptionally(throwable);
                } else {
                    result.complete(unused);
                }
            });
        } catch (Throwable throwable) {
            result.completeExceptionally(throwable);
        }

        return result;
    }

    private CompletableFuture<Void> runQuery(@NonNull Cancellable cancellable, @NonNull ID key,
                                             @NonNull QueryFunc queryFn) {
        // pick the K closest peers to the key in our Routing table.
        List<QueryPeer> seedPeers = routingTable.nearestPeers(key);
        return Query.runQuery(this, cancellable, key, seedPeers, queryFn);

    }

    private List<Peer> getRecordOfPeers(@NonNull Cancellable cancellable,
                                        @NonNull Peer peer,
                                        @NonNull Consumer<IpnsService.Entry> consumer,
                                        @NonNull byte[] key)
            throws CancellationException, ConnectException {


        Dht.Message pms = getValueSingle(cancellable, peer, key);

        List<Peer> peers = evalClosestPeers(pms);

        if (pms.hasRecord()) {

            Dht.Message.Record rec = pms.getRecord();
            try {
                byte[] record = rec.getValue().toByteArray();
                if (record != null && record.length > 0) {
                    IpnsService.Entry entry = validator.validate(rec.getKey().toByteArray(), record);
                    consumer.accept(entry);
                }
            } catch (Throwable throwable) {
                peer.setLatency(Long.MAX_VALUE);
                LogUtils.error(TAG, throwable);
            }
        }

        return peers;
    }

    private CompletableFuture<Void> getValues(@NonNull Cancellable cancellable, @NonNull Consumer<IpnsService.Entry> consumer,
                                              @NonNull ID key, @NonNull byte[] target) {
        return runQuery(cancellable, key, (ctx1, peer) -> getRecordOfPeers(ctx1, peer, consumer, target));
    }


    private void processValues(@Nullable IpnsService.Entry best,
                               @NonNull IpnsService.Entry current,
                               @NonNull Consumer<IpnsService.Entry> reporter) {

        if (best != null) {
            int value = validator.compare(best, current);
            if (value == -1) { // "current" is newer entry
                reporter.accept(current);
            }
        } else {
            reporter.accept(current);
        }
    }


    public CompletableFuture<Void> searchValue(@NonNull Cancellable cancellable,
                                               @NonNull Consumer<IpnsService.Entry> consumer,
                                               @NonNull byte[] target) {
        CompletableFuture<Void> result = new CompletableFuture<>();
        try {
            bootstrap();

            AtomicReference<IpnsService.Entry> best = new AtomicReference<>();
            ID key = ID.convertKey(target);

            getValues(cancellable, entry -> processValues(best.get(), entry, (current) -> {
                consumer.accept(current);
                best.set(current);
            }), key, target).whenComplete((unused, throwable) -> {
                if (throwable != null) {
                    result.completeExceptionally(throwable);
                } else {
                    result.complete(unused);
                }
            });
        } catch (Throwable throwable) {
            result.completeExceptionally(throwable);
        }

        return result;
    }


    public interface QueryFunc {
        @NonNull
        List<Peer> query(@NonNull Cancellable cancellable, @NonNull Peer peer)
                throws CancellationException, ConnectException;
    }


}
