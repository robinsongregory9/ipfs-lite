package threads.lite.utils;

import androidx.annotation.NonNull;

import com.google.protobuf.MessageLite;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import threads.lite.cid.Multihash;
import threads.lite.core.Frame;
import threads.lite.core.FrameReader;
import threads.lite.core.Transport;
import threads.lite.host.LiteTransport;

public class DataHandler {

    public static int unsignedLeb128Size(int value) {
        int remaining = value >> 7;
        int count = 0;
        while (remaining != 0) {
            remaining >>= 7;
            count++;
        }
        return count + 1;
    }

    public static void writeUnsignedLeb128(ByteBuffer out, int value) {
        int remaining = value >>> 7;
        while (remaining != 0) {
            out.put((byte) ((value & 0x7f) | 0x80));
            value = remaining;
            remaining >>>= 7;
        }
        out.put((byte) (value & 0x7f));
    }

    public static int readUnsignedLeb128(ByteBuffer in) throws IOException {
        int result = 0;
        int cur;
        int count = 0;
        do {
            cur = in.get() & 0xff;
            result |= (cur & 0x7f) << (count * 7);
            count++;
        } while (((cur & 0x80) == 0x80) && count < 5);
        if ((cur & 0x80) == 0x80) {
            throw new IOException("invalid LEB128 sequence");
        }
        return result;
    }

    public static boolean isToken(byte[] data) {
        if (data.length > 2) {
            if (data[0] == '/' && data[data.length - 1] == '\n') {
                return true;
            } else if (data[0] == 'n' && data[1] == 'a' && data[2] == '\n') {
                return true;
            } else return data[0] == 'l' && data[1] == 's' && data[2] == '\n';
        }
        return false;
    }

    public static byte[] encode(@NonNull MessageLite message) {
        try (ByteArrayOutputStream buf = new ByteArrayOutputStream()) {
            message.writeDelimitedTo(buf);
            return buf.toByteArray();
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }

    // might be expensive
    public static byte[] encode(byte[] data) {
        try (ByteArrayOutputStream buf = new ByteArrayOutputStream()) {
            Multihash.putUvarint(buf, data.length);
            buf.write(data);
            return buf.toByteArray();
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }


    public static byte[] encodeTokens(String... tokens) {

        try (ByteArrayOutputStream buf = new ByteArrayOutputStream()) {
            for (String token : tokens) {
                byte[] data = token.getBytes(StandardCharsets.UTF_8);
                Multihash.putUvarint(buf, data.length + 1);
                buf.write(data);
                buf.write('\n');
            }
            return buf.toByteArray();
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }

    public static List<ByteBuffer> decode(ByteBuffer bytes) throws Exception {
        Transport transport = new LiteTransport();
        List<ByteBuffer> frames = new ArrayList<>();

        FrameReader frameReader = Transport.getFrameReader(transport);
        Frame frame = frameReader.getFrame(bytes);

        if (frame.getLength() < 0) {
            throw new Exception("invalid length of < 0");
        } else if (frame.getLength() == 0) {
            frames.add(frame.getData());
        } else {
            int read = Math.min(frame.getLength(), bytes.remaining());
            for (int i = 0; i < read; i++) {
                frame.put(bytes.get());
            }
            if (read == frame.getLength()) {
                frames.add(frame.getData());
            }
            // check for a next iteration
            if (bytes.remaining() > 0) {
                frames.addAll(decode(bytes));
            }
        }
        return frames;
    }

}


