package threads.lite.utils;


import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;

import merkledag.pb.Merkledag;
import threads.lite.cid.Cid;
import threads.lite.core.Cancellable;
import threads.lite.dag.Adder;
import threads.lite.dag.BlockService;
import threads.lite.dag.DagReader;
import threads.lite.dag.Directory;
import threads.lite.dag.NodeService;
import threads.lite.format.Block;
import threads.lite.format.BlockStore;
import threads.lite.format.Link;
import threads.lite.host.Session;
import unixfs.pb.Unixfs;


public class Stream {


    private static Adder getFileAdder(@NonNull BlockStore blockStore) {
        return Adder.createAdder(blockStore);
    }

    public static boolean isDir(@NonNull Cancellable cancellable,
                                @NonNull BlockStore blockstore,
                                @NonNull Session session,
                                @NonNull Cid cid) throws Exception {


        BlockService blockservice = BlockService.createBlockService(blockstore, session);
        NodeService nodeService = NodeService.createNodeService(blockservice);

        Merkledag.PBNode node = Resolver.resolveNode(cancellable, nodeService, cid);
        Objects.requireNonNull(node);
        return Directory.isDirectory(node);
    }

    @NonNull
    public static Cid createEmptyDirectory(@NonNull BlockStore storage) throws Exception {
        Adder fileAdder = getFileAdder(storage);
        return fileAdder.createEmptyDirectory();
    }

    @NonNull
    public static Cid addLinkToDirectory(@NonNull BlockStore storage,
                                         @NonNull Cid directory,
                                         @NonNull Link link) throws Exception {

        Adder fileAdder = getFileAdder(storage);
        Block block = storage.getBlock(directory);
        Objects.requireNonNull(block, "Block not local available");
        Merkledag.PBNode dirNode = block.getNode();
        Objects.requireNonNull(dirNode);
        return fileAdder.addChild(dirNode, link);

    }

    @NonNull
    public static Cid createDirectory(@NonNull BlockStore storage, @NonNull List<Link> links) throws Exception {
        Adder fileAdder = getFileAdder(storage);
        return fileAdder.createDirectory(links);
    }

    @NonNull
    public static Cid removeFromDirectory(@NonNull BlockStore storage,
                                          @NonNull Cid directory,
                                          @NonNull String name) throws Exception {

        Adder fileAdder = getFileAdder(storage);
        Block block = storage.getBlock(directory);
        Objects.requireNonNull(block, "Block not local available");
        Merkledag.PBNode dirNode = block.getNode();
        Objects.requireNonNull(dirNode);
        return fileAdder.removeChild(dirNode, name);
    }

    @NonNull
    public static List<Cid> getBlocks(@NonNull BlockStore blockstore, @NonNull Cid cid) throws Exception {
        List<Cid> result = new ArrayList<>();

        Block block = blockstore.getBlock(cid);
        Objects.requireNonNull(block, "Block not local available");
        Merkledag.PBNode node = block.getNode();
        List<Merkledag.PBLink> links = node.getLinksList();

        for (Merkledag.PBLink link : links) {
            Cid child = Cid.fromArray(link.getHash().toByteArray());
            result.add(child);
            result.addAll(getBlocks(blockstore, child));
        }

        return result;
    }

    public static void ls(@NonNull Cancellable cancellable, @NonNull Consumer<Link> consumer,
                          @NonNull BlockStore blockstore, @NonNull Session session,
                          @NonNull Cid cid, boolean resolveChildren)
            throws Exception {

        BlockService blockservice = BlockService.createBlockService(blockstore, session);
        NodeService nodeService = NodeService.createNodeService(blockservice);


        Merkledag.PBNode node = Resolver.resolveNode(cancellable, nodeService, cid);
        Objects.requireNonNull(node);
        List<Merkledag.PBLink> links = node.getLinksList();
        for (Merkledag.PBLink link : links) {
            processLink(cancellable, consumer, nodeService, link, resolveChildren);
        }
    }

    public static boolean hasLink(@NonNull Cancellable cancellable,
                                  @NonNull BlockStore blockstore,
                                  @NonNull Session session,
                                  @NonNull Cid cid,
                                  @NonNull String name) throws Exception {

        BlockService blockservice = BlockService.createBlockService(blockstore, session);
        NodeService nodeService = NodeService.createNodeService(blockservice);

        Merkledag.PBNode node = Resolver.resolveNode(cancellable, nodeService, cid);
        Objects.requireNonNull(node);
        Merkledag.PBLink link = DagReader.getLinkByName(node, name);
        return link != null;
    }

    @NonNull
    public static Cid readInputStream(@NonNull BlockStore storage,
                                      @NonNull ReaderInputStream readerInputStream) throws Exception {

        Adder fileAdder = getFileAdder(storage);
        return fileAdder.createFromStream(readerInputStream);
    }

    private static void processLink(@NonNull Cancellable cancellable,
                                    @NonNull Consumer<Link> consumer,
                                    @NonNull NodeService nodeService,
                                    @NonNull Merkledag.PBLink link, boolean resolveChildren)
            throws Exception {

        String name = link.getName();
        long size = link.getTsize();
        Cid cid = Cid.fromArray(link.getHash().toByteArray());


        if (!resolveChildren) {
            consumer.accept(Link.create(cid, name, size, Link.Unknown));
        } else {

            Merkledag.PBNode linkNode = nodeService.getNode(cancellable, cid);

            Unixfs.Data data = DagReader.getData(linkNode);
            int type;
            switch (data.getType()) {
                case File:
                    type = Link.File;
                    break;
                case Raw:
                    type = Link.Raw;
                    break;
                case Directory:
                    type = Link.Dir;
                    break;
                case Symlink:
                case HAMTShard:
                case Metadata:
                default:
                    type = Link.Unknown;
            }
            size = data.getFilesize();
            consumer.accept(Link.create(cid, name, size, type));


        }

    }
}
