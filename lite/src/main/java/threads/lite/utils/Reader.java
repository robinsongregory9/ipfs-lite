package threads.lite.utils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.protobuf.ByteString;

import java.util.Objects;

import merkledag.pb.Merkledag;
import threads.lite.cid.Cid;
import threads.lite.core.Cancellable;
import threads.lite.dag.BlockService;
import threads.lite.dag.DagReader;
import threads.lite.dag.NodeService;
import threads.lite.format.BlockStore;
import threads.lite.host.Session;

public class Reader {

    private final DagReader dagReader;
    private final Cancellable cancellable;


    private Reader(@NonNull Cancellable cancellable, @NonNull DagReader dagReader) {
        this.cancellable = cancellable;
        this.dagReader = dagReader;
    }

    public static Reader getReader(@NonNull Cancellable cancellable, @NonNull BlockStore blockstore,
                                   @NonNull Session session, @NonNull Cid cid)
            throws Exception {
        BlockService blockservice = BlockService.createBlockService(blockstore, session);
        NodeService dags = NodeService.createNodeService(blockservice);
        Merkledag.PBNode top = Resolver.resolveNode(cancellable, dags, cid);
        Objects.requireNonNull(top);
        DagReader dagReader = DagReader.create(top, dags);

        return new Reader(cancellable, dagReader);
    }

    @Nullable
    public ByteString loadNextData() throws Exception {
        return dagReader.loadNextData(cancellable);
    }

    public void seek(long position) throws Exception {
        dagReader.seek(cancellable, position);
    }

    public long getSize() {
        return this.dagReader.getSize();
    }
}
