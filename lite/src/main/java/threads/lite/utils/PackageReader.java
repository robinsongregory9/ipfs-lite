package threads.lite.utils;

import androidx.annotation.NonNull;

import net.luminis.quic.QuicStream;
import net.luminis.quic.RawStreamData;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;

import threads.lite.LogUtils;
import threads.lite.core.Frame;
import threads.lite.core.FrameReader;
import threads.lite.core.Package;
import threads.lite.core.Stream;
import threads.lite.core.StreamHandler;
import threads.lite.core.Transport;
import threads.lite.mplex.MuxFlag;
import threads.lite.mplex.MuxedStream;
import threads.lite.noise.SecuredStream;

public class PackageReader implements Consumer<RawStreamData> {
    private static final String TAG = PackageReader.class.getSimpleName();
    @NonNull
    private final StreamHandler streamHandler;
    @NonNull
    private final State state = new State();

    public PackageReader(@NonNull StreamHandler streamHandler) {
        this.streamHandler = streamHandler;
    }

    public static void acceptPackage(@NonNull StreamHandler streamHandler,
                                     @NonNull Package aPackage) throws Exception {
        Stream stream = aPackage.getStream();
        if (stream instanceof MuxedStream) {
            MuxedStream muxedStream = (MuxedStream) stream;
            MuxFlag muxFlag = muxedStream.getMuxFlag();
            switch (muxFlag) {
                case OPEN: {

                    byte[] decoded = MuxedStream.getDecodedData(aPackage.getData().array(),
                            muxedStream.getReceiver());
                    LogUtils.error(TAG, "New MuxStream data " + new String(decoded));

                    // todo this seems a shitty hack (and not working)
                    if (!Objects.equals(new String(decoded),
                            String.valueOf(muxedStream.getMuxId().getStreamId()))) {
                        List<ByteBuffer> frames = DataHandler.decode(ByteBuffer.wrap(decoded));
                        accept(streamHandler, muxedStream, frames, aPackage.isFinal());
                    }

                    LogUtils.error(TAG, "New MuxStream " + muxedStream);
                    break;
                }
                case DATA: {
                    LogUtils.error(TAG, "Data MuxStream " + muxedStream);
                    accept(streamHandler, muxedStream,
                            muxedStream.readFrames(aPackage.getData()), aPackage.isFinal());
                    break;
                }
                case RESET:
                case CLOSE: {
                    byte[] decoded = MuxedStream.getDecodedData(aPackage.getData().array(),
                            muxedStream.getReceiver());
                    LogUtils.error(TAG, "Close MuxStream data " + new String(decoded));
                    LogUtils.error(TAG, "Close MuxStream " + muxedStream);
                    break;
                }
                default:
                    throw new IllegalStateException("not expected mux frame flag");
            }
        } else if (stream instanceof SecuredStream) {
            SecuredStream securedStream = (SecuredStream) stream;
            accept(streamHandler, securedStream, securedStream.readFrames(
                    aPackage.getData()), aPackage.isFinal());
        } else {
            accept(streamHandler, aPackage);
        }
    }

    private static void accept(StreamHandler streamHandler, Stream stream,
                               List<ByteBuffer> frames, boolean isFinal) {
        for (ByteBuffer frame : frames) {
            accept(streamHandler, new Package() {
                @Override
                public Stream getStream() {
                    return stream;
                }

                @Override
                public ByteBuffer getData() {
                    return frame;
                }

                @Override
                public boolean isFinal() {
                    return isFinal;
                }
            });
        }
    }

    public static void accept(@NonNull StreamHandler streamHandler, @NonNull Package aPackage) {
        try {
            ByteBuffer frame = aPackage.getData();

            byte[] data = frame.array();
            // expected to be for a token
            if (DataHandler.isToken(data)) {
                String token = new String(data, StandardCharsets.UTF_8);
                token = token.substring(0, data.length - 1);
                try {
                    streamHandler.token(aPackage.getStream(), token);
                } catch (Throwable throwable) {
                    streamHandler.throwable(aPackage.getStream(), throwable);
                }
            } else {
                streamHandler.data(aPackage.getStream(), frame);
            }

            if (aPackage.isFinal()) {
                streamHandler.fin();
            }

        } catch (Throwable exception) {
            LogUtils.error(TAG, exception);
            streamHandler.throwable(aPackage.getStream(), exception);
        }
    }

    private void iteration(@NonNull State state, @NonNull QuicStream quicStream,
                           @NonNull ByteBuffer bytes, @NonNull Consumer<Frame> frameConsumer) {
        try {
            if (!state.reset) {
                if (state.length() == 0) {
                    Transport transport = streamHandler.getTransport(quicStream);
                    FrameReader frameReader = Transport.getFrameReader(transport);
                    state.frame = frameReader.getFrame(bytes);
                    if (state.length() < 0) {
                        state.frame = null;
                        throw new Exception("invalid length of < 0");
                    } else if (state.length() == 0) {
                        Frame frame = Objects.requireNonNull(state.frame);
                        state.frame = null;
                        frameConsumer.accept(frame);
                    } else {

                        int read = Math.min(state.length(), bytes.remaining());
                        for (int i = 0; i < read; i++) {
                            state.frame.put(bytes.get());
                        }

                        if (read == state.length()) {
                            Frame frame = Objects.requireNonNull(state.frame);
                            state.frame = null;
                            frameConsumer.accept(frame);
                        }

                        // check for a next iteration
                        // check for a next iteration
                        if (bytes.remaining() > 0) {
                            iteration(state, quicStream, bytes, frameConsumer);
                        }
                    }
                } else {
                    Frame frame = Objects.requireNonNull(state.frame);
                    frame.put(bytes.array());
                    if (frame.position() == frame.getLength()) {
                        state.frame = null;
                        frameConsumer.accept(frame);
                    }
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, "Expected Length " + state.length());
            LogUtils.error(TAG, "Data " + bytes);
            LogUtils.error(TAG, throwable);
            state.reset();
        }
    }

    @Override
    public void accept(RawStreamData rawStreamData) {

        QuicStream quicStream = rawStreamData.getStream();

        if (rawStreamData.isTerminated()) {
            state.reset();
            streamHandler.streamTerminated(quicStream);
            return;
        }

        iteration(state, quicStream, rawStreamData.getStreamData(),
                frame -> {
                    try {
                        // upgrade stream
                        Stream stream = streamHandler.getTransport(quicStream)
                                .getStream(quicStream, frame);

                        acceptPackage(streamHandler, new Package() {
                            @Override
                            public Stream getStream() {
                                return stream;
                            }

                            @Override
                            public ByteBuffer getData() {
                                return frame.getData();
                            }

                            @Override
                            public boolean isFinal() {
                                return rawStreamData.isFinal();
                            }

                        });
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                        state.reset();
                    }
                });


        if (rawStreamData.isFinal()) {
            state.reset();
        }
    }

    public static class State {

        public Frame frame = null;
        public boolean reset = false;

        public void reset() {
            frame = null;
            reset = true;
        }

        public int length() {
            if (frame != null) {
                return frame.getLength();
            }
            return 0;
        }

    }
}
