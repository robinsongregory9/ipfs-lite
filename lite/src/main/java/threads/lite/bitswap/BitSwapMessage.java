package threads.lite.bitswap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.protobuf.ByteString;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bitswap.pb.MessageOuterClass;
import merkledag.pb.Merkledag;
import threads.lite.cid.Cid;
import threads.lite.cid.Prefix;
import threads.lite.format.Block;
import threads.lite.format.BlockStore;


public final class BitSwapMessage {

    private final HashMap<Cid, Block> blocks = new HashMap<>();
    private final List<Cid> haves = new ArrayList<>();


    public static BitSwapMessage create(MessageOuterClass.Message pbm) throws Exception {
        BitSwapMessage m = new BitSwapMessage();


        // deprecated
        for (ByteString data : pbm.getBlocksList()) {
            // CIDv0, sha256, protobuf only
            Block block = Block.createBlock(Merkledag.PBNode.parseFrom(data.toByteArray()));
            m.blocks.put(block.getCid(), block);
        }

        for (MessageOuterClass.Message.Block b : pbm.getPayloadList()) {
            ByteString prefix = b.getPrefix();
            Prefix pref = Prefix.getPrefixFromBytes(prefix.toByteArray());
            Cid cid = Prefix.sum(pref, b.getData().toByteArray());
            Block block = Block.createBlockWithCid(cid,
                    Merkledag.PBNode.parseFrom(b.getData().toByteArray()));
            m.blocks.put(block.getCid(), block);
        }

        for (MessageOuterClass.Message.BlockPresence bi : pbm.getBlockPresencesList()) {
            Cid cid = Cid.fromArray(bi.getCid().toByteArray());
            if (!m.blocks.containsKey(cid)) {
                if (bi.getType() == MessageOuterClass.Message.BlockPresenceType.Have) {
                    m.haves.add(cid);
                }
            }
        }

        return m;
    }


    @Nullable
    public static MessageOuterClass.Message response(@NonNull BlockStore blockstore,
                                                     @NonNull MessageOuterClass.Message bsm)
            throws IOException {

        HashMap<Cid, byte[]> datas = new HashMap<>();
        HashMap<Cid, MessageOuterClass.Message.BlockPresenceType> blockPresences = new HashMap<>();

        List<MessageOuterClass.Message.Wantlist.Entry> wants = new ArrayList<>();
        for (MessageOuterClass.Message.Wantlist.Entry et : bsm.getWantlist().getEntriesList()) {
            if (!et.getCancel()) {
                wants.add(et);
            }
        }


        for (MessageOuterClass.Message.Wantlist.Entry entry : wants) {
            // For each want-have / want-block
            Cid cid = Cid.fromArray(entry.getBlock().toByteArray());

            byte[] data = blockstore.getData(cid);
            if (data == null) {
                // Only add the task to the queue if the requester wants a DONT_HAVE
                if (entry.getSendDontHave()) {
                    // Add DONT_HAVEs to the message
                    blockPresences.put(cid, MessageOuterClass.Message.BlockPresenceType.DontHave);
                }
            } else {
                datas.put(cid, data);
                boolean isWantBlock =
                        entry.getWantType() == MessageOuterClass.Message.Wantlist.WantType.Block;

                if (isWantBlock) {
                    blockPresences.remove(cid);
                } else {
                    // Add HAVES to the message
                    blockPresences.put(cid, MessageOuterClass.Message.BlockPresenceType.Have);
                }
            }
        }

        if (blockPresences.isEmpty() && datas.isEmpty()) {
            return null;
        }

        return create(Collections.emptyList(), blockPresences, datas, 0);

    }


    @NonNull
    public static MessageOuterClass.Message create(
            @NonNull MessageOuterClass.Message.Wantlist.WantType wantType,
            @NonNull List<Cid> cids) throws Exception {

        if (cids.isEmpty()) {
            throw new Exception("no cids defined");
        }

        List<MessageOuterClass.Message.Wantlist.Entry> wantlist = new ArrayList<>();

        int priority = Integer.MAX_VALUE;

        for (Cid cid : cids) {

            wantlist.add(MessageOuterClass.Message.Wantlist.Entry
                    .newBuilder()
                    .setBlock(ByteString.copyFrom(cid.bytes()))
                    .setPriority(priority)
                    .setCancel(false)
                    .setWantType(wantType)
                    .setSendDontHave(false)
                    .build());

            priority--;
        }


        return create(wantlist, Collections.emptyMap(), Collections.emptyMap(), 0);

    }


    public static MessageOuterClass.Message create(
            List<MessageOuterClass.Message.Wantlist.Entry> wantlist,
            Map<Cid, MessageOuterClass.Message.BlockPresenceType> blockPresences,
            Map<Cid, byte[]> datas, int pendingBytes) throws IOException {

        MessageOuterClass.Message.Builder builder = MessageOuterClass.Message.newBuilder();

        MessageOuterClass.Message.Wantlist.Builder wantBuilder =
                MessageOuterClass.Message.Wantlist.newBuilder();


        for (MessageOuterClass.Message.Wantlist.Entry entry : wantlist) {
            wantBuilder.addEntries(entry);
        }
        wantBuilder.setFull(false);
        builder.setWantlist(wantBuilder.build());


        for (Map.Entry<Cid, byte[]> entry : datas.entrySet()) {
            builder.addPayload(MessageOuterClass.Message.Block.newBuilder()
                    .setData(ByteString.copyFrom(entry.getValue()))
                    .setPrefix(ByteString.copyFrom(entry.getKey().getPrefix().bytes())).build());
        }

        for (Map.Entry<Cid, MessageOuterClass.Message.BlockPresenceType> mapEntry :
                blockPresences.entrySet()) {
            builder.addBlockPresences(MessageOuterClass.Message.BlockPresence.newBuilder()
                    .setType(mapEntry.getValue())
                    .setCid(ByteString.copyFrom(mapEntry.getKey().bytes())));
        }

        builder.setPendingBytes(pendingBytes);

        return builder.build();

    }

    public Collection<Block> blocks() {
        return blocks.values();
    }

    public List<Cid> haves() {
        return haves;
    }


}
