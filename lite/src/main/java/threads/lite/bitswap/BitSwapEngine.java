package threads.lite.bitswap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import net.luminis.quic.QuicStream;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Objects;

import bitswap.pb.MessageOuterClass;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Cid;
import threads.lite.core.Cancellable;
import threads.lite.core.Connection;
import threads.lite.core.Stream;
import threads.lite.core.StreamHandler;
import threads.lite.core.Transport;
import threads.lite.format.Block;
import threads.lite.format.BlockStore;
import threads.lite.host.LiteTransport;
import threads.lite.utils.DataHandler;


public final class BitSwapEngine implements BitSwap {
    private static final String TAG = BitSwapEngine.class.getSimpleName();
    private final BlockStore blockstore;


    public BitSwapEngine(@NonNull BlockStore bs) {
        this.blockstore = bs;
    }

    @Nullable
    @Override
    public Block getBlock(Cancellable cancellable, Cid cid) throws Exception {
        throw new Exception("Implementation error");
    }

    @Override
    public void clear() {
        // nothing to do here
    }

    public void receiveMessage(@NonNull Connection conn,
                               @NonNull MessageOuterClass.Message bsm) throws IOException {

        MessageOuterClass.Message msg = BitSwapMessage.response(blockstore, bsm);
        if (msg != null) {
            sendReply(conn, msg);
        }
    }


    @Override
    public void throwable(@NonNull Connection connection, @NonNull Throwable throwable) {

        LogUtils.error(TAG, "" + connection.getRemoteAddress() +
                " error " + throwable.getMessage());
        if (connection.isConnected()) {
            connection.close();
        }
    }


    private void sendReply(@NonNull Connection connection, @NonNull MessageOuterClass.Message msg) {

        connection.createStream(new StreamHandler() {
                    @Override
                    public void throwable(Stream stream, Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                        connection.close();
                    }

                    @Override
                    public void token(Stream stream, String token) throws Exception {
                        if (!Arrays.asList(IPFS.STREAM_PROTOCOL,
                                IPFS.BITSWAP_PROTOCOL).contains(token)) {
                            throw new Exception("Token " + token + " not supported");
                        }
                        if (Objects.equals(token, IPFS.BITSWAP_PROTOCOL)) {
                            stream.writeOutput(DataHandler.encode(msg))
                                    .thenApply(Stream::closeOutput);
                        }
                    }

                    @Override
                    public void fin() {
                        // nothing to do here
                    }

                    @Override
                    public void data(Stream stream, ByteBuffer data) {
                        LogUtils.error(TAG, "data sendReply invoked");
                    }

                    @NonNull
                    @Override
                    public Transport getTransport(QuicStream quicStream) {
                        return new LiteTransport();
                    }

                    @Override
                    public void streamTerminated(QuicStream quicStream) {
                        LogUtils.error(TAG, "todo stream terminated");
                    }
                })
                .thenApply(quicStream -> quicStream.writeOutput(
                        DataHandler.encodeTokens(IPFS.STREAM_PROTOCOL, IPFS.BITSWAP_PROTOCOL)));

    }

}
