package threads.lite.bitswap;

import androidx.annotation.Nullable;

import bitswap.pb.MessageOuterClass;
import threads.lite.cid.Cid;
import threads.lite.core.Cancellable;
import threads.lite.core.Connection;
import threads.lite.format.Block;

public interface BitSwap {

    @Nullable
    Block getBlock(Cancellable cancellable, Cid cid) throws Exception;

    void clear();

    void receiveMessage(Connection connection, MessageOuterClass.Message bsm) throws Exception;

    void throwable(Connection connection, Throwable throwable);
}
