package threads.lite.data;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Room;

import java.util.List;

import merkledag.pb.Merkledag;
import threads.lite.cid.Cid;
import threads.lite.format.BlockStore;


public class BLOCKS implements BlockStore {

    private static volatile BLOCKS INSTANCE = null;
    private final BlocksDatabase blocksDatabase;

    private BLOCKS(BLOCKS.Builder builder) {
        this.blocksDatabase = builder.blocksDatabase;
    }

    @NonNull
    private static BLOCKS createBlocks(@NonNull BlocksDatabase blocksDatabase) {

        return new BLOCKS.Builder()
                .blocksDatabase(blocksDatabase)
                .build();
    }

    public static BLOCKS getInstance(@NonNull Context context) {

        if (INSTANCE == null) {
            synchronized (BLOCKS.class) {
                if (INSTANCE == null) {
                    BlocksDatabase blocksDatabase = Room.databaseBuilder(context, BlocksDatabase.class,
                                    BlocksDatabase.class.getSimpleName()).
                            allowMainThreadQueries().
                            fallbackToDestructiveMigration().build();

                    INSTANCE = BLOCKS.createBlocks(blocksDatabase);
                }
            }
        }
        return INSTANCE;
    }

    @Nullable
    private Merkledag.PBNode getNode(@NonNull Cid cid) throws Exception {
        Block block = getBlocksDatabase().blockDao().getBlock(cid);
        if (block != null) {
            return Merkledag.PBNode.parseFrom(block.getData());
        }
        return null;
    }

    @Override
    public void clear() {
        getBlocksDatabase().clearAllTables();
    }

    @NonNull
    public BlocksDatabase getBlocksDatabase() {
        return blocksDatabase;
    }

    @NonNull
    private Block createBlock(@NonNull Cid cid, @NonNull Merkledag.PBNode data) {
        return Block.createBlock(cid, data.toByteArray());
    }

    private void storeBlock(@NonNull Block block) {
        getBlocksDatabase().blockDao().insertBlock(block);
    }


    private void insertBlock(@NonNull Cid cid, @NonNull Merkledag.PBNode data) {
        storeBlock(createBlock(cid, data));
    }


    @Override
    public boolean hasBlock(@NonNull Cid cid) {
        return getBlocksDatabase().blockDao().hasBlock(cid);
    }

    @Nullable
    @Override
    public threads.lite.format.Block getBlock(@NonNull Cid cid) throws Exception {
        Merkledag.PBNode data = getNode(cid);
        if (data == null) {
            return null;
        }
        return threads.lite.format.Block.createBlockWithCid(cid, data);
    }

    @Nullable
    @Override
    public byte[] getData(@NonNull Cid cid) {
        return getBlocksDatabase().blockDao().getData(cid);
    }

    @Override
    public void putBlock(@NonNull threads.lite.format.Block block) {
        insertBlock(block.getCid(), block.getNode());
    }

    @Override
    public void deleteBlock(@NonNull Cid cid) {
        getBlocksDatabase().blockDao().deleteBlock(cid);
    }

    @Override
    public void deleteBlocks(@NonNull List<Cid> cids) {
        for (Cid cid : cids) {
            deleteBlock(cid);
        }
    }

    static class Builder {
        BlocksDatabase blocksDatabase = null;

        BLOCKS build() {
            return new BLOCKS(this);
        }

        Builder blocksDatabase(@NonNull BlocksDatabase blocksDatabase) {

            this.blocksDatabase = blocksDatabase;
            return this;
        }
    }
}
