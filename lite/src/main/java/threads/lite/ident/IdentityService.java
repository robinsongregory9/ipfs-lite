package threads.lite.ident;

import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import net.luminis.quic.QuicStream;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

import identify.pb.IdentifyOuterClass;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Multiaddr;
import threads.lite.core.Connection;
import threads.lite.core.Stream;
import threads.lite.core.StreamHandler;
import threads.lite.core.Transport;
import threads.lite.host.LiteTransport;
import threads.lite.host.PeerInfo;
import threads.lite.utils.DataHandler;

public class IdentityService {

    private static final String TAG = IdentityService.class.getSimpleName();

    @NonNull
    public static CompletableFuture<PeerInfo> getPeerInfo(@NonNull Connection conn) {
        CompletableFuture<PeerInfo> done = new CompletableFuture<>();
        IdentityService.getIdentity(conn).whenComplete((identify, throwable) -> {
            if (throwable != null) {
                done.completeExceptionally(throwable);
            } else {
                done.complete(getPeerInfo(identify));
            }
        });
        return done;
    }

    @NonNull
    public static PeerInfo getPeerInfo(@NonNull IdentifyOuterClass.Identify identify) {

        String agent = identify.getAgentVersion();
        String version = identify.getProtocolVersion();
        Multiaddr observedAddr = null;
        if (identify.hasObservedAddr()) {
            try {
                observedAddr = Multiaddr.create(identify.getObservedAddr());
            } catch (Throwable ignore) {
            }
        }

        List<String> protocols = identify.getProtocolsList();
        List<Multiaddr> addresses = new ArrayList<>();
        for (ByteString entry : identify.getListenAddrsList()) {
            try {
                addresses.add(Multiaddr.create(entry));
            } catch (Throwable ignore) {
            }
        }

        return new PeerInfo(agent, version, addresses, protocols, observedAddr);
    }

    @NonNull
    private static CompletableFuture<IdentifyOuterClass.Identify> getIdentity(
            @NonNull Connection conn) {

        CompletableFuture<IdentifyOuterClass.Identify> done = new CompletableFuture<>();

        conn.createStream(new StreamHandler() {
            @Override
            public void throwable(Stream stream, Throwable throwable) {
                done.completeExceptionally(throwable);
            }

            @Override
            public void token(Stream stream, String token) throws Exception {
                if (!Arrays.asList(IPFS.STREAM_PROTOCOL, IPFS.IDENTITY_PROTOCOL).contains(token)) {
                    throw new Exception("Token " + token + " not supported");
                }
                if (Objects.equals(token, IPFS.IDENTITY_PROTOCOL)) {
                    stream.closeOutput();
                }
            }

            @Override
            public void fin() {
                // nothing to do here
            }

            @Override
            public void data(Stream stream, ByteBuffer data) throws Exception {
                done.complete(IdentifyOuterClass.Identify.parseFrom(data.array()));
            }

            @NonNull
            @Override
            public Transport getTransport(QuicStream quicStream) {
                return new LiteTransport();
            }

            @Override
            public void streamTerminated(QuicStream quicStream) {
                LogUtils.error(TAG, "todo stream terminated");
            }
        }).whenComplete((stream, throwable) -> {
            if (throwable != null) {
                done.completeExceptionally(throwable);
            } else {
                stream.writeOutput(
                        DataHandler.encodeTokens(IPFS.STREAM_PROTOCOL, IPFS.IDENTITY_PROTOCOL));
            }
        });

        return done;

    }
}
