package threads.lite.host;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.protobuf.ByteString;

import net.luminis.quic.QuicClientConnection;
import net.luminis.quic.QuicClientConnectionImpl;
import net.luminis.quic.QuicConnection;
import net.luminis.quic.QuicStream;
import net.luminis.quic.RawStreamData;
import net.luminis.quic.Reachability;
import net.luminis.quic.TransportParameters;
import net.luminis.quic.Version;
import net.luminis.quic.server.ApplicationProtocolConnectionFactory;
import net.luminis.quic.server.ServerConnector;

import java.io.ByteArrayInputStream;
import java.net.ConnectException;
import java.net.DatagramSocket;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.security.KeyPair;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;
import java.util.function.Function;

import crypto.pb.Crypto;
import identify.pb.IdentifyOuterClass;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.autonat.Autonat;
import threads.lite.autonat.AutonatService;
import threads.lite.bitswap.BitSwapEngine;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.cid.Protocol;
import threads.lite.cid.ProtocolSupport;
import threads.lite.core.Connection;
import threads.lite.core.StreamHandler;
import threads.lite.data.BlockSupplier;
import threads.lite.format.BlockStore;
import threads.lite.holepunch.HolePunchService;
import threads.lite.noise.Noise;
import threads.lite.push.Push;
import threads.lite.relay.RelayService;
import threads.lite.relay.Reservation;
import threads.lite.utils.PackageReader;


public class LiteHost {
    private static final String TAG = LiteHost.class.getSimpleName();
    @NonNull
    private static final AtomicInteger failure = new AtomicInteger(0);
    @NonNull
    private static final AtomicInteger success = new AtomicInteger(0);
    @NonNull
    public final AtomicReference<ProtocolSupport> protocol = new AtomicReference<>(ProtocolSupport.UNKNOWN);
    /* NOT YET REQUIRED
    @NonNull

    @NonNull
    private static final TrustManager tm = new X509TrustManager() {
        @Override
        public void checkClientTrusted(X509Certificate[] chain, String s) {
            try {
                if (IPFS.EVALUATE_PEER) {
                    for (X509Certificate cert : chain) {
                        PubKey pubKey = LiteHostCertificate.extractPublicKey(cert);
                        Objects.requireNonNull(pubKey);
                        PeerId peerId = PeerId.fromPubKey(pubKey);
                        Objects.requireNonNull(peerId);
                    }
                }
            } catch (Throwable throwable) {
                throw new Exception(throwable);
            }
        }

        @Override
        public void checkServerTrusted(X509Certificate[] chain, String s) {

            try {
                if (IPFS.EVALUATE_PEER) {
                    for (X509Certificate cert : chain) {
                        PubKey pubKey = LiteHostCertificate.extractPublicKey(cert);
                        Objects.requireNonNull(pubKey);
                        PeerId peerId = PeerId.fromPubKey(pubKey);
                        Objects.requireNonNull(peerId);
                        remotes.put(peerId, pubKey);
                    }
                }
            } catch (Throwable throwable) {
                throw new Exception(throwable);
            }
        }

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[0];
        }
    };*/
    @NonNull
    private final Set<Reservation> reservations = ConcurrentHashMap.newKeySet();
    @NonNull
    private final BlockStore blockStore;
    @NonNull
    private final KeyPair keypair;
    @NonNull
    private final PeerId self;
    @NonNull
    private final LiteHostCertificate liteHostCertificate;
    @NonNull
    private final AtomicReference<Multiaddr> autonatAddress = new AtomicReference(null);
    @NonNull
    private final ReentrantLock autonat = new ReentrantLock();
    @NonNull
    private final ReentrantLock reserve = new ReentrantLock();
    @NonNull
    private final DatagramSocket socket;
    @NonNull
    private final ServerConnector server;
    @NonNull
    private final Noise noise;
    @NonNull
    private final Session serverSession;
    @Nullable
    private Consumer<Push> incomingPush;
    @Nullable
    private Consumer<Connection> connectConsumer;
    @Nullable
    private Consumer<Reachability> reachabilityConsumer;
    @Nullable
    private Function<PeerId, Boolean> isGated;


    public LiteHost(@NonNull KeyPair keypair, @NonNull BlockStore blockStore, int port)
            throws Exception {

        this.liteHostCertificate = LiteHostCertificate.createCertificate(keypair);
        this.keypair = keypair;
        this.blockStore = blockStore;
        this.noise = Noise.getInstance();
        this.self = PeerId.fromRsaPublicKey(keypair.getPublic());
        evaluateProtocol();
        this.socket = getSocket(port);
        List<Version> supportedVersions = new ArrayList<>();
        supportedVersions.add(Version.IETF_draft_29);
        supportedVersions.add(Version.QUIC_version_1);
        this.serverSession = new Session(new BitSwapEngine(blockStore), this);


        this.server = new ServerConnector(socket,
                new ByteArrayInputStream(liteHostCertificate.certificate()),
                new ByteArrayInputStream(liteHostCertificate.privateKey()),
                supportedVersions, new Function<QuicStream, Consumer<RawStreamData>>() {
            @Override
            public Consumer<RawStreamData> apply(QuicStream quicStream) {
                return new PackageReader(new ServerHandler(serverSession));
            }
        }, false);

        server.registerApplicationProtocol(IPFS.ALPN, new ApplicationProtocolConnectionFactory() {
            @Override
            public void createConnection(String protocol, QuicConnection quicConnection) {

                LogUtils.error(TAG, "Server connection established " +
                        quicConnection.getRemoteAddress().toString());

                try {
                    if (connectConsumer != null) {
                        connectConsumer.accept(new LiteConnection(quicConnection));
                    }
                } catch (Throwable ignore) {
                }

                try {
                    InetSocketAddress address = quicConnection.getRemoteAddress();

                    if (!Multiaddr.isLocalAddress(address.getAddress())) {
                        if (reachabilityConsumer != null) {
                            reachabilityConsumer.accept(Reachability.GLOBAL);
                        }
                    }
                } catch (Throwable ignore) {
                }

            }
        });

        server.start();
    }

    public static int nextFreePort() {
        int port = ThreadLocalRandom.current().nextInt(4001, 65535);
        if (isLocalPortFree(port)) {
            return port;
        } else {
            return nextFreePort();
        }
    }

    private static boolean isLocalPortFree(int port) {
        try {
            new ServerSocket(port).close();
            return true;
        } catch (Throwable ignore) {
            return false;
        }
    }

    @NonNull
    public Noise getNoise() {
        return noise;
    }

    public void setIsGated(@Nullable Function<PeerId, Boolean> isGated) {
        this.isGated = isGated;
    }

    private DatagramSocket getSocket(int port) {
        try {
            return new DatagramSocket(port);
        } catch (Throwable ignore) {
            return getSocket(nextFreePort());
        }
    }

    @NonNull
    public ServerConnector getServer() {
        return server;
    }

    public void setConnectConsumer(@Nullable Consumer<Connection> connectConsumer) {
        this.connectConsumer = connectConsumer;
    }

    public void setClosedConsumer(@Nullable Consumer<Connection> closedConsumer) {

        getServer().setClosedConsumer(new Consumer<QuicConnection>() {
            @Override
            public void accept(QuicConnection connection) {
                closedConsumer.accept(new LiteConnection(connection));
            }
        });
    }

    public void setReachabilityConsumer(@Nullable Consumer<Reachability> reachabilityConsumer) {
        this.reachabilityConsumer = reachabilityConsumer;
        getServer().setReachabilityConsumer(reachabilityConsumer);
    }

    @NonNull
    public KeyPair getKeypair() {
        return keypair;
    }


    @NonNull
    public Set<Reservation> reservations() {
        return reservations;
    }


    @NonNull
    public Session createSession() {
        return createSession(blockSupplier -> {
        }, false, false);
    }

    @NonNull
    public Session createSession(@NonNull Consumer<BlockSupplier> supplier,
                                 boolean findProvidersActive, boolean sendReplyActive) {
        return new Session(blockStore, this, supplier, findProvidersActive, sendReplyActive);
    }

    public PeerId self() {
        return self;
    }


    @NonNull
    private List<Multiaddr> prepareAddresses(@NonNull Set<Multiaddr> multiaddrs) {
        List<Multiaddr> all = new ArrayList<>();
        for (Multiaddr multiaddr : multiaddrs) {

            if (multiaddr.isDns()) {
                Multiaddr ma = DnsResolver.resolveDns(protocol.get(), multiaddr);
                if (ma != null) {
                    all.add(ma);
                }
            } else if (multiaddr.isDns6()) {
                Multiaddr ma = DnsResolver.resolveDns6(protocol.get(), multiaddr);
                if (ma != null) {
                    all.add(ma);
                }
            } else if (multiaddr.isDns4()) {
                Multiaddr ma = DnsResolver.resolveDns4(protocol.get(), multiaddr);
                if (ma != null) {
                    all.add(ma);
                }
            } else if (multiaddr.isDnsaddr()) {
                all.addAll(DnsResolver.resolveDnsaddr(protocol.get(), multiaddr));
            } else {
                all.add(multiaddr);
            }
        }
        return all;
    }


    @NonNull
    public Set<Multiaddr> networkListenAddresses() {
        Set<Multiaddr> set = new HashSet<>();
        Multiaddr observed = autonatAddress.get();
        if (observed != null) {
            set.add(observed);
        }

        int port = socket.getLocalPort();
        if (port > 0) {
            for (InetAddress inetAddress : networkAddresses()) {
                try {
                    set.add(Multiaddr.create(new InetSocketAddress(inetAddress, port)));
                } catch (Throwable ignore) {
                }
            }
        }

        return set;
    }


    @NonNull
    public Set<Multiaddr> listenAddresses() {
        Set<Multiaddr> set = networkListenAddresses();

        for (Reservation reservation : reservations()) {
            try {
                if (reservation.getConnection().isConnected()) {
                    set.addAll(reservation.getReservationAddresses(protocol.get()));
                } else {
                    reservations.remove(reservation);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }

        return set;
    }

    @NonNull
    public CompletableFuture<Connection> connect(
            @NonNull Session session, @NonNull Set<Multiaddr> multiaddrs, int timeout,
            int maxIdleTimeoutInSeconds, int initialMaxStreams, int initialMaxStreamData) {

        CompletableFuture<Connection> done = new CompletableFuture<>();
        List<Multiaddr> multiaddr = prepareAddresses(multiaddrs);
        int addresses = multiaddr.size();
        if (addresses == 0) {
            done.completeExceptionally(new ConnectException("no addresses left"));
            return done;
        }

        ExecutorService executor = Executors.newFixedThreadPool(
                Runtime.getRuntime().availableProcessors());
        AtomicInteger counter = new AtomicInteger(0);
        for (Multiaddr address : multiaddr) {
            counter.incrementAndGet();
            executor.execute(() -> {
                try {
                    Connection conn = dial(session, address, timeout,
                            maxIdleTimeoutInSeconds, initialMaxStreams,
                            initialMaxStreamData);
                    done.complete(conn);
                    executor.shutdownNow();
                } catch (Throwable throwable) {
                    if (counter.decrementAndGet() == 0) {
                        if (!done.isDone()) {
                            done.completeExceptionally(throwable);
                        }
                    }
                }
            });

        }
        executor.shutdown();
        return done;
    }

    // todo when sdk version min is sdk31, then return CompletableFuture<QuicConnection>
    @NonNull
    public Connection dial(@NonNull Session session, @NonNull Multiaddr address,
                           int timeout, int maxIdleTimeoutInSeconds,
                           int initialMaxStreams, int initialMaxStreamData)
            throws ConnectException, CancellationException {

        if (address.isCircuitAddress()) {
            List<String> list = address.getStringComponent(Protocol.P2P);
            if (list.size() != 2) {
                throw new ConnectException("Invalid circuit address");
            }
            try {
                PeerId peerId = PeerId.decodeName(list.get(1));
                Objects.requireNonNull(peerId);
                Multiaddr relayAddress = address.getCircuitAddress();
                return HolePunchService.directConnect(session, relayAddress, peerId, timeout,
                                maxIdleTimeoutInSeconds, initialMaxStreams, initialMaxStreamData)
                        .get(timeout, TimeUnit.SECONDS);
            } catch (Throwable throwable) {
                throw new ConnectException(throwable.getMessage());
            }
        } else {
            return dial(new LiteHandler(session), address,
                    false, timeout, maxIdleTimeoutInSeconds, initialMaxStreams,
                    initialMaxStreamData);
        }
    }


    @NonNull
    public LiteConnection dial(StreamHandler streamHandler, Multiaddr address,
                               boolean serverConnect, int timeout,
                               int maxIdleTimeoutInSeconds, int initialMaxStreams,
                               int initialMaxStreamData)
            throws ConnectException, CancellationException {


        long start = System.currentTimeMillis();
        boolean run = false;

        QuicClientConnection conn = getClientConnection(streamHandler, address.getHost(),
                address.getPort(), serverConnect, maxIdleTimeoutInSeconds,
                initialMaxStreams, initialMaxStreamData);

        try {
            conn.connect(timeout).get(timeout, TimeUnit.SECONDS); // todo future remove get
            run = true;
            return new LiteConnection(conn);
        } catch (ExecutionException | TimeoutException exception) {
            conn.abortHandshake(); // todo hack sdk31
            throw new ConnectException(exception.getMessage());
        } catch (InterruptedException interruptedException) {
            conn.abortHandshake(); // todo hack sdk31
            throw new CancellationException(interruptedException.getMessage());
        } finally {
            if (LogUtils.isError()) {
                if (run) {
                    success.incrementAndGet();
                } else {
                    failure.incrementAndGet();
                }
                LogUtils.debug(TAG, "Dial " +
                        " Success " + run +
                        " Count Success " + success.get() +
                        " Count Failure " + failure.get() +
                        " ServerConnect " + serverConnect +
                        " Address " + address +
                        " Time " + (System.currentTimeMillis() - start));
            }
        }
    }

    @NonNull
    private QuicClientConnection getClientConnection(
            StreamHandler streamHandler, String host, int port, boolean serverConnect,
            int maxIdleTimeoutInSeconds, int initialMaxStreams, int initialMaxStreamData)
            throws ConnectException {

        int initialMaxData = initialMaxStreamData;
        if (initialMaxStreams > 0) {
            initialMaxData = Integer.MAX_VALUE;
        }

        QuicClientConnectionImpl.Builder builder = QuicClientConnectionImpl.newBuilder()
                .version(Version.IETF_draft_29) // in the future switch to version 1
                .noServerCertificateCheck()
                .clientCertificate(liteHostCertificate.cert())
                .clientCertificateKey(getKeypair().getPrivate())
                .host(host)
                .port(port)
                .alpn(IPFS.ALPN)
                .transportParams(new TransportParameters(
                        maxIdleTimeoutInSeconds, initialMaxData, initialMaxStreamData,
                        initialMaxStreams, 0));
        if (serverConnect) {
            builder.serverConnector(getServer());
        }
        return builder.build(new Function<QuicStream, Consumer<RawStreamData>>() {
            @Override
            public Consumer<RawStreamData> apply(QuicStream quicStream) {
                return new PackageReader(streamHandler);
            }
        });
    }


    public void push(@NonNull Connection connection, byte[] data) {
        try {
            if (incomingPush != null) {
                incomingPush.accept(new Push(connection, new String(data)));
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }

    public void setIncomingPush(@Nullable Consumer<Push> incomingPush) {
        this.incomingPush = incomingPush;
    }

    public int numServerConnections() {
        return server.numConnections();
    }


    @NonNull
    public IdentifyOuterClass.Identify createIdentity(@NonNull Set<String> protocols,
                                                      @NonNull Set<Multiaddr> multiaddrs,
                                                      @Nullable InetSocketAddress inetSocketAddress) {

        IdentifyOuterClass.Identify.Builder builder = IdentifyOuterClass.Identify.newBuilder()
                .setAgentVersion(IPFS.AGENT)
                .setPublicKey(ByteString.copyFrom(
                        Crypto.PublicKey.newBuilder().setType(Crypto.KeyType.RSA).
                                setData(ByteString.copyFrom(
                                        keypair.getPublic().getEncoded())).build().toByteArray()))
                .setProtocolVersion(IPFS.PROTOCOL_VERSION);


        if (!multiaddrs.isEmpty()) {
            for (Multiaddr addr : multiaddrs) {
                builder.addListenAddrs(ByteString.copyFrom(addr.getBytes()));
            }
        }
        for (String protocol : protocols) {
            builder.addProtocols(protocol);
        }

        if (inetSocketAddress != null) {
            Multiaddr observed = Multiaddr.create(inetSocketAddress);
            builder.setObservedAddr(ByteString.copyFrom(observed.getBytes()));
        }

        return builder.build();
    }

    private Set<String> getProtocols() {
        return Set.of(IPFS.STREAM_PROTOCOL, IPFS.HOLE_PUNCH_PROTOCOL,
                IPFS.PUSH_PROTOCOL, IPFS.BITSWAP_PROTOCOL,
                IPFS.IDENTITY_PROTOCOL, IPFS.DHT_PROTOCOL, IPFS.RELAY_PROTOCOL_STOP);
    }


    public void updateNetwork() {
        evaluateProtocol();
    }

    private void evaluateProtocol() {
        List<InetAddress> addresses = networkAddresses();
        if (!addresses.isEmpty()) {
            protocol.set(getProtocol(addresses));
        } else {
            protocol.set(ProtocolSupport.IPv4);
        }
    }

    private List<InetAddress> networkAddresses() {
        List<InetAddress> inetAddresses = new ArrayList<>();
        try {
            List<NetworkInterface> interfaces = Collections.list(
                    NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface networkInterface : interfaces) {
                List<InetAddress> addresses =
                        Collections.list(networkInterface.getInetAddresses());
                for (InetAddress inetAddress : addresses) {
                    if (!Multiaddr.weakLocalAddress(inetAddress)) {
                        inetAddresses.add(inetAddress);
                    }
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return inetAddresses;
    }

    @NonNull
    private ProtocolSupport getProtocol(@NonNull List<InetAddress> addresses) {
        boolean ipv4 = false;
        boolean ipv6 = false;
        for (InetAddress inet : addresses) {
            if (inet instanceof Inet6Address) {
                ipv6 = true;
            } else {
                ipv4 = true;
            }
        }

        if (ipv4 && ipv6) {
            return ProtocolSupport.IPv6;
        } else if (ipv4) {
            return ProtocolSupport.IPv4;
        } else if (ipv6) {
            return ProtocolSupport.IPv6;
        } else {
            return ProtocolSupport.UNKNOWN;
        }
    }

    public int getPort() {
        return socket.getLocalPort();
    }

    @NonNull
    public PeerInfo getIdentity() {

        IdentifyOuterClass.Identify identity = createIdentity(getProtocols(),
                listenAddresses(), null);

        String agent = identity.getAgentVersion();
        String version = identity.getProtocolVersion();
        Multiaddr observed = null;
        if (identity.hasObservedAddr()) {
            try {
                observed = Multiaddr.create(identity.getObservedAddr());
            } catch (Throwable ignore) {
            }
        }

        List<String> protocols = identity.getProtocolsList();
        List<Multiaddr> addresses = new ArrayList<>();
        for (ByteString entry : identity.getListenAddrsList()) {
            try {
                addresses.add(Multiaddr.create(entry));
            } catch (Throwable ignore) {
            }
        }

        return new PeerInfo(agent, version, addresses, protocols, observed);

    }


    public boolean autonat(long timeout) {
        autonat.lock();

        try {
            Autonat autonat = new Autonat();
            Set<Multiaddr> bootstrap = getBootstrap();
            if (!bootstrap.isEmpty()) {
                ExecutorService service = Executors.newFixedThreadPool(
                        Runtime.getRuntime().availableProcessors());
                for (Multiaddr multiaddr : bootstrap) {

                    service.execute(() -> {
                        Connection conn = null;
                        try {
                            conn = dial(new LiteHandler(serverSession),
                                    multiaddr, true,
                                    IPFS.CONNECT_TIMEOUT, IPFS.GRACE_PERIOD,
                                    0, IPFS.MESSAGE_SIZE_MAX);
                            autonat.addAddr(AutonatService.autonat(serverSession, conn)
                                    .get(timeout, TimeUnit.SECONDS));
                        } catch (Throwable ignore) {
                        } finally {
                            if (conn != null) {
                                conn.close();
                            }
                        }
                    });
                }
                service.shutdown();
                if (timeout > 0) {
                    try {
                        boolean termination = service.awaitTermination(timeout, TimeUnit.SECONDS);
                        if (!termination) {
                            service.shutdownNow();
                        }
                    } catch (Throwable ignore) {
                    }
                }
            }

            Multiaddr winner = autonat.winner();
            if (winner != null) {
                autonatAddress.set(winner);
            }
            return winner != null;
        } finally {
            autonat.unlock();
        }

    }


    public Set<Reservation> reservations(long timeout) {
        reserve.lock();
        try {
            Set<PeerId> relaysStillValid = ConcurrentHashMap.newKeySet();
            Set<Reservation> list = reservations();
            // check if reservations are still valid and not expired
            for (Reservation reservation : list) {

                // check if still a connection
                // only for safety here
                if (!reservation.getConnection().isConnected()) {
                    list.remove(reservation);
                    continue;
                }

                if (reservation.expireInMinutes() < 2) {
                    try {
                        RelayService.reservation(serverSession, reservation.getRelayId(),
                                reservation.getRelayAddress()).get();
                        relaysStillValid.add(reservation.getRelayId());
                    } catch (Throwable throwable) {
                        // note RelayService reservation throws exceptions
                        list.remove(reservation);
                    }
                } else {
                    // still valid
                    relaysStillValid.add(reservation.getRelayId());
                }

            }

            Set<Multiaddr> bootstrap = getBootstrap();
            if (!bootstrap.isEmpty()) {
                ExecutorService service = Executors.newFixedThreadPool(
                        Runtime.getRuntime().availableProcessors());
                for (Multiaddr address : bootstrap) {
                    service.execute(() -> {
                        try {
                            String name = address.getStringComponent(Protocol.P2P).get(0);
                            Objects.requireNonNull(name);
                            PeerId relayId = PeerId.fromBase58(name);
                            Objects.requireNonNull(relayId);

                            if (!relaysStillValid.contains(relayId)) {
                                reservation(serverSession, relayId, address);
                            } // else case: nothing to do here, reservation is sill valid


                        } catch (Throwable ignore) {
                        }
                    });

                }
                service.shutdown();
                if (timeout > 0) {
                    try {
                        boolean termination = service.awaitTermination(timeout, TimeUnit.SECONDS);
                        if (!termination) {
                            service.shutdownNow();
                        }
                    } catch (Throwable ignore) {
                    }
                }
            }
        } finally {
            reserve.unlock();
        }
        return reservations();
    }

    private void reservation(@NonNull Session session, @NonNull PeerId relayId,
                             @NonNull Multiaddr relayAddress) {
        try {
            reservations.add(RelayService.reservation(session, relayId, relayAddress).get());
        } catch (Throwable ignore) {
        }
    }

    public boolean hasReservations() {
        return reservations.size() > 0;
    }

    public Set<Multiaddr> getBootstrap() {
        return DnsResolver.resolveDnsaddrHost(protocol.get(), IPFS.LIB2P_DNS);
    }

    public boolean isPeerGated(@NonNull PeerId peerId) {
        if (isGated != null) {
            return isGated.apply(peerId);
        }
        return false;
    }

    @NonNull
    public DatagramSocket getSocket() {
        return socket;
    }
}


