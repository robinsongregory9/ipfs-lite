package threads.lite.host;

import androidx.annotation.NonNull;

import net.luminis.quic.QuicStream;

import java.nio.ByteBuffer;
import java.util.Set;

import bitswap.pb.MessageOuterClass;
import identify.pb.IdentifyOuterClass;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.core.Stream;
import threads.lite.core.StreamHandler;
import threads.lite.core.Transport;
import threads.lite.utils.DataHandler;

public class ServerHandler implements StreamHandler {

    private static final String TOKEN = "TOKEN";
    private static final String TAG = ServerHandler.class.getSimpleName();


    private final Set<String> protocols = Set.of(IPFS.STREAM_PROTOCOL, IPFS.PUSH_PROTOCOL,
            IPFS.BITSWAP_PROTOCOL, IPFS.IDENTITY_PROTOCOL);
    @NonNull
    private final Session session;

    public ServerHandler(@NonNull Session session) {
        this.session = session;
    }

    @Override
    public void throwable(Stream stream, Throwable throwable) {
        session.throwable(stream.getConnection(), throwable);
    }

    @NonNull
    @Override
    public Transport getTransport(QuicStream quicStream) {
        return new LiteTransport();
    }

    @Override
    public void streamTerminated(QuicStream quicStream) {
        // noting to do here
    }

    @Override
    public void token(Stream stream, String token) {

        stream.setAttribute(TOKEN, token);
        switch (token) {
            case IPFS.STREAM_PROTOCOL:
                break;
            case IPFS.PUSH_PROTOCOL:
                stream.writeOutput(DataHandler.encodeTokens(IPFS.STREAM_PROTOCOL, IPFS.PUSH_PROTOCOL))
                        .thenApply(Stream::closeOutput);
                break;
            case IPFS.BITSWAP_PROTOCOL:
                stream.writeOutput(DataHandler.encodeTokens(IPFS.STREAM_PROTOCOL, IPFS.BITSWAP_PROTOCOL))
                        .thenApply(Stream::closeOutput);
                break;
            case IPFS.IDENTITY_PROTOCOL:
                stream.writeOutput(DataHandler.encodeTokens(IPFS.STREAM_PROTOCOL, IPFS.IDENTITY_PROTOCOL));

                IdentifyOuterClass.Identify response =
                        session.getHost().createIdentity(protocols,
                                session.getHost().listenAddresses(),
                                stream.getConnection().getRemoteAddress());
                stream.writeOutput(DataHandler.encode(response))
                        .thenApply(Stream::closeOutput);
                break;

            default:
                LogUtils.debug(TAG, "Ignore " + token);
                stream.writeOutput(DataHandler.encodeTokens(IPFS.STREAM_PROTOCOL, IPFS.NA))
                        .thenApply(Stream::closeOutput);
        }

    }

    @Override
    public void fin() {
        LogUtils.info(TAG, "nothing to do here");
    }


    @Override
    public void data(Stream stream, ByteBuffer data) throws Exception {
        String protocol = (String) stream.getAttribute(TOKEN);
        if (protocol != null) {
            switch (protocol) {
                case IPFS.BITSWAP_PROTOCOL: {
                    session.receiveMessage(stream.getConnection(),
                            MessageOuterClass.Message.parseFrom(data.array()));
                    break;
                }
                case IPFS.PUSH_PROTOCOL: {
                    session.getHost().push(stream.getConnection(), data.array());
                    break;
                }
                default:
                    throw new Exception("StreamHandler invalid protocol");
            }
        } else {
            throw new Exception("StreamHandler invalid protocol");
        }
    }
}
