package threads.lite.host;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import net.luminis.quic.QuicStream;

import java.util.concurrent.CompletableFuture;

import threads.lite.core.Connection;
import threads.lite.core.Stream;

public class LiteStream implements Stream {

    @NonNull
    private final QuicStream quicStream;


    public LiteStream(@NonNull QuicStream quicStream) {
        this.quicStream = quicStream;
    }


    @NonNull
    @Override
    public Connection getConnection() {
        return new LiteConnection(quicStream.getConnection());
    }

    @NonNull
    public CompletableFuture<Stream> closeOutput() {
        CompletableFuture<Stream> stream = new CompletableFuture<>();
        quicStream.closeOutput().whenComplete((quicStream, throwable) -> {
            if (throwable != null) {
                stream.completeExceptionally(throwable);
            } else {
                stream.complete(new LiteStream(quicStream));
            }
        });
        return stream;
    }

    public void closeInput() {
        quicStream.closeInput();
    }

    @Override
    public void setAttribute(String key, Object value) {
        quicStream.setAttribute(key, value);
    }

    @Nullable
    @Override
    public Object getAttribute(String key) {
        return quicStream.getAttribute(key);
    }

    @Override
    public boolean isInitiator() {
        return getStreamId() % 2 == 0;
    }

    @NonNull
    public CompletableFuture<Stream> writeOutput(byte[] data) {
        CompletableFuture<Stream> stream = new CompletableFuture<>();
        quicStream.writeOutput(data).whenComplete((quicStream, throwable) -> {
            if (throwable != null) {
                stream.completeExceptionally(throwable);
            } else {
                stream.complete(new LiteStream(quicStream));
            }
        });
        return stream;
    }

    @Override
    public int getStreamId() {
        return quicStream.getStreamId();
    }


    @NonNull
    public QuicStream getQuicStream() {
        return quicStream;
    }

}
