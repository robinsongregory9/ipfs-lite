package threads.lite.host;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import bitswap.pb.MessageOuterClass;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.bitswap.BitSwap;
import threads.lite.bitswap.BitSwapManager;
import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.core.Cancellable;
import threads.lite.core.Connection;
import threads.lite.data.BlockSupplier;
import threads.lite.dht.KadDht;
import threads.lite.format.Block;
import threads.lite.format.BlockStore;
import threads.lite.ipns.IpnsService;


public class Session {

    private static final String TAG = Session.class.getSimpleName();
    @NonNull
    private final BitSwap bitSwap;
    @NonNull
    private final LiteHost host;
    @NonNull
    private final ConcurrentHashMap<Multiaddr, Connection> swarm = new ConcurrentHashMap<>();
    private final KadDht routing;

    public Session(@NonNull BlockStore blockstore,
                   @NonNull LiteHost host,
                   @NonNull Consumer<BlockSupplier> supplierConsumer,
                   boolean findProvidersActive, boolean sendReplyActive) {
        this.host = host;
        this.bitSwap = new BitSwapManager(host, this, blockstore,
                supplierConsumer, findProvidersActive, sendReplyActive);
        this.routing = new KadDht(host, new IpnsService());
    }

    public Session(@NonNull BitSwap bitSwap, @NonNull LiteHost host) {
        this.host = host;
        this.bitSwap = bitSwap;
        this.routing = new KadDht(host, new IpnsService());
    }

    @Nullable
    public Block getBlock(@NonNull Cancellable cancellable, @NonNull Cid cid)
            throws Exception {
        return bitSwap.getBlock(cancellable, cid);
    }

    public void clear(boolean clearSwarm) {
        if (clearSwarm) swarm.values().forEach(Connection::close);
        bitSwap.clear();
        routing.clear();
        if (clearSwarm) swarm.clear();
    }

    public void receiveMessage(@NonNull Connection conn,
                               @NonNull MessageOuterClass.Message bsm) throws Exception {
        bitSwap.receiveMessage(conn, bsm);
    }

    @NonNull
    public LiteHost getHost() {
        return host;
    }

    public boolean swarmHas(@NonNull Multiaddr address) {
        Connection conn = swarm.get(address);
        if (conn != null) {
            if (!conn.isConnected()) {
                swarmReduce(conn);
                return false;
            }
            return true;
        }
        return false;
    }

    public void swarmReduce(@NonNull Connection connection) {
        swarm.remove(Multiaddr.create(connection.getRemoteAddress()));
    }

    public boolean swarmEnhance(@NonNull Connection connection) {
        Multiaddr multiaddr = Multiaddr.create(connection.getRemoteAddress());
        if (!swarmHas(multiaddr)) {
            swarm.put(multiaddr, connection);
            return true;
        }
        return false;
    }

    @NonNull
    public List<Connection> getSwarm() {
        List<Connection> result = new ArrayList<>();
        for (Connection conn : swarm.values()) {
            if (conn.isConnected()) {
                result.add(conn);
            } else {
                swarm.remove(Multiaddr.create(conn.getRemoteAddress()));
            }
        }
        return result;
    }

    public boolean swarmContains(@NonNull Connection connection) {
        return swarm.contains(connection);
    }

    public CompletableFuture<Void> putValue(@NonNull Cancellable cancellable,
                                            @NonNull byte[] key,
                                            @NonNull byte[] data) {
        return routing.putValue(cancellable, key, data);
    }

    public CompletableFuture<Void> findPeer(@NonNull Cancellable cancellable,
                                            @NonNull Consumer<Multiaddr> consumer,
                                            @NonNull PeerId peerID) {
        return routing.findPeer(cancellable, consumer, peerID);
    }

    public CompletableFuture<Void> searchValue(@NonNull Cancellable cancellable,
                                               @NonNull Consumer<IpnsService.Entry> consumer,
                                               @NonNull byte[] key) {
        return routing.searchValue(cancellable, consumer, key);
    }

    public CompletableFuture<Void> findProviders(@NonNull Cancellable cancellable,
                                                 @NonNull Consumer<Multiaddr> consumer,
                                                 @NonNull Cid cid) {
        return routing.findProviders(cancellable, consumer, cid);
    }

    public CompletableFuture<Void> provide(@NonNull Cancellable cancellable, @NonNull Cid cid) {
        return routing.provide(cancellable, cid);
    }

    public void throwable(@NonNull Connection connection,
                          @NonNull Throwable throwable) {
        bitSwap.throwable(connection, throwable);
    }

    public boolean isGated(@NonNull PeerId peerId) {
        return host.isPeerGated(peerId);
    }

    public void punching(Multiaddr multiaddr) {

        // Upon expiry of the timer, B starts to send UDP packets filled with random bytes to A's
        // address. Packets should be sent repeatedly in random intervals between 10 and 200 ms.
        try {
            InetAddress address = multiaddr.getInetAddress();
            int port = multiaddr.getPort();

            byte[] datagramData = new byte[64];
            Random rd = new Random();
            rd.nextBytes(datagramData);

            DatagramPacket datagram = new DatagramPacket(
                    datagramData, datagramData.length, address, port);

            LogUtils.debug(TAG, "[B] Hole Punch Send Random " + address + " " + port);

            host.getSocket().send(datagram);

            Thread.sleep(getRandomPunch()); // sleep for random value between 10 and 200

            if (!Thread.currentThread().isInterrupted()) {
                punching(multiaddr);
            }

        } catch (Throwable ignore) {
        }
    }

    private int getRandomPunch() {
        return new Random().nextInt(190) + 10;
    }

    public void holePunchConnect(@NonNull Set<Multiaddr> multiaddrs) {
        try {
            ExecutorService service = Executors.newFixedThreadPool(multiaddrs.size());
            for (Multiaddr multiaddr : multiaddrs) {
                service.execute(() -> punching(multiaddr));
            }
            boolean finished = service.awaitTermination(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
            if (!finished) {
                service.shutdownNow();
            }
        } catch (Throwable ignore) {
        }
    }
}

