package threads.lite.host;


import androidx.annotation.NonNull;

import java.nio.ByteBuffer;

import threads.lite.core.Frame;

public class LiteFrame implements Frame {

    private final int length;
    private final ByteBuffer data;

    public LiteFrame(int length) {
        this.length = length;
        this.data = ByteBuffer.allocate(length);
    }

    @NonNull
    @Override
    public String toString() {
        return "LiteFrame{" +
                "length=" + length +
                '}';
    }

    @Override
    public int getLength() {
        return length;
    }

    @Override
    public ByteBuffer getData() {
        return data;
    }

    @Override
    public void put(byte[] bytes) {
        data.put(bytes);
    }

    @Override
    public void put(byte value) {
        data.put(value);
    }

    @Override
    public int position() {
        return data.position();
    }
}
