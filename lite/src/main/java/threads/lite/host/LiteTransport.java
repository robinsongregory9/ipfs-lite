package threads.lite.host;

import net.luminis.quic.QuicStream;

import threads.lite.core.Frame;
import threads.lite.core.Stream;
import threads.lite.core.Transport;

public class LiteTransport implements Transport {
    @Override
    public Type getType() {
        return Type.PLAIN;
    }

    @Override
    public Stream getStream(QuicStream quicStream, Frame frame) {
        return new LiteStream(quicStream);
    }
}
