package threads.lite.ipns;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;

import com.google.common.primitives.Bytes;
import com.google.protobuf.ByteString;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Arrays;
import java.util.Date;
import java.util.Objects;

import crypto.pb.Crypto;
import ipns.pb.Ipns;
import ipns.pb.Ipns.IpnsEntry;
import threads.lite.IPFS;
import threads.lite.cid.Cid;
import threads.lite.cid.Multihash;
import threads.lite.cid.PeerId;
import threads.lite.core.Validator;
import threads.lite.crypto.Key;
import threads.lite.crypto.PubKey;
import threads.lite.utils.DataHandler;


public class IpnsService implements Validator {

    @NonNull
    private static PubKey extractPublicKey(@NonNull PeerId id) throws Exception {
        ByteBuffer wrap = ByteBuffer.wrap(id.getBytes());
        int version = DataHandler.readUnsignedLeb128(wrap);
        if (version != Cid.IDENTITY) {
            throw new Exception("not supported codec");
        }
        int length = DataHandler.readUnsignedLeb128(wrap);
        byte[] data = new byte[length];
        wrap.get(data);
        return Key.unmarshalPublicKey(data);
    }


    @SuppressLint("SimpleDateFormat")
    @NonNull
    public static Date getDate(@NonNull String format) throws ParseException {
        return Objects.requireNonNull(new SimpleDateFormat(IPFS.TIME_FORMAT_IPFS).parse(format));
    }


    private static byte[] sign(PrivateKey key, byte[] data) throws Exception {
        Signature sha256withRSA = Signature.getInstance("SHA256withRSA");
        sha256withRSA.initSign(key);
        sha256withRSA.update(data);
        return sha256withRSA.sign();
    }

    @NonNull
    public static IpnsEntry create(@NonNull PrivateKey key, byte[] value,
                                   long sequence, @NonNull Date eol,
                                   @NonNull Duration duration) throws Exception {

        @SuppressLint("SimpleDateFormat") String format = new SimpleDateFormat(
                IPFS.TIME_FORMAT_IPFS).format(eol);


        IpnsEntry entry = IpnsEntry.newBuilder()
                .setValidityType(IpnsEntry.ValidityType.EOL)
                .setSequence(sequence)
                .setTtl(duration.toNanos())
                .setValue(ByteString.copyFrom(value))
                .setData(ByteString.copyFrom(new byte[0]))
                .setValidity(ByteString.copyFrom(format.getBytes())).buildPartial();

        byte[] sig1 = sign(key, ipnsEntryDataForSigV1(entry));
        byte[] sig2 = sign(key, ipnsEntryDataForSigV2(entry));

        IpnsEntry.Builder builder = entry.toBuilder();
        builder.setSignatureV1(ByteString.copyFrom(sig1));
        builder.setSignatureV2(ByteString.copyFrom(sig2));

        return builder.build();
    }


    public static IpnsEntry embedPublicKey(PublicKey publicKey, Ipns.IpnsEntry entry) {

        byte[] pkBytes = Crypto.PublicKey.newBuilder().setType(Crypto.KeyType.RSA).
                setData(ByteString.copyFrom(publicKey.getEncoded())).build().toByteArray();
        return entry.toBuilder().setPubKey(ByteString.copyFrom(pkBytes)).build();
    }

    public static byte[] ipnsEntryDataForSigV1(Ipns.IpnsEntry entry) throws Exception {
        ByteString value = entry.getValue();
        ByteString validity = entry.getValidity();
        String type = entry.getValidityType().toString();

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            outputStream.write(value.toByteArray());
            outputStream.write(validity.toByteArray());
            outputStream.write(type.getBytes());
            return outputStream.toByteArray();
        }
    }

    private static byte[] ipnsEntryDataForSigV2(Ipns.IpnsEntry entry) {

        byte[] dataForSig = "ipns-signature:".getBytes();

        byte[] data = entry.getData().toByteArray();

        ByteBuffer out = ByteBuffer.allocate(dataForSig.length + data.length);
        out.put(dataForSig);
        out.put(data);
        return out.array();
    }

    @NonNull
    @Override
    public Entry validate(byte[] key, byte[] value) throws Exception {

        byte[] ipns = IPFS.IPNS_PATH.getBytes();
        int index = Bytes.indexOf(key, ipns);
        if (index != 0) {
            throw new Exception("parsing issue");
        }

        Ipns.IpnsEntry entry = IpnsEntry.parseFrom(value);
        Objects.requireNonNull(entry);

        byte[] pid = Arrays.copyOfRange(key, ipns.length, key.length);

        Multihash mh = Multihash.deserialize(pid);
        PeerId peerId = PeerId.fromBase58(mh.toBase58());

        PubKey pubKey = extractPublicKey(peerId, entry);
        Objects.requireNonNull(pubKey);

        validate(pubKey, entry);

        Crypto.KeyType keyType = pubKey.getKeyType();

        return new Entry(peerId, keyType, getEOL(entry),
                entry.getValue().toStringUtf8(), entry.getSequence());
    }

    @Override
    public int compare(@NonNull IpnsService.Entry a, @NonNull IpnsService.Entry b) {

        long as = a.getSequence();
        long bs = b.getSequence();

        if (as > bs) {
            return 1;
        } else if (as < bs) {
            return -1;
        }

        Date at = a.getEol();
        Date bt = b.getEol();

        if (at.after(bt)) {
            return 1;
        } else if (bt.after(at)) {
            return -1;
        }
        return 0;
    }

    @NonNull
    private Date getEOL(Ipns.IpnsEntry entry) throws Exception {
        if (entry.getValidityType() != Ipns.IpnsEntry.ValidityType.EOL) {
            throw new Exception("validity type");
        }
        String date = new String(entry.getValidity().toByteArray());
        return getDate(date);
    }

    // ExtractPublicKey extracts a public key matching `pid` from the IPNS record,
    // if possible.
    //
    // This function returns (nil, nil) when no public key can be extracted and
    // nothing is malformed.
    @NonNull
    private PubKey extractPublicKey(PeerId pid, Ipns.IpnsEntry entry)
            throws Exception {


        if (entry.hasPubKey()) {
            byte[] pubKey = entry.getPubKey().toByteArray();

            PubKey pk = Key.unmarshalPublicKey(pubKey);

            PeerId expPid = PeerId.fromPubKey(pk);

            if (!Objects.equals(pid, expPid)) {
                throw new Exception("invalid peer");
            }
            return pk;
        }

        return extractPublicKey(pid);
    }

    // Validates validates the given IPNS entry against the given public key
    private void validate(@NonNull PubKey pk, Ipns.IpnsEntry entry) throws Exception {

        if (entry.hasSignatureV2()) {
            pk.verify(ipnsEntryDataForSigV2(entry), entry.getSignatureV2().toByteArray());
        } else if (entry.hasSignatureV1()) {
            pk.verify(ipnsEntryDataForSigV1(entry), entry.getSignatureV1().toByteArray());
        } else {
            throw new Exception("no signature");
        }

        if (new Date().after(getEOL(entry))) {
            throw new Exception("outdated");
        }
    }

    public static class Entry {
        @NonNull
        private final PeerId peerId;
        @NonNull
        private final Crypto.KeyType keyType;
        private final long sequence;
        @NonNull
        private final String value;
        @NonNull
        private final Date eol;

        public Entry(@NonNull PeerId peerId, @NonNull Crypto.KeyType keyType, @NonNull Date eol,
                     @NonNull String value, long sequence) {
            this.peerId = peerId;
            this.keyType = keyType;
            this.eol = eol;
            this.sequence = sequence;
            this.value = value;
        }

        @NonNull
        public Crypto.KeyType getKeyType() {
            return keyType;
        }

        @NonNull
        @Override
        public String toString() {
            return "Entry{" +
                    "peerId=" + peerId +
                    ", keyType=" + keyType +
                    ", sequence=" + sequence +
                    ", value='" + value + '\'' +
                    ", eol=" + eol +
                    '}';
        }

        @NonNull
        public Date getEol() {
            return eol;
        }

        @NonNull
        public PeerId getPeerId() {
            return peerId;
        }

        public long getSequence() {
            return sequence;
        }

        @NonNull
        public String getValue() {
            return value;
        }

        @NonNull
        public Cid getHash() throws Exception {
            return Cid.decode(value.replaceFirst(IPFS.IPFS_PATH, ""));
        }
    }
}
