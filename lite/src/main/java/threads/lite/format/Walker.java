package threads.lite.format;

import android.util.Pair;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Objects;
import java.util.Stack;

import merkledag.pb.Merkledag;
import threads.lite.cid.Cid;
import threads.lite.core.Cancellable;
import threads.lite.dag.NodeService;
import unixfs.pb.Unixfs;

public class Walker {

    private final NodeService nodeService;
    private final Merkledag.PBNode root;

    private Walker(@NonNull Merkledag.PBNode node, @NonNull NodeService nodeService) {
        this.root = node;
        this.nodeService = nodeService;
    }

    public static Walker createWalker(@NonNull Merkledag.PBNode node, @NonNull NodeService nodeService) {
        return new Walker(node, nodeService);
    }


    @Nullable
    public Stage next(@NonNull Cancellable cancellable, @NonNull Visitor visitor)
            throws Exception {


        if (!visitor.isRootVisited(true)) {
            Stage stage = visitor.peekStage();
            Objects.requireNonNull(stage);
            if (stage.getNode().equals(root)) {
                return stage;
            }
        }
        if (visitor.isPresent()) {

            boolean success = down(cancellable, visitor);
            if (success) {
                Stage stage = visitor.peekStage();
                Objects.requireNonNull(stage);
                return stage;
            }

            success = up(visitor);

            if (success) {
                return next(cancellable, visitor);
            }
        }
        return null;
    }

    private boolean up(@NonNull Visitor visitor) {

        if (visitor.isPresent()) {
            visitor.popStage();
        } else {
            return false;
        }
        if (visitor.isPresent()) {
            boolean result = nextChild(visitor);
            if (result) {
                return true;
            } else {
                return up(visitor);
            }
        } else {
            return false;
        }
    }


    private boolean nextChild(@NonNull Visitor visitor) {
        Stage stage = visitor.peekStage();
        Merkledag.PBNode activeNode = stage.getNode();

        if (stage.index() + 1 < activeNode.getLinksCount()) {
            stage.incrementIndex();
            return true;
        }

        return false;
    }


    public boolean down(@NonNull Cancellable cancellable, @NonNull Visitor visitor)
            throws Exception {

        Merkledag.PBNode child = fetchChild(cancellable, visitor);
        if (child != null) {
            visitor.pushActiveNode(child);
            return true;
        }
        return false;
    }


    @Nullable
    private Merkledag.PBNode fetchChild(@NonNull Cancellable cancellable, @NonNull Visitor visitor)
            throws Exception {
        Stage stage = visitor.peekStage();
        Merkledag.PBNode activeNode = stage.getNode();
        int index = stage.index();
        Objects.requireNonNull(activeNode);

        if (index >= activeNode.getLinksCount()) {
            return null;
        }

        return nodeService.getNode(cancellable, getChild(activeNode, index));
    }

    @NonNull
    public Merkledag.PBNode getRoot() {
        return root;
    }

    public Pair<Stack<Stage>, Long> seek(@NonNull Cancellable cancellable,
                                         @NonNull Stack<Stage> stack,
                                         long offset) throws Exception {

        if (offset < 0) {
            throw new Exception("invalid offset");
        }

        if (offset == 0) {
            return Pair.create(stack, 0L);
        }

        long left = offset;
        Stage peek = stack.peek();
        Merkledag.PBNode node = peek.getNode();

        if (node.getLinksCount() > 0) {
            // Internal node, should be a `mdag.ProtoNode` containing a
            // `unixfs.FSNode` (see the `balanced` package for more details).
            Unixfs.Data unixData = peek.getData();

            // If there aren't enough size hints don't seek
            // (see the `io.EOF` handling error comment below).
            if (unixData.getBlocksizesCount() != node.getLinksCount()) {
                throw new Exception("ErrSeekNotSupported");
            }


            // Internal nodes have no data, so just iterate through the
            // sizes of its children (advancing the child index of the
            // `dagWalker`) to find where we need to go down to next in
            // the search
            for (int i = 0; i < unixData.getBlocksizesCount(); i++) {

                long childSize = unixData.getBlocksizes(i);

                if (childSize > left) {
                    stack.peek().setIndex(i);

                    Merkledag.PBNode fetched = nodeService.getNode(cancellable, getChild(node, i));
                    stack.push(new Stage(fetched));

                    return seek(cancellable, stack, left);
                }
                left -= childSize;
            }
        }

        return Pair.create(stack, left);
    }

    @NonNull
    public Cid getChild(@NonNull Merkledag.PBNode node, int index) {
        return Cid.fromArray(node.getLinks(index).getHash().toByteArray());
    }

    public Pair<Stack<Stage>, Long> seek(@NonNull Cancellable cancellable, long offset)
            throws Exception {

        Stack<Stage> stack = new Stack<>();
        stack.push(new Stage(getRoot()));

        return seek(cancellable, stack, offset);

    }
}

