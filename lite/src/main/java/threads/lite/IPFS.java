package threads.lite;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.common.io.ByteStreams;
import com.google.common.primitives.Bytes;
import com.google.protobuf.ByteString;

import net.luminis.quic.Reachability;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ConnectException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.function.Function;

import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.core.Cancellable;
import threads.lite.core.Connection;
import threads.lite.core.Progress;
import threads.lite.data.BLOCKS;
import threads.lite.data.BlockSupplier;
import threads.lite.format.BlockStore;
import threads.lite.format.Link;
import threads.lite.host.DnsResolver;
import threads.lite.host.LiteHost;
import threads.lite.host.LiteHostCertificate;
import threads.lite.host.PeerInfo;
import threads.lite.host.Session;
import threads.lite.ident.IdentityService;
import threads.lite.ipns.IpnsService;
import threads.lite.push.Push;
import threads.lite.push.PushService;
import threads.lite.relay.Reservation;
import threads.lite.utils.ProgressStream;
import threads.lite.utils.Reader;
import threads.lite.utils.ReaderInputStream;
import threads.lite.utils.ReaderProgress;
import threads.lite.utils.ReaderStream;
import threads.lite.utils.Resolver;
import threads.lite.utils.Stream;

public class IPFS {

    public static final String TIME_FORMAT_IPFS = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSS'Z'";  // RFC3339Nano = "2006-01-02T15:04:05.999999999Z07:00"
    public static final String MPLEX_PROTOCOL = "/mplex/6.7.0";
    public static final String HOLE_PUNCH_PROTOCOL = "/libp2p/dcutr";
    public static final String RELAY_PROTOCOL_HOP = "/libp2p/circuit/relay/0.2.0/hop";
    public static final String RELAY_PROTOCOL_STOP = "/libp2p/circuit/relay/0.2.0/stop";
    public static final String NOISE_PROTOCOL = "/noise";
    public static final String DHT_PROTOCOL = "/ipfs/kad/1.0.0";
    public static final String PUSH_PROTOCOL = "/ipfs/push/1.0.0";
    public static final String AUTONAT_PROTOCOL = "/libp2p/autonat/1.0.0";
    public static final String STREAM_PROTOCOL = "/multistream/1.0.0";
    public static final String BITSWAP_PROTOCOL = "/ipfs/bitswap/1.2.0";
    public static final String IDENTITY_PROTOCOL = "/ipfs/id/1.0.0";
    public static final String INDEX_HTML = "index.html";
    public static final String AGENT_PREFIX = "lite";
    public static final String AGENT = AGENT_PREFIX + "/0.9.0/";
    public static final String PROTOCOL_VERSION = "ipfs/0.1.0";
    public static final String IPFS_PATH = "/ipfs/";
    public static final String IPNS_PATH = "/ipns/";
    public static final String LIB2P_DNS = "bootstrap.libp2p.io"; // IPFS BOOTSTRAP DNS
    public static final String NA = "na";
    public static final String ALPN = "libp2p";

    public static final int CHUNK_SIZE = 262144;
    public static final long RESOLVE_MAX_TIME = 30000; // 30 sec
    public static final int MAX_STREAMS = 10000;
    public static final int GRACE_PERIOD = 15;
    public static final int GRACE_PERIOD_RESERVATION = 60 * 60; // 60 min
    public static final int RESOLVE_TIMEOUT = 1000; // 1 sec
    // MessageSizeMax is a soft (recommended) maximum for network messages.
    // One can write more, as the interface is a stream. But it is useful
    // to bunch it up into multiple read/writes when the whole message is
    // a single, large serialized object.
    public static final int MESSAGE_SIZE_MAX = 1 << 22; // 4 MB
    @NonNull
    public static final List<String> DHT_BOOTSTRAP_NODES = new ArrayList<>(Arrays.asList(
            "QmNnooDu7bfjPFoTZYxMNLWUQJyrVwtbZg5gBMjTezGAJN", // default dht peer
            "QmQCU2EcMqAqQPR2i9bChDtGNJchTbq5TbXJJ16u19uLTa", // default dht peer
            "QmbLHAnMoJPWSCR5Zhtx6BHJX9KiKNN6tpvbUcqanj75Nb", // default dht peer
            "QmcZf59bWwK5XFi76CZX8cbJ4BhTzzA3gU1ZjYZcYW3dwt" // default dht peer

    ));
    public static final int DHT_TABLE_SIZE = 200;
    public static final int DHT_ALPHA = 25;
    public static final int DHT_CONCURRENCY = 5;
    public static final int CONNECT_TIMEOUT = 5;
    public static final int DHT_REQUEST_READ_TIMEOUT = 5;
    public static final long IPNS_DURATION = 6; // 6 hours duration
    public static final int BITSWAP_REQUEST_DELAY = 1500; // 1,5 sec

    @NonNull
    private static final Duration RECORD_EOL = Duration.ofHours(24);
    private static final String PRIVATE_KEY = "privateKey";
    private static final String PUBLIC_KEY = "publicKey";
    private static final String TAG = IPFS.class.getSimpleName();
    private static final String PREF_KEY = "liteKey";
    // rough estimates on expected sizes
    private static final int ROUGH_LINK_BLOCK_SIZE = 1 << 13; // 8KB
    private static final int ROUGH_LINK_SIZE = 34 + 8 + 5;// sha256 multihash + size + no name + protobuf framing
    // DefaultLinksPerBlock governs how the importer decides how many links there
    // will be per block. This calculation is based on expected distributions of:
    //  * the expected distribution of block sizes
    //  * the expected distribution of link sizes
    //  * desired access speed
    // For now, we use:
    //
    //   var roughLinkBlockSize = 1 << 13 // 8KB
    //   var roughLinkSize = 34 + 8 + 5   // sha256 multihash + size + no name
    //                                    // + protobuf framing
    //   var DefaultLinksPerBlock = (roughLinkBlockSize / roughLinkSize)
    //                            = ( 8192 / 47 )
    //                            = (approximately) 174
    public static final int LINKS_PER_BLOCK = ROUGH_LINK_BLOCK_SIZE / ROUGH_LINK_SIZE;
    private static final String LITE_KEY = "LiteKey";
    private static final String PORT_KEY = "portKey";
    private static volatile IPFS INSTANCE = null;
    @NonNull
    private final BlockStore blockstore;
    @NonNull
    private final LiteHost host;


    private IPFS(@NonNull Context context) throws Exception {
        int port = getPort(context);
        KeyPair keypair = getKeyPair(context);
        this.blockstore = BLOCKS.getInstance(context);
        this.host = new LiteHost(keypair, blockstore, port);
    }

    public static void setPort(Context context, int port) {
        SharedPreferences sharedPref = context.getSharedPreferences(LITE_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(PORT_KEY, port);
        editor.apply();
    }

    public static int getPort(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(LITE_KEY, Context.MODE_PRIVATE);
        return sharedPref.getInt(PORT_KEY, 5001);
    }

    private static void setPublicKey(@NonNull Context context, @NonNull String key) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(PUBLIC_KEY, key);
        editor.apply();
    }

    @SuppressWarnings("UnusedReturnValue")
    public static long copy(InputStream source, OutputStream sink) throws IOException {
        return ByteStreams.copy(source, sink);
    }

    public static void copy(@NonNull InputStream source,
                            @NonNull OutputStream sink,
                            @NonNull ReaderProgress progress) throws IOException {
        long nread = 0L;
        byte[] buf = new byte[4096];
        int remember = 0;
        int n;
        while ((n = source.read(buf)) > 0) {
            sink.write(buf, 0, n);
            nread += n;

            if (progress.doProgress()) {
                if (progress.getSize() > 0) {
                    int percent = (int) ((nread * 100.0f) / progress.getSize());
                    if (remember < percent) {
                        remember = percent;
                        progress.setProgress(percent);
                    }
                }
            }
        }
    }

    @NonNull
    private static String getPublicKey(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        return Objects.requireNonNull(sharedPref.getString(PUBLIC_KEY, ""));

    }

    private static void setPrivateKey(@NonNull Context context, @NonNull String key) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(PRIVATE_KEY, key);
        editor.apply();
    }

    @NonNull
    private static String getPrivateKey(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        return Objects.requireNonNull(sharedPref.getString(PRIVATE_KEY, ""));

    }

    @NonNull
    public static IPFS getInstance(@NonNull Context context) throws Exception {
        if (INSTANCE == null) {
            synchronized (IPFS.class) {
                if (INSTANCE == null) {
                    INSTANCE = new IPFS(context);
                }
            }
        }
        return INSTANCE;
    }


    @NonNull
    private KeyPair getKeyPair(@NonNull Context context)
            throws NoSuchAlgorithmException, InvalidKeySpecException {

        if (!getPrivateKey(context).isEmpty() && !getPublicKey(context).isEmpty()) {

            Base64.Decoder decoder = Base64.getDecoder();

            byte[] privateKeyData = decoder.decode(getPrivateKey(context));
            byte[] publicKeyData = decoder.decode(getPublicKey(context));

            PublicKey publicKey = KeyFactory.getInstance("RSA").
                    generatePublic(new X509EncodedKeySpec(publicKeyData));
            PrivateKey privateKey = KeyFactory.getInstance("RSA").
                    generatePrivate(new PKCS8EncodedKeySpec(privateKeyData));

            return new KeyPair(publicKey, privateKey);

        } else {

            KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
            keyGen.initialize(2048, LiteHostCertificate.ThreadLocalInsecureRandom.current());
            KeyPair keypair = keyGen.generateKeyPair();

            Base64.Encoder encoder = Base64.getEncoder();
            setPrivateKey(context, encoder.encodeToString(keypair.getPrivate().getEncoded()));
            setPublicKey(context, encoder.encodeToString(keypair.getPublic().getEncoded()));
            return keypair;
        }
    }

    @NonNull
    public PeerId getPeerId(@NonNull String name) throws Exception {
        return PeerId.decodeName(name);
    }

    @NonNull
    public PeerInfo getIdentity() {
        return host.getIdentity();
    }

    public int getPort() {
        return host.getPort();
    }

    @NonNull
    public Cid storeFile(@NonNull File target) throws Exception {
        try (FileInputStream inputStream = new FileInputStream(target)) {
            return storeInputStream(inputStream);
        }
    }

    public CompletableFuture<Void> provide(@NonNull Session session,
                                           @NonNull Cid cid,
                                           @NonNull Cancellable cancellable) {
        return session.provide(cancellable, cid);
    }

    public boolean has(@NonNull Cid cid) {
        return blockstore.hasBlock(cid);
    }

    public void rm(@NonNull Cid cid) throws Exception {
        List<Cid> cids = getBlocks(cid);
        cids.add(cid);
        blockstore.deleteBlocks(cids);
    }

    @NonNull
    public List<Cid> getBlocks(@NonNull Cid cid) throws Exception {
        return Stream.getBlocks(blockstore, cid);
    }

    @NonNull
    public Cid storeData(byte[] data) throws Exception {

        try (InputStream inputStream = new ByteArrayInputStream(data)) {
            return storeInputStream(inputStream);
        }
    }

    @NonNull
    public Cid storeText(@NonNull String content) throws Exception {

        try (InputStream inputStream = new ByteArrayInputStream(content.getBytes())) {
            return storeInputStream(inputStream);
        }
    }

    public void storeToFile(@NonNull Session session, @NonNull File file,
                            @NonNull Cid cid, @NonNull Cancellable cancellable)
            throws Exception {

        try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            storeToOutputStream(session, fileOutputStream, cid, cancellable);
        }
    }

    public void storeToFile(@NonNull Session session, @NonNull File file,
                            @NonNull Cid cid, @NonNull Progress progress)
            throws Exception {

        try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            storeToOutputStream(session, fileOutputStream, cid, progress);
        }
    }

    @NonNull
    public Cid storeInputStream(@NonNull InputStream inputStream,
                                @NonNull Progress progress, long size) throws Exception {

        return Stream.readInputStream(blockstore,
                new ReaderInputStream(inputStream, progress, size));

    }

    @NonNull
    public Cid storeInputStream(@NonNull InputStream inputStream) throws Exception {
        return Stream.readInputStream(blockstore,
                new ReaderInputStream(inputStream, 0));
    }

    @Nullable
    public String getText(@NonNull Session session, @NonNull Cid cid, @NonNull Cancellable cancellable)
            throws Exception {

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            storeToOutputStream(session, outputStream, cid, cancellable);
            return outputStream.toString();
        }
    }

    public void storeToOutputStream(@NonNull Session session,
                                    @NonNull OutputStream outputStream,
                                    @NonNull Cid cid,
                                    @NonNull Progress progress)
            throws Exception {

        long totalRead = 0L;
        int remember = 0;

        Reader reader = getReader(session, cid, progress);
        long size = reader.getSize();

        do {
            if (progress.isCancelled()) {
                throw new Exception("cancelled");
            }

            ByteString buffer = reader.loadNextData();
            if (buffer == null || buffer.size() <= 0) {
                return;
            }
            outputStream.write(buffer.toByteArray());

            // calculate progress
            totalRead += buffer.size();
            if (progress.doProgress()) {
                if (size > 0) {
                    int percent = (int) ((totalRead * 100.0f) / size);
                    if (remember < percent) {
                        remember = percent;
                        progress.setProgress(percent);
                    }
                }
            }
        } while (true);
    }

    public void storeToOutputStream(@NonNull Session session,
                                    @NonNull OutputStream outputStream,
                                    @NonNull Cid cid,
                                    @NonNull Cancellable cancellable)
            throws Exception {

        Reader reader = getReader(session, cid, cancellable);

        do {
            ByteString buffer = reader.loadNextData();
            if (buffer == null || buffer.size() <= 0) {
                return;
            }
            outputStream.write(buffer.toByteArray());
        } while (true);
    }

    @NonNull
    public byte[] getData(@NonNull Session session, @NonNull Cid cid, @NonNull Progress progress)
            throws Exception {

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            storeToOutputStream(session, outputStream, cid, progress);
            return outputStream.toByteArray();
        }
    }

    @NonNull
    public byte[] getData(@NonNull Session session, @NonNull Cid cid, @NonNull Cancellable cancellable)
            throws Exception {

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            storeToOutputStream(session, outputStream, cid, cancellable);
            return outputStream.toByteArray();
        }
    }

    @NonNull
    public Cid removeFromDirectory(@NonNull Cid dir, @NonNull String name) throws Exception {
        return Stream.removeFromDirectory(blockstore, dir, name);
    }

    @NonNull
    public Cid addLinkToDirectory(@NonNull Cid dir, @NonNull Link link) throws Exception {
        return Stream.addLinkToDirectory(blockstore, dir, link);
    }

    @NonNull
    public Cid createDirectory(@NonNull List<Link> links) throws Exception {
        return Stream.createDirectory(blockstore, links);
    }

    @NonNull
    public Cid createEmptyDirectory() throws Exception {
        return Stream.createEmptyDirectory(blockstore);
    }

    @NonNull
    public PeerId decodeName(@NonNull String name) throws Exception {
        return PeerId.decodeName(name);
    }

    @NonNull
    public PeerId self() {
        return host.self();
    }

    @NonNull
    public CompletableFuture<PeerInfo> getPeerInfo(@NonNull Connection conn) {
        return IdentityService.getPeerInfo(conn);
    }


    @NonNull
    public Session createSession(Consumer<BlockSupplier> supplier,
                                 boolean findProvidersActive,
                                 boolean sendReplyActive) {
        return host.createSession(supplier, findProvidersActive, sendReplyActive);
    }

    @NonNull
    public Session createSession() {
        return createSession(blockSupplier -> {
        }, true, true);
    }

    @NonNull
    public Cid resolve(@NonNull Session session,
                       @NonNull Cid root, @NonNull List<String> path,
                       @NonNull Cancellable cancellable) throws Exception {
        return Resolver.resolveNode(cancellable, blockstore, session, root, path);
    }

    @NonNull
    public Set<Reservation> reservations() {
        return host.reservations();
    }


    public CompletableFuture<Void> publishName(@NonNull Session session, @NonNull Cid cid,
                                               long sequence, @NonNull Cancellable cancellable) {
        return publishName(session, cancellable, host.getKeypair(), IPFS_PATH + cid.String(),
                self(), sequence);

    }

    public CompletableFuture<Void> publishName(@NonNull Session session, @NonNull String name,
                                               int sequence, @NonNull Cancellable cancellable) {

        return publishName(session, cancellable, host.getKeypair(), name, self(), sequence);
    }

    public void clearDatabase() {
        blockstore.clear();
    }


    public CompletableFuture<Void> findProviders(@NonNull Session session,
                                                 @NonNull Consumer<Multiaddr> consumer,
                                                 @NonNull Cid cid,
                                                 @NonNull Cancellable cancellable) {
        return session.findProviders(cancellable, consumer, cid);
    }


    @NonNull
    public Multiaddr remoteAddress(@NonNull Connection conn) {
        return Multiaddr.create(conn.getRemoteAddress());
    }

    public boolean isDir(@NonNull Session session, @NonNull Cid cid, @NonNull Cancellable cancellable)
            throws Exception {
        return Stream.isDir(cancellable, blockstore, session, cid);
    }


    public boolean hasLink(@NonNull Session session, @NonNull Cid cid,
                           @NonNull String name, @NonNull Cancellable cancellable)
            throws Exception {
        return Stream.hasLink(cancellable, blockstore, session, cid, name);
    }

    @NonNull
    public List<Link> links(@NonNull Session session, @NonNull Cid cid, boolean resolveChildren,
                            @NonNull Cancellable cancellable) throws Exception {

        List<Link> result = new ArrayList<>();
        links(session, cid, result::add, resolveChildren, cancellable);
        return result;
    }

    public void links(@NonNull Session session, @NonNull Cid cid, @NonNull Consumer<Link> consumer,
                      boolean resolveChildren, @NonNull Cancellable cancellable)
            throws Exception {

        Stream.ls(cancellable, link -> {
            if (!link.getName().isEmpty()) {
                consumer.accept(link);
            }
        }, blockstore, session, cid, resolveChildren);
    }

    @NonNull
    public List<Link> allLinks(@NonNull Session session, @NonNull Cid cid, boolean resolveChildren,
                               @NonNull Cancellable cancellable) throws Exception {

        List<Link> links = new ArrayList<>();
        Stream.ls(cancellable, links::add, blockstore, session, cid, resolveChildren);
        return links;
    }

    @NonNull
    public Reader getReader(@NonNull Session session, @NonNull Cid cid, @NonNull Cancellable cancellable)
            throws Exception {
        return Reader.getReader(cancellable, blockstore, session, cid);
    }

    public void setIncomingPush(@Nullable Consumer<Push> incomingPush) {
        this.host.setIncomingPush(incomingPush);
    }

    private CompletableFuture<Void> publishName(@NonNull Session session,
                                                @NonNull Cancellable cancellable,
                                                @NonNull KeyPair keypair,
                                                @NonNull String name,
                                                @NonNull PeerId id,
                                                long sequence) {
        CompletableFuture<Void> done = new CompletableFuture<>();

        try {
            Date eol = Date.from(new Date().toInstant().plus(RECORD_EOL));

            Duration duration = Duration.ofHours(IPFS.IPNS_DURATION);
            ipns.pb.Ipns.IpnsEntry record =
                    IpnsService.create(keypair.getPrivate(), name.getBytes(), sequence, eol, duration);

            PublicKey pk = keypair.getPublic();

            record = IpnsService.embedPublicKey(pk, record);

            byte[] bytes = record.toByteArray();

            byte[] ipns = IPFS.IPNS_PATH.getBytes();
            byte[] ipnsKey = Bytes.concat(ipns, id.getBytes());
            session.putValue(cancellable, ipnsKey, bytes)
                    .whenComplete((unused, throwable) -> {
                                if (throwable != null) {
                                    done.completeExceptionally(throwable);
                                } else {
                                    done.complete(unused);
                                }
                            }
                    );
        } catch (Throwable throwable) {
            done.completeExceptionally(throwable);
        }
        return done;
    }

    @NonNull
    public InputStream getInputStream(@NonNull Session session, @NonNull Cid cid,
                                      @NonNull Cancellable cancellable)
            throws Exception {
        Reader reader = getReader(session, cid, cancellable);
        return new ReaderStream(reader);
    }

    @NonNull
    public InputStream getInputStream(@NonNull Session session, @NonNull Cid cid,
                                      @NonNull Progress progress)
            throws Exception {
        Reader reader = getReader(session, cid, progress);
        return new ProgressStream(reader, progress);

    }

    @Nullable
    public IpnsService.Entry resolveName(@NonNull Session session, @NonNull PeerId peerId,
                                         long last, @NonNull Cancellable cancellable) {

        long time = System.currentTimeMillis();

        AtomicReference<IpnsService.Entry> resolvedName = new AtomicReference<>(null);
        try {
            AtomicLong timeout = new AtomicLong(System.currentTimeMillis()
                    + IPFS.RESOLVE_MAX_TIME);

            byte[] ipns = IPFS.IPNS_PATH.getBytes();
            byte[] ipnsKey = Bytes.concat(ipns, peerId.getBytes());

            session.searchValue(
                    () -> (timeout.get() < System.currentTimeMillis()) || cancellable.isCancelled(),
                    entry -> {

                        long sequence = entry.getSequence();

                        LogUtils.debug(TAG, "IpnsEntry : " + entry +
                                (System.currentTimeMillis() - time));

                        if (sequence < last) {
                            // newest value already available
                            LogUtils.debug(TAG, "newest value " + sequence);
                            timeout.set(System.currentTimeMillis());
                            return;
                        }


                        resolvedName.set(entry);
                        timeout.set(System.currentTimeMillis() + IPFS.RESOLVE_TIMEOUT);

                    }, ipnsKey).get(IPFS.RESOLVE_MAX_TIME, TimeUnit.MILLISECONDS);

        } catch (Throwable ignore) {
            // nothing to do here
        }

        LogUtils.debug(TAG, "Finished resolve name " + peerId.toBase58() + " " +
                (System.currentTimeMillis() - time));


        return resolvedName.get();
    }

    public CompletableFuture<Void> notify(@NonNull Connection conn, @NonNull String content) {
        return PushService.notify(conn, content);
    }

    public CompletableFuture<Void> findPeer(@NonNull Session session,
                                            @NonNull PeerId peerId,
                                            @NonNull Consumer<Multiaddr> consumer,
                                            @NonNull Cancellable cancellable) {
        return session.findPeer(cancellable, consumer, peerId);
    }


    public void updateNetwork() {
        host.updateNetwork();
    }

    public boolean hasReservations() {
        return host.hasReservations();
    }

    public int numReservations() {
        return reservations().size();
    }

    // todo future CompletableFuture<QuicConnection>
    @NonNull
    public Connection dial(@NonNull Session session, @NonNull Multiaddr address,
                           int timeout, int maxIdleTimeoutInSeconds, int initialMaxStreams,
                           int initialMaxStreamData)
            throws ConnectException, CancellationException {
        return host.dial(session, address, timeout, maxIdleTimeoutInSeconds,
                initialMaxStreams, initialMaxStreamData);
    }

    public void setConnectConsumer(@Nullable Consumer<Connection> connectConsumer) {
        host.setConnectConsumer(connectConsumer);
    }

    public void setClosedConsumer(@Nullable Consumer<Connection> closedConsumer) {
        host.setClosedConsumer(closedConsumer);
    }

    public void setReachabilityConsumer(@Nullable Consumer<Reachability> reachabilityConsumer) {
        host.setReachabilityConsumer(reachabilityConsumer);
    }

    public int numServerConnections() {
        return host.numServerConnections();
    }


    public Set<Multiaddr> getBootstrap() {
        return host.getBootstrap();
    }

    public boolean autonat(long timeout) {
        return host.autonat(timeout);
    }

    public Set<Reservation> reservations(long timeout) {
        return host.reservations(timeout);
    }

    public void setIsGated(@Nullable Function<PeerId, Boolean> isGated) {
        host.setIsGated(isGated);
    }

    public List<Multiaddr> resolveDnsaddr(@NonNull Multiaddr multiaddr) throws Exception {
        if (!multiaddr.isDnsaddr()) {
            throw new Exception("not a dnsaddr");
        }
        return DnsResolver.resolveDnsaddr(host.protocol.get(), multiaddr);
    }

    @NonNull
    public LiteHost getHost() {
        return host;
    }
}
