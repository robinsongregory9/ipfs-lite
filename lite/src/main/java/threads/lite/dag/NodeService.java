package threads.lite.dag;

import androidx.annotation.NonNull;

import java.util.Objects;

import merkledag.pb.Merkledag;
import threads.lite.cid.Cid;
import threads.lite.core.Cancellable;
import threads.lite.format.Block;

public interface NodeService {

    static NodeService createNodeService(@NonNull BlockService blockService) {
        return (closeable, cid) -> {

            Block block = blockService.getBlock(closeable, cid);
            Objects.requireNonNull(block, "Block not found");
            return block.getNode();
        };
    }

    @NonNull
    Merkledag.PBNode getNode(@NonNull Cancellable cancellable, @NonNull Cid cid)
            throws Exception;


}
