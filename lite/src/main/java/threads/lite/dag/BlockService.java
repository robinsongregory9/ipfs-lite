package threads.lite.dag;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import threads.lite.cid.Cid;
import threads.lite.core.Cancellable;
import threads.lite.format.Block;
import threads.lite.format.BlockStore;
import threads.lite.host.Session;

public interface BlockService {

    static BlockService createBlockService(@NonNull final BlockStore bs,
                                           @NonNull final Session session) {
        return (closeable, cid) -> {
            Block block = bs.getBlock(cid);
            if (block != null) {
                return block;
            }
            return session.getBlock(closeable, cid);
        };
    }

    @Nullable
    Block getBlock(@NonNull Cancellable cancellable, @NonNull Cid cid) throws Exception;


}
