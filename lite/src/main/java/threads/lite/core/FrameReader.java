package threads.lite.core;

import java.nio.ByteBuffer;

public interface FrameReader {

    Frame getFrame(ByteBuffer data) throws Exception;
}
