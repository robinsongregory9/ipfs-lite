package threads.lite.core;

import java.nio.ByteBuffer;

public interface Frame {
    int getLength();

    ByteBuffer getData();

    void put(byte[] bytes);

    void put(byte value);

    int position();
}
