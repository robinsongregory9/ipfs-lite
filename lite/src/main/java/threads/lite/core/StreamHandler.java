package threads.lite.core;


import androidx.annotation.NonNull;

import net.luminis.quic.QuicStream;

import java.nio.ByteBuffer;

public interface StreamHandler {

    void throwable(Stream stream, Throwable throwable);

    void token(Stream stream, String token) throws Exception;

    void fin();

    void data(Stream stream, ByteBuffer data) throws Exception;

    @NonNull
    Transport getTransport(QuicStream quicStream);

    void streamTerminated(QuicStream quicStream);
}
