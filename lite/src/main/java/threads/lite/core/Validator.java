package threads.lite.core;

import androidx.annotation.NonNull;

import threads.lite.ipns.IpnsService;

public interface Validator {


    @NonNull
    IpnsService.Entry validate(byte[] key, byte[] value) throws Exception;

    // return 1 for rec and -1 for cmp and 0 for both equal
    int compare(@NonNull IpnsService.Entry rec, @NonNull IpnsService.Entry cmp);

}
