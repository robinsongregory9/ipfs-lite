package threads.lite.core;

import androidx.annotation.NonNull;

import java.net.InetSocketAddress;
import java.util.concurrent.CompletableFuture;

public interface Connection {


    void close();

    boolean isConnected();

    @NonNull
    InetSocketAddress getRemoteAddress();

    @NonNull
    CompletableFuture<Stream> createStream(@NonNull StreamHandler streamHandler);
}
