package threads.lite.core;


import java.nio.ByteBuffer;

public interface Package {

    Stream getStream();

    ByteBuffer getData();

    // if the stream data is final
    boolean isFinal();

}
