package threads.lite.core;

import android.util.Pair;

import net.luminis.quic.QuicStream;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Objects;

import threads.lite.LogUtils;
import threads.lite.host.LiteFrame;
import threads.lite.mplex.Mplex;
import threads.lite.mplex.MuxFlag;
import threads.lite.mplex.MuxId;
import threads.lite.mplex.MuxedFrame;
import threads.lite.noise.Noise;
import threads.lite.noise.SecuredFrame;
import threads.lite.utils.DataHandler;

public interface Transport {
    String TAG = Transport.class.getSimpleName();

    static FrameReader getFrameReader(Transport transport) {
        return data -> {
            switch (transport.getType()) {
                case MUXED:
                    return muxVarintReader(data);
                case HANDSHAKE:
                    return handshakeReader(data);
                case SECURED:
                    return securedReader(data);
                case PLAIN:
                    return varintReader(data);
                default:
                    throw new IllegalStateException("not supported transport reader");
            }
        };

    }

    private static Frame handshakeReader(ByteBuffer data) {
        Objects.requireNonNull(data);
        int length = Noise.bytesToBigEndianShort(data);
        LogUtils.debug(TAG, "handshakeReader Length " + length);
        return new LiteFrame(length);
    }

    private static Frame securedReader(ByteBuffer data) {
        Objects.requireNonNull(data);
        int length = Noise.bytesToBigEndianShort(data);
        LogUtils.debug(TAG, "handshakeReader Length " + length);
        return new SecuredFrame(length);
    }


    private static Frame muxVarintReader(ByteBuffer data) throws IOException {
        Objects.requireNonNull(data);

        int header = DataHandler.readUnsignedLeb128(data);
        LogUtils.error(TAG, "muxReader Header " + header);
        int length = DataHandler.readUnsignedLeb128(data);
        LogUtils.error(TAG, "muxReader Length " + length);

        Pair<MuxId, MuxFlag> pair = Mplex.decodeHeader(header);

        return new MuxedFrame(pair.first, pair.second, length);

    }

    private static Frame varintReader(ByteBuffer data) throws IOException {
        Objects.requireNonNull(data);
        int length = DataHandler.readUnsignedLeb128(data);
        LogUtils.debug(TAG, "plainReader Length " + length);
        return new LiteFrame(length);

    }

    Type getType();

    Stream getStream(QuicStream quicStream, Frame frame);

    enum Type {PLAIN, HANDSHAKE, SECURED, MUXED}
}
