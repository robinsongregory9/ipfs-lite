package threads.lite.autonat;

import androidx.annotation.Nullable;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import threads.lite.cid.Multiaddr;

public class Autonat extends ConcurrentHashMap<Multiaddr, Integer> {


    public void addAddr(Multiaddr multiaddr) {
        Integer match = get(multiaddr);
        if (match == null) {
            put(multiaddr, 1);
        } else {
            int incMatch = ++match;
            put(multiaddr, incMatch);
        }
    }

    @Nullable
    public Multiaddr winner() {
        Map.Entry<Multiaddr, Integer> winner = null;
        for (Map.Entry<Multiaddr, Integer> entry : this.entrySet()) {
            if (winner == null) {
                winner = entry;
            } else {
                if (winner.getValue() < entry.getValue()) {
                    winner = entry;
                }
            }
        }

        // at least the winner should be have 3 matched
        if (winner != null) {
            if (winner.getValue() >= 3) {
                return winner.getKey();
            }
        }
        return null;
    }
}
