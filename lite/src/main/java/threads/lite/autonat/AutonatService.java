package threads.lite.autonat;

import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import net.luminis.quic.QuicStream;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

import autonat.pb.Autonat;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.core.Connection;
import threads.lite.core.Stream;
import threads.lite.core.StreamHandler;
import threads.lite.core.Transport;
import threads.lite.host.LiteTransport;
import threads.lite.host.Session;
import threads.lite.ident.IdentityService;
import threads.lite.utils.DataHandler;

public class AutonatService {

    private static final String TAG = AutonatService.class.getSimpleName();


    public static CompletableFuture<Multiaddr> autonat(Session session, Connection conn) {
        CompletableFuture<Multiaddr> future = new CompletableFuture<>();
        IdentityService.getPeerInfo(conn)
                .whenComplete((peerInfo, throwable) -> {
                    if (throwable != null) {
                        future.completeExceptionally(throwable);
                    } else {
                        if (peerInfo.hasProtocol(IPFS.AUTONAT_PROTOCOL)) {
                            Set<Multiaddr> addresses = session.getHost().networkListenAddresses();
                            Multiaddr observed = peerInfo.getObserved();
                            if (observed != null) {
                                addresses.add(observed);
                            }
                            AutonatService.getObserved(conn, session.getHost().self(), addresses)
                                    .whenComplete((multiaddr, throwable1) -> {
                                        if (throwable1 != null) {
                                            future.completeExceptionally(throwable1);
                                        } else {
                                            future.complete(multiaddr);
                                        }
                                    });

                        }
                    }
                });
        return future;
    }

    private static CompletableFuture<Multiaddr> getObserved(
            @NonNull Connection conn, @NonNull PeerId peerId, @NonNull Set<Multiaddr> addresses) {
        CompletableFuture<Multiaddr> multiaddrCompletableFuture = new CompletableFuture<>();

        AutonatService.dial(conn, peerId, addresses).whenComplete((message, throwable) -> {
            if (throwable != null) {
                multiaddrCompletableFuture.completeExceptionally(throwable);
            } else {
                if (message.hasDialResponse()) {
                    Autonat.Message.DialResponse dialResponse =
                            message.getDialResponse();
                    if (dialResponse.hasStatusText()) {
                        LogUtils.error(TAG, "Autonat Dial Text : "
                                + dialResponse.getStatusText());
                    }
                    if (dialResponse.hasStatus()) {
                        if (dialResponse.getStatus() == Autonat.Message.ResponseStatus.OK) {
                            try {
                                ByteString raw = dialResponse.getAddr();
                                multiaddrCompletableFuture.complete(Multiaddr.create(raw));
                            } catch (Throwable exception) {
                                multiaddrCompletableFuture.completeExceptionally(exception);
                            }
                        } else {
                            multiaddrCompletableFuture.completeExceptionally(
                                    new Exception(dialResponse.getStatusText())
                            );
                        }
                    } else {
                        multiaddrCompletableFuture.completeExceptionally(
                                new Exception("invalid status")
                        );
                    }
                }
            }
        });

        return multiaddrCompletableFuture;
    }

    @NonNull
    private static CompletableFuture<Autonat.Message> dial(
            @NonNull Connection conn, @NonNull PeerId peerId, @NonNull Set<Multiaddr> addresses) {

        CompletableFuture<Autonat.Message> response = new CompletableFuture<>();
        Autonat.Message.PeerInfo.Builder peerInfoBuilder = Autonat.Message.PeerInfo.newBuilder();

        peerInfoBuilder.setId(ByteString.copyFrom(peerId.getBytes()));

        if (addresses.isEmpty()) {
            response.completeExceptionally(new RuntimeException("No addresses defined"));
            return response;
        }

        for (Multiaddr addr : addresses) {
            peerInfoBuilder.addAddrs(ByteString.copyFrom(addr.getBytes()));
        }

        Autonat.Message.Dial dial = Autonat.Message.Dial.newBuilder()
                .setPeer(peerInfoBuilder.build()).build();

        Autonat.Message message = Autonat.Message.newBuilder().
                setType(Autonat.Message.MessageType.DIAL).
                setDial(dial).build();


        conn.createStream(new StreamHandler() {
                    @Override
                    public void throwable(Stream stream, Throwable throwable) {
                        response.completeExceptionally(throwable);
                    }

                    @Override
                    public void token(Stream stream, String token) throws Exception {
                        if (!Arrays.asList(IPFS.STREAM_PROTOCOL, IPFS.AUTONAT_PROTOCOL).contains(token)) {
                            throw new Exception("Token " + token + " not supported");
                        }
                        if (Objects.equals(token, IPFS.AUTONAT_PROTOCOL)) {
                            stream.writeOutput(DataHandler.encode(message)).
                                    thenApply(Stream::closeOutput);
                        }
                    }

                    @Override
                    public void fin() {
                        // nothing to do here
                    }

                    @Override
                    public void data(Stream stream, ByteBuffer data) throws Exception {
                        response.complete(Autonat.Message.parseFrom(data.array()));
                    }

                    @NonNull
                    @Override
                    public Transport getTransport(QuicStream quicStream) {
                        return new LiteTransport();
                    }

                    @Override
                    public void streamTerminated(QuicStream quicStream) {
                        LogUtils.error(TAG, "todo stream terminated");
                    }

                })
                .thenApply(quicStream -> quicStream.writeOutput(
                        DataHandler.encodeTokens(IPFS.STREAM_PROTOCOL, IPFS.AUTONAT_PROTOCOL)));

        return response;

    }
}
