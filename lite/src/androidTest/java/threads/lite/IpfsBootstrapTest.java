package threads.lite;


import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.core.Connection;
import threads.lite.host.Session;
import threads.lite.relay.Reservation;


@RunWith(AndroidJUnit4.class)
public class IpfsBootstrapTest {
    private static final String TAG = IpfsBootstrapTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    //@Test
    public void test_idle() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        Session session = ipfs.createSession();

        try {

            for (Multiaddr ma : ipfs.getIdentity().getAddresses()) {
                LogUtils.error(TAG, ma.toString());
            }

            Cid cid = ipfs.storeText("moin moin moin moin");
            LogUtils.debug(TAG, "ipfs get /ipfs/" + cid.String());


            Multiaddr peer = Multiaddr.create("/ip4/192.168.43.171/udp/4001/quic");
            Connection conn = ipfs.dial(session, peer, IPFS.CONNECT_TIMEOUT,
                    IPFS.GRACE_PERIOD, IPFS.MAX_STREAMS, IPFS.MESSAGE_SIZE_MAX);
            assertNotNull(conn);
            assertNotNull(conn.getRemoteAddress());


            Thread.sleep(TimeUnit.MINUTES.toMillis(15));
        } finally {
            session.clear(true);
        }
    }

    @Test
    public void test_bootstrap_hops() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        Session session = ipfs.createSession();

        try {
            int timeInMinutes = 1; // make higher for long run

            assertTrue(ipfs.hasReservations());

            int numReservation = ipfs.numReservations();

            for (Multiaddr ma : ipfs.getIdentity().getAddresses()) {
                LogUtils.debug(TAG, ma.toString());
            }

            // test 1 minutes
            for (int i = 0; i < timeInMinutes; i++) {
                Thread.sleep(TimeUnit.MINUTES.toMillis(1));

                Set<Reservation> reservations = ipfs.reservations();
                for (Reservation reservation : reservations) {
                    LogUtils.debug(TAG, "Expire in minutes " + reservation.expireInMinutes()
                            + " " + reservation);
                    Connection conn = reservation.getConnection();
                    assertNotNull(conn);
                    assertTrue(conn.isConnected());
                    assertNotNull(conn.getRemoteAddress());
                    assertNotNull(ipfs.remoteAddress(conn));
                }

                assertEquals(numReservation, ipfs.numReservations());
            }
        } finally {
            session.clear(true);
        }
    }

}
