package threads.lite;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

import threads.lite.core.Connection;
import threads.lite.host.PeerInfo;
import threads.lite.host.Session;
import threads.lite.ident.IdentityService;
import threads.lite.relay.Reservation;

@RunWith(AndroidJUnit4.class)
public class IpfsHolepunchTest {

    private static final String TAG = IpfsBootstrapTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void test_incoming() throws Throwable {

        IPFS ipfs = TestEnv.getTestInstance(context);
        Session session = ipfs.createSession();

        DUMMY dummy = DUMMY.getInstance();
        Session dummySession = dummy.createSession();

        try {
            assertTrue(ipfs.hasReservations());

            Set<Reservation> reservations = ipfs.reservations();
            assertFalse(reservations.isEmpty());
            Reservation reservation = reservations.stream().
                    findAny().orElseThrow((Supplier<Throwable>) () ->
                            new RuntimeException("at least one present"));

            LogUtils.error(TAG, reservation.toString());
            LogUtils.error(TAG, reservation.getCircuitAddress().toString());

            Connection conn = dummy.getHost().dial(dummySession,
                    reservation.getCircuitAddress(),
                    60, IPFS.GRACE_PERIOD, IPFS.MAX_STREAMS,
                    IPFS.MESSAGE_SIZE_MAX);

            assertNotNull(conn);

            PeerInfo info = IdentityService.getPeerInfo(conn)
                    .get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);

            assertNotNull(info);
            LogUtils.error(TAG, info.toString());

            conn.close();

        } finally {
            dummy.getHost().getServer().shutdown();
            dummySession.clear(true);
            session.clear(true);
        }

    }
}