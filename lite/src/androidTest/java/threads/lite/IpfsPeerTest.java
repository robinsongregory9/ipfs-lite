package threads.lite;


import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import junit.framework.TestCase;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.core.Connection;
import threads.lite.host.Session;
import threads.lite.relay.Reservation;

@RunWith(AndroidJUnit4.class)
public class IpfsPeerTest {
    private static final String TAG = IpfsPeerTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void test_connectAndFindPeer() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        Session session = ipfs.createSession();
        try {
            Multiaddr multiaddr = Multiaddr.create("/ip4/139.178.68.145/udp/4001/quic");
            assertTrue(multiaddr.isIP4());
            PeerId peerId = ipfs.getPeerId("12D3KooWSzPeHsfxULJwFiLeq6Qsx6TruezAwjZ619qsLhqC7cUR");
            Connection conn = ipfs.dial(session, multiaddr, IPFS.CONNECT_TIMEOUT,
                    IPFS.GRACE_PERIOD, 0, IPFS.MESSAGE_SIZE_MAX);
            assertNotNull(conn);
            TestCase.assertTrue(conn.isConnected());

            try {
                ipfs.notify(conn, "hello").get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
                fail();
            } catch (Throwable throwable) {
                LogUtils.info(TAG, throwable.getMessage());
            }

            AtomicBoolean found = new AtomicBoolean(false);
            ipfs.findPeer(session, peerId, multiaddr1 -> {
                LogUtils.error(TAG, multiaddr1.toString());
                found.set(true);
            }, found::get).get(30, TimeUnit.SECONDS);
            assertTrue(found.get());
            conn.close();

        } finally {
            session.clear(true);
        }
    }

    @Test
    public void test_findPeer() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        Session session = ipfs.createSession();

        try {
            Set<Reservation> reservations = ipfs.reservations();
            assertFalse(reservations.isEmpty());
            for (Reservation reservation : reservations) {
                PeerId relayId = reservation.getRelayId();
                LogUtils.debug(TAG, relayId.toString());
                Connection conn = reservation.getConnection();
                assertNotNull(conn);
                assertTrue(conn.isConnected());
                AtomicBoolean found = new AtomicBoolean(false);
                ipfs.findPeer(session, relayId, multiaddr -> {
                    LogUtils.debug(TAG, multiaddr.toString());
                    found.set(true);
                }, found::get).get(30, TimeUnit.SECONDS);
                assertTrue(found.get());
            }
        } finally {
            session.clear(true);
        }
    }
}