package threads.lite;


import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertNull;
import static junit.framework.TestCase.assertTrue;
import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertEquals;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.net.ConnectException;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import threads.lite.cid.Multiaddr;
import threads.lite.core.Connection;
import threads.lite.host.PeerInfo;
import threads.lite.host.Session;
import threads.lite.relay.Reservation;


@RunWith(AndroidJUnit4.class)
public class IpfsConnectTest {
    private static final String TAG = IpfsConnectTest.class.getSimpleName();

    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test(expected = ConnectException.class)
    public void swarm_connect() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);
        Session session = ipfs.createSession();
        try {
            String address = "/ip4/139.178.68.146/udp/4001/quic";


            Multiaddr multiaddr = Multiaddr.create(address);
            assertNotNull(multiaddr.getInetSocketAddress());
            assertEquals(multiaddr.getInetSocketAddress().getPort(), multiaddr.getPort());
            assertEquals(multiaddr.getInetSocketAddress().getHostName(), multiaddr.getHost());

            assertEquals(Multiaddr.stringConverter(address), multiaddr);
            assertEquals(Multiaddr.multiaddressConverter(multiaddr), address);


            // multiaddress is just a fiction
            Connection conn = ipfs.dial(session, multiaddr,
                    IPFS.CONNECT_TIMEOUT, IPFS.GRACE_PERIOD,
                    IPFS.MAX_STREAMS, IPFS.MESSAGE_SIZE_MAX);
            assertNull(conn); // does not come to this point

            fail(); // exception is thrown
        } finally {
            session.clear(true);
        }


    }


    @Test
    public void test_print_swarm_peers() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);
        Session session = ipfs.createSession();
        try {
            Set<Reservation> reservations = ipfs.reservations();
            for (Reservation reservation : reservations) {

                Connection conn = reservation.getConnection();
                assertTrue(conn.isConnected());
                assertFalse(session.swarmContains(conn));
                assertTrue(session.swarmEnhance(conn));
            }

            List<Connection> quicConnections = session.getSwarm();

            assertNotNull(quicConnections);
            LogUtils.debug(TAG, "Peers : " + quicConnections.size());
            for (Connection conn : quicConnections) {

                try {
                    assertNotNull(conn.getRemoteAddress());
                    PeerInfo peerInfo = ipfs.getPeerInfo(conn).
                            get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);

                    LogUtils.debug(TAG, peerInfo.toString());
                    assertNotNull(peerInfo.getAddresses());
                    assertNotNull(peerInfo.getAgent());

                    Multiaddr observed = peerInfo.getObserved();
                    if (observed != null) {
                        LogUtils.debug(TAG, observed.toString());
                    }

                } catch (Throwable throwable) {
                    LogUtils.debug(TAG, "" + throwable.getClass().getName());
                } finally {
                    session.swarmReduce(conn);
                }

            }
        } finally {
            session.clear(true);
        }
    }

}
