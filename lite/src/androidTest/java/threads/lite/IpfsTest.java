package threads.lite;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.Objects;
import java.util.Set;

import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.format.Link;
import threads.lite.host.PeerInfo;
import threads.lite.host.Session;
import threads.lite.utils.TimeoutCancellable;


@RunWith(AndroidJUnit4.class)
public class IpfsTest {
    private static final String TAG = IpfsTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void test_listenAddresses() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);

        List<Multiaddr> result = ipfs.getIdentity().getAddresses();
        assertNotNull(result);
        for (Multiaddr ma : result) {
            LogUtils.debug(TAG, "Listen Address : " + ma.toString());
        }
        assertTrue(result.size() >= 1);

        PeerInfo info = ipfs.getIdentity();
        Objects.requireNonNull(info);

        assertNotNull(info.getVersion());
        assertFalse(info.hasRelayHop());
        assertEquals(info.getAgent(), IPFS.AGENT);

        List<String> protocols = info.getProtocols();
        for (String protocol : protocols) {
            assertNotNull(protocol);
        }

        LogUtils.debug(TAG, info.toString());

        List<Multiaddr> multiaddrs = ipfs.getIdentity().getAddresses();
        assertNotNull(multiaddrs);
        assertFalse(multiaddrs.isEmpty());

    }


    @Test
    public void test_dns_addr() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);
        Set<Multiaddr> bootstrap = ipfs.getBootstrap();
        assertNotNull(bootstrap);

        assertTrue(bootstrap.size() >= 7);
        for (Multiaddr address : bootstrap) {
            LogUtils.error(TAG, address.toString());
        }
    }

    @Test
    public void streamTest() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);
        Session session = ipfs.createSession();
        try {
            String test = "Moin";
            Cid cid = ipfs.storeText(test);
            assertNotNull(cid);
            byte[] bytes = ipfs.getData(session, cid, () -> false);
            assertNotNull(bytes);
            assertEquals(test, new String(bytes));


            try {
                Cid fault = Cid.decode(ipfs.self().toBase58());
                ipfs.getData(session, fault, new TimeoutCancellable(10));
                fail();
            } catch (Exception ignore) {
                // ok
            }
        } finally {
            session.clear(true);
        }

    }

    @Test
    public void test_timeout_cat() throws Exception {

        Cid notValid = Cid.decode("QmaFuc7VmzwT5MAx3EANZiVXRtuWtTwALjgaPcSsZ2Jdip");
        IPFS ipfs = TestEnv.getTestInstance(context);
        Session session = ipfs.createSession();
        try {
            try {
                ipfs.getData(session, notValid, new TimeoutCancellable(10));
                fail();
            } catch (Exception ignore) {
                // ok
            }
        } finally {
            session.clear(true);
        }
    }


    private byte[] getRandomBytes() {
        return RandomStringUtils.randomAlphabetic(400000).getBytes();
    }

    @Test
    public void test_add_cat() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);
        Session session = ipfs.createSession();
        try {
            byte[] content = getRandomBytes();

            Cid hash58Base = ipfs.storeData(content);
            assertNotNull(hash58Base);
            LogUtils.debug(TAG, hash58Base.String());

            byte[] fileContents = ipfs.getData(session, hash58Base, () -> false);
            assertNotNull(fileContents);
            assertEquals(content.length, fileContents.length);
            assertEquals(new String(content), new String(fileContents));

            ipfs.rm(hash58Base);
        } finally {
            session.clear(true);
        }
    }


    @Test(expected = Exception.class)
    public void test_ls_timeout() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);
        Session session = ipfs.createSession();
        try {
            ipfs.links(session, Cid.decode("QmXm3f7uKuFKK3QUL1V1oJZnpJSYX8c3vdhd94evSQUPCH"),
                    true, new TimeoutCancellable(20));

        } finally {
            session.clear(true);
        }
    }

    @Test
    public void test_ls_small() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);
        Session session = ipfs.createSession();
        try {
            Cid cid = ipfs.storeText("hallo");
            assertNotNull(cid);
            List<Link> links = ipfs.links(session, cid, true, () -> false);
            assertNotNull(links);
            assertEquals(links.size(), 0);
            links = ipfs.links(session, cid, true, new TimeoutCancellable(20));
            assertNotNull(links);
            assertEquals(links.size(), 0);
        } finally {
            session.clear(true);
        }
    }
}
