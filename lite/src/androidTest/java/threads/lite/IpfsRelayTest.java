package threads.lite;


import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Supplier;

import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.core.Connection;
import threads.lite.host.PeerInfo;
import threads.lite.host.Session;
import threads.lite.relay.Reservation;


@RunWith(AndroidJUnit4.class)
public class IpfsRelayTest {
    private static final String TAG = IpfsRelayTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test
    public void test_relay_reserve_and_connect() throws Throwable {
        IPFS ipfs = TestEnv.getTestInstance(context);
        Session session = ipfs.createSession();

        DUMMY dummy = DUMMY.getInstance();
        Session dummySession = dummy.createSession();
        try {
            assertTrue(ipfs.hasReservations());

            Set<Reservation> reservations = ipfs.reservations();
            assertFalse(reservations.isEmpty());
            Reservation reservation = reservations.stream().
                    findAny().orElseThrow((Supplier<Throwable>) () ->
                            new RuntimeException("at least one present"));


            PeerId relayID = reservation.getRelayId();
            assertNotNull(relayID);
            Multiaddr relay = reservation.getRelayAddress();
            assertNotNull(relay);

            assertNotNull(reservation.getReservation());
            assertNotNull(reservation.getLimit());

            Connection conn = dummy.getHost().dial(dummySession,
                    reservation.getCircuitAddress(),
                    60, IPFS.GRACE_PERIOD, IPFS.MAX_STREAMS,
                    IPFS.MESSAGE_SIZE_MAX);
            Objects.requireNonNull(conn);

            // TEST 1
            PeerInfo peerInfo = ipfs.getPeerInfo(conn).get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
            assertNotNull(peerInfo);
            Multiaddr observed = peerInfo.getObserved();
            assertNotNull(observed);
            assertEquals(peerInfo.getAgent(), IPFS.AGENT);
            LogUtils.debug(TAG, peerInfo.toString());


            // TEST 2
            AtomicBoolean success = new AtomicBoolean(false);
            String data = "moin";
            ipfs.setIncomingPush((ctx) -> success.set(ctx.getConnection() != null &&
                    ctx.getData().equals(data)));
            ipfs.notify(conn, data).get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
            Thread.sleep(1000);
            Assert.assertTrue(success.get());

            // TEST 3
            peerInfo = ipfs.getPeerInfo(conn).get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
            assertNotNull(peerInfo);

            // TEST 4
            success.set(false);
            String test = "zehn";
            ipfs.setIncomingPush((ctx) -> success.set(ctx.getData().equals(test)));
            ipfs.notify(conn, test).get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
            Thread.sleep(1000);
            Assert.assertTrue(success.get());

            // will close the relay connection from the dummy
            conn.close();

            assertTrue(ipfs.hasReservations());

        } finally {
            dummy.getHost().getServer().shutdown();
            dummySession.clear(true);
            dummy.clearDatabase();
            session.clear(true);
        }

    }

}
