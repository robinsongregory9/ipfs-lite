package threads.lite;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.protobuf.ByteString;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

import threads.lite.cid.Cid;
import threads.lite.cid.PeerId;
import threads.lite.core.Cancellable;
import threads.lite.core.Connection;
import threads.lite.core.Progress;
import threads.lite.format.Block;
import threads.lite.format.BlockStore;
import threads.lite.host.LiteHost;
import threads.lite.host.LiteHostCertificate;
import threads.lite.host.PeerInfo;
import threads.lite.host.Session;
import threads.lite.ident.IdentityService;
import threads.lite.utils.Reader;
import threads.lite.utils.ReaderStream;

public class DUMMY {

    @NonNull
    private final LiteHost host;
    @NonNull
    private final BlockStore blockstore;

    private DUMMY() throws Exception {
        KeyPair keypair = getKeyPair();


        this.blockstore = new BlockStore() {
            private final ConcurrentHashMap<Cid, Block> blocks = new ConcurrentHashMap<>();

            @Override
            public boolean hasBlock(@NonNull Cid cid) {
                return blocks.containsKey(cid);
            }

            @Override
            public Block getBlock(@NonNull Cid cid) {
                return blocks.get(cid);
            }

            @Override
            public byte[] getData(@NonNull Cid cid) {
                try {
                    return Objects.requireNonNull(getBlock(cid)).getNode().toByteArray();
                } catch (Throwable throwable) {
                    return null;
                }
            }

            @Override
            public void deleteBlock(@NonNull Cid cid) {
                blocks.remove(cid);
            }

            @Override
            public void deleteBlocks(@NonNull List<Cid> cids) {
                for (Cid cid : cids) {
                    deleteBlock(cid);
                }
            }

            @Override
            public void putBlock(@NonNull Block block) {
                blocks.put(block.getCid(), block);
            }

            @Override
            public void clear() {
                blocks.clear();
            }
        };

        this.host = new LiteHost(keypair, blockstore, LiteHost.nextFreePort());

    }


    @NonNull
    public static DUMMY getInstance() throws Exception {
        synchronized (DUMMY.class) {
            return new DUMMY();
        }
    }

    private KeyPair getKeyPair() throws NoSuchAlgorithmException {

        String algorithm = "RSA";
        final KeyPair keypair;

        KeyPairGenerator keyGen = KeyPairGenerator.getInstance(algorithm);
        keyGen.initialize(2048, LiteHostCertificate.ThreadLocalInsecureRandom.current());
        keypair = keyGen.generateKeyPair();

        return keypair;
    }


    @Nullable
    public String getText(@NonNull Session session, @NonNull Cid cid, @NonNull Cancellable cancellable)
            throws Exception {

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            getToOutputStream(session, outputStream, cid, cancellable);
            return outputStream.toString();
        }
    }

    public void storeToOutputStream(@NonNull Session session, @NonNull OutputStream outputStream,
                                    @NonNull Cid cid, @NonNull Progress progress)
            throws Exception {

        long totalRead = 0L;
        int remember = 0;

        Reader reader = getReader(session, cid, progress);
        long size = reader.getSize();

        do {
            if (progress.isCancelled()) {
                throw new IOException();
            }

            ByteString buffer = reader.loadNextData();
            if (buffer == null || buffer.size() <= 0) {
                return;
            }
            outputStream.write(buffer.toByteArray());

            // calculate progress
            totalRead += buffer.size();
            if (progress.doProgress()) {
                if (size > 0) {
                    int percent = (int) ((totalRead * 100.0f) / size);
                    if (remember < percent) {
                        remember = percent;
                        progress.setProgress(percent);
                    }
                }
            }
        } while (true);
    }

    public void storeToOutputStream(@NonNull Session session,
                                    @NonNull OutputStream outputStream,
                                    @NonNull Cid cid,
                                    @NonNull Cancellable cancellable)
            throws Exception {

        Reader reader = getReader(session, cid, cancellable);

        do {
            ByteString buffer = reader.loadNextData();
            if (buffer == null || buffer.size() <= 0) {
                return;
            }
            outputStream.write(buffer.toByteArray());
        } while (true);
    }

    @NonNull
    public byte[] getData(@NonNull Session session,
                          @NonNull Cid cid,
                          @NonNull Progress progress) throws Exception {

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            storeToOutputStream(session, outputStream, cid, progress);
            return outputStream.toByteArray();
        }
    }

    @NonNull
    public byte[] getData(@NonNull Session session, @NonNull Cid cid, @NonNull Cancellable cancellable)
            throws Exception {

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            storeToOutputStream(session, outputStream, cid, cancellable);
            return outputStream.toByteArray();
        }
    }

    @NonNull
    public PeerId getPeerID() {
        return host.self();
    }

    @NonNull
    public CompletableFuture<PeerInfo> getPeerInfo(@NonNull Connection conn) {
        return IdentityService.getPeerInfo(conn);
    }


    public void clearDatabase() {
        blockstore.clear();
    }

    @NonNull
    public Reader getReader(@NonNull Session session, @NonNull Cid cid,
                            @NonNull Cancellable cancellable) throws Exception {
        return Reader.getReader(cancellable, blockstore, session, cid);
    }

    private void getToOutputStream(@NonNull Session session,
                                   @NonNull OutputStream outputStream,
                                   @NonNull Cid cid,
                                   @NonNull Cancellable cancellable)
            throws Exception {
        try (InputStream inputStream = getInputStream(session, cid, cancellable)) {
            IPFS.copy(inputStream, outputStream);
        }
    }

    @NonNull
    public InputStream getInputStream(@NonNull Session session, @NonNull Cid cid, @NonNull Cancellable cancellable)
            throws Exception {
        Reader reader = getReader(session, cid, cancellable);
        return new ReaderStream(reader);
    }

    @NonNull
    public LiteHost getHost() {
        return host;
    }

    @NonNull
    public Session createSession() {
        return host.createSession(dataSupplier -> {
        }, false, false);
    }
}
