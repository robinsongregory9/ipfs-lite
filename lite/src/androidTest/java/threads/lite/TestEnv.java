package threads.lite;

import android.content.Context;

import androidx.annotation.NonNull;

import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.lite.cid.Multiaddr;
import threads.lite.relay.Reservation;

class TestEnv {

    private static final String TAG = TestEnv.class.getSimpleName();
    private static final AtomicBoolean started = new AtomicBoolean(false);

    public static IPFS getTestInstance(@NonNull Context context) throws Exception {

        IPFS.setPort(context, 5001);

        IPFS ipfs = IPFS.getInstance(context);
        ipfs.clearDatabase();
        ipfs.updateNetwork();
        ipfs.setConnectConsumer(connection -> LogUtils.error(TAG, "Incoming connection : "
                + connection.getRemoteAddress()));
        ipfs.setIsGated(peerId -> {
            LogUtils.error(TAG, "Peer Gated : " + peerId.toBase58());
            return false;
        });

        if (!started.getAndSet(true)) {
            boolean success = ipfs.autonat(30);
            LogUtils.error(TAG, "Success Autonat : " + success);


            Set<Reservation> reservations = ipfs.reservations(30);
            for (Reservation reservation : reservations) {
                LogUtils.error(TAG, reservation.toString());
            }

            for (Multiaddr ma : ipfs.getIdentity().getAddresses()) {
                LogUtils.error(TAG, ma.toString());
            }
        }

        System.gc();
        return ipfs;
    }


}
