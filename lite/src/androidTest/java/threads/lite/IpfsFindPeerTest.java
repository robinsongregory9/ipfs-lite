package threads.lite;


import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.core.Connection;
import threads.lite.host.PeerInfo;
import threads.lite.host.Session;
import threads.lite.ipns.IpnsService;
import threads.lite.utils.TimeoutCancellable;


@RunWith(AndroidJUnit4.class)
public class IpfsFindPeerTest {
    private static final String TAG = IpfsFindPeerTest.class.getSimpleName();


    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    //@Test
    public void direct_dial_pc() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        Thread.sleep(500000);

        Session session = ipfs.createSession();
        try {
            Connection conn = ipfs.dial(session, Multiaddr.create(
                            "/ip4/3.16.42.100/udp/4001/quic/p2p/12D3KooWQ4MJwP85MGpFd34qYqEhkdQPF5GKiRGJPqwYxRSBt19g/p2p-circuit/p2p/12D3KooWSR9wEwGHjd6Rj3CViDPvHX5FGkXop4y6wNVzRvbZSXoK"), 60,
                    IPFS.GRACE_PERIOD, IPFS.MAX_STREAMS, IPFS.MESSAGE_SIZE_MAX);
            assertNotNull(conn);

            PeerInfo info = ipfs.getPeerInfo(conn).get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
            assertNotNull(info);
            LogUtils.error(TAG, info.toString());
        } finally {
            session.clear(true);
        }
    }

    //@Test
    public void find_pc_data() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);
        Session session = ipfs.createSession();
        try {

            Cid cid = Cid.decode("QmdjoDyD7JC8K3JnYqtEE31BoZC7yxa5pzJbWZfFB41gcF");

            byte[] data = ipfs.getData(session, cid, new TimeoutCancellable(60));
            assertNotNull(data);

        } finally {
            session.clear(true);
        }
    }


    //@Test
    public void find_pc() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);
        Session session = ipfs.createSession();
        try {
            PeerId pc = PeerId.fromBase58("12D3KooWSR9wEwGHjd6Rj3CViDPvHX5FGkXop4y6wNVzRvbZSXoK");
            AtomicBoolean connect = new AtomicBoolean(false);
            ipfs.findPeer(session, pc,
                    multiaddr -> {
                        LogUtils.error(TAG, multiaddr.toString());

                        try {
                            Connection conn = ipfs.dial(session, multiaddr, IPFS.CONNECT_TIMEOUT,
                                    IPFS.GRACE_PERIOD, IPFS.MAX_STREAMS, IPFS.MESSAGE_SIZE_MAX);
                            assertNotNull(conn);
                            connect.set(true);
                        } catch (Throwable throwable) {
                            LogUtils.error(TAG, throwable);
                        }


                    }, connect::get).get(60, TimeUnit.SECONDS);

            assertTrue(connect.get());

        } finally {
            session.clear(true);
        }
    }

    @Test
    public void find_peer_corbett() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);

        //CorbettReport ipns://k2k4r8jllj4k33jxoa4vaeleqkrwu8b7tqz7tgczhptbfkhqr2i280fm
        Session session = ipfs.createSession();
        try {
            String key = "k2k4r8jllj4k33jxoa4vaeleqkrwu8b7tqz7tgczhptbfkhqr2i280fm";
            IpnsService.Entry res = ipfs.resolveName(session, PeerId.decodeName(key),
                    0, new TimeoutCancellable(30));
            assertNotNull(res);
            LogUtils.debug(TAG, res.getPeerId().toBase58());
            LogUtils.debug(TAG, res.getKeyType().toString());
            LogUtils.debug(TAG, res.getValue());
            LogUtils.debug(TAG, res.toString());
            assertTrue(res.getHash().isSupported());

        } finally {
            session.clear(true);
        }
    }

    @Test
    public void find_peer_freedom() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);

        //FreedomsPhoenix.com ipns://k2k4r8magsykrprepvtuvd1h8wonxy7rbdkxd09aalsvclqh7wpb28m1
        Session session = ipfs.createSession();
        try {
            String key = "k2k4r8magsykrprepvtuvd1h8wonxy7rbdkxd09aalsvclqh7wpb28m1";
            IpnsService.Entry res = ipfs.resolveName(session, PeerId.decodeName(key),
                    0, new TimeoutCancellable(30));
            assertNotNull(res);
            LogUtils.debug(TAG, res.getPeerId().toBase58());
            LogUtils.debug(TAG, res.getKeyType().toString());
            LogUtils.debug(TAG, res.getValue());
            LogUtils.debug(TAG, res.toString());
            assertTrue(res.getHash().isSupported());

        } finally {
            session.clear(true);
        }
    }

    @Test
    public void find_peer_pirates() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);

        //PiratesWithoutBorders.com ipns://k2k4r8l8zgv45qm2sjt7p16l7pvy69l4jr1o50cld4s98wbnanl0zn6t
        Session session = ipfs.createSession();
        try {
            String key = "k2k4r8l8zgv45qm2sjt7p16l7pvy69l4jr1o50cld4s98wbnanl0zn6t";
            IpnsService.Entry res = ipfs.resolveName(session, PeerId.decodeName(key),
                    0, new TimeoutCancellable(30));
            assertNotNull(res);
            LogUtils.debug(TAG, res.getPeerId().toBase58());
            LogUtils.debug(TAG, res.getKeyType().toString());
            LogUtils.debug(TAG, res.getValue());
            LogUtils.debug(TAG, res.toString());
            assertTrue(res.getHash().isSupported());

        } finally {
            session.clear(true);
        }
    }
}
