package threads.lite;


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import threads.lite.cid.Multiaddr;

@RunWith(AndroidJUnit4.class)
public class IpfsDnsAddressTest {

    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void test_dnsAddress() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);

        Multiaddr dnsaddr = Multiaddr.create(
                "/dnsaddr/bootstrap.libp2p.io/p2p/QmNnooDu7bfjPFoTZYxMNLWUQJyrVwtbZg5gBMjTezGAJN");
        List<Multiaddr> addresses = ipfs.resolveDnsaddr(dnsaddr);
        assertNotNull(addresses);
        assertFalse(addresses.isEmpty());

    }
}
