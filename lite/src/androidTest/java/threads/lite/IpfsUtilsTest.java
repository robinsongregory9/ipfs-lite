package threads.lite;


import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import android.content.Context;
import android.util.Pair;

import androidx.annotation.NonNull;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;

import threads.lite.cid.Cid;
import threads.lite.cid.ID;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Multihash;
import threads.lite.cid.Peer;
import threads.lite.cid.PeerId;
import threads.lite.cid.Prefix;
import threads.lite.dht.QueryPeer;
import threads.lite.dht.Util;
import threads.lite.mplex.Mplex;
import threads.lite.mplex.MuxFlag;
import threads.lite.mplex.MuxId;
import threads.lite.mplex.MuxedFrame;
import threads.lite.noise.Noise;


@RunWith(AndroidJUnit4.class)
public class IpfsUtilsTest {
    private static final String TAG = IpfsUtilsTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test
    public void muxHeaderTest() {
        MuxId muxId = new MuxId(0, true);
        int header = Mplex.encodeHeader(new MuxedFrame(muxId, MuxFlag.OPEN, 0));
        assertEquals(header, 0);

        Pair<MuxId, MuxFlag> pair = Mplex.decodeHeader(header);
        assertNotNull(pair);
        MuxId cmp = pair.first;
        assertEquals(muxId.getStreamId(), cmp.getStreamId());
        assertTrue(muxId.isInitiator());
        assertFalse(cmp.isInitiator());
        assertEquals(pair.second, MuxFlag.OPEN);
    }


    @Test
    public void bigEndianTest() {
        int test = 30;
        byte[] bigEndian = Noise.shortToBigEndianBytes((short) test);
        int length = Noise.bytesToBigEndianShort(bigEndian);
        assertEquals(length, test);
    }

    @Test
    public void cidV1() throws Exception {

        Cid cid = Cid.nsToCid("moin welt");
        assertNotNull(cid);
        Prefix prefix = cid.getPrefix();
        assertNotNull(prefix);
        assertEquals(prefix.getCodec(), Cid.Raw);
        assertEquals(prefix.getType(), Multihash.Type.sha2_256);
        assertEquals(prefix.getVersion(), 1);
        assertNotNull(cid.getHash());

        Cid cmp = Cid.fromArray(cid.bytes());
        assertEquals(cmp, cid);
        assertEquals(cmp.getPrefix(), cid.getPrefix());
        assertArrayEquals(cmp.getHash(), cid.getHash());


    }

    @Test
    public void decode_name() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        // test of https://docs.ipfs.io/how-to/address-ipfs-on-web/#http-gateways
        PeerId test = PeerId.fromBase58("QmNnooDu7bfjPFoTZYxMNLWUQJyrVwtbZg5gBMjTezGAJN");
        assertEquals("k2k4r8jl0yz8qjgqbmc2cdu5hkqek5rj6flgnlkyywynci20j0iuyfuj", test.toBase36());

        Cid cid = Cid.decode("QmbWqxBEKC3P8tqsKc98xmWNzrzDtRLMiMPL8wBuTGsMnR");

        assertEquals(cid.String(),
                "QmbWqxBEKC3P8tqsKc98xmWNzrzDtRLMiMPL8wBuTGsMnR");
        assertEquals(Cid.newCidV1(cid.getCodec(), cid.bytes()).String(),
                "bafybeigdyrzt5sfp7udm7hu76uh7y26nf3efuylqabf3oclgtqy55fbzdi");

        assertEquals(ipfs.self(), ipfs.decodeName(ipfs.self().toBase36()));
    }

    @Test
    public void network() throws Exception {

        int port = 4001;

        IPFS ipfs = TestEnv.getTestInstance(context);
        PeerId peerId = ipfs.self();

        Multiaddr ma = Multiaddr.getSiteLocalAddress(peerId, port);
        assertNotNull(ma);
        LogUtils.error(TAG, ma.toString());

        ma = Multiaddr.getLocalHost(port);
        assertNotNull(ma);
        LogUtils.error(TAG, ma.toString());


    }

    @Test
    public void cat_utils() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        PeerId peerId = ipfs.self();

        Peer peer = Peer.create(peerId, Collections.emptyList());

        ID a = ID.convertPeerID(peerId);
        ID b = ID.convertPeerID(peerId);


        BigInteger dist = Util.distance(a, b);
        assertEquals(dist.longValue(), 0L);

        int res = Util.commonPrefixLen(a, b);
        assertEquals(res, (a.data.length * 8));

        int cmp = a.compareTo(b);
        assertEquals(0, cmp);


        PeerId randrom = PeerId.random();
        Peer randomPeer = Peer.create(peerId, Collections.emptyList());
        ID r1 = ID.convertPeerID(randrom);
        ID r2 = ID.convertPeerID(randrom);

        BigInteger distCmp = Util.distance(a, r1);
        assertNotEquals(distCmp.longValue(), 0L);

        int rres = Util.commonPrefixLen(r1, r2);
        assertEquals(rres, (r1.data.length * 8));

        int rcmp = r1.compareTo(r2);
        assertEquals(0, rcmp);

        PeerDistanceSorterTest pds = new PeerDistanceSorterTest(a);
        pds.appendPeer(peer);
        pds.appendPeer(randomPeer);
        Collections.sort(pds);
        assertEquals(pds.get(0).getPeer(), peer);
        assertEquals(pds.get(1).getPeer(), randomPeer);


        PeerDistanceSorterTest pds2 = new PeerDistanceSorterTest(a);
        pds2.appendPeer(randomPeer);
        pds2.appendPeer(peer);
        Collections.sort(pds2);
        assertEquals(pds2.get(0).getPeer(), peer);
        assertEquals(pds2.get(1).getPeer(), randomPeer);


        PeerDistanceSorterTest pds3 = new PeerDistanceSorterTest(r1);
        pds3.appendPeer(peer);
        pds3.appendPeer(randomPeer);
        Collections.sort(pds3);
        assertEquals(pds3.get(0).getPeer(), randomPeer);
        assertEquals(pds3.get(1).getPeer(), peer);


        Cid cid = Cid.nsToCid("time");
        assertNotNull(cid);
        assertEquals(cid.getPrefix().getVersion(), 1);

    }


    public static class PeerDistanceSorterTest extends ArrayList<QueryPeer> {
        private final ID key;

        public PeerDistanceSorterTest(@NonNull ID key) {
            this.key = key;
        }

        @NonNull
        @Override
        public String toString() {
            return "PeerDistanceSorter{" +
                    "target=" + key +
                    '}';
        }

        public void appendPeer(@NonNull Peer peer) {
            this.add(QueryPeer.create(peer, key));
        }
    }

}
