package threads.lite;


import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.southernstorm.noise.protocol.CipherState;
import com.southernstorm.noise.protocol.CipherStatePair;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import threads.lite.mplex.Mplex;
import threads.lite.mplex.MuxFlag;
import threads.lite.mplex.MuxId;
import threads.lite.mplex.MuxedStream;
import threads.lite.noise.Noise;
import threads.lite.utils.DataHandler;

@RunWith(AndroidJUnit4.class)
public class IpfsNoiseTest {

    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void test_noise() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);
        DUMMY dummy = DUMMY.getInstance();

        Noise.NoiseState initiator = Noise.getInstance().getInitiator(dummy.getPeerID(),
                ipfs.getHost().getKeypair());
        assertNotNull(initiator);


        Noise.NoiseState responder = Noise.getInstance().getResponder(
                dummy.getHost().getKeypair());
        assertNotNull(responder);

        byte[] msg = initiator.getInitalMessage();

        Noise.Response resResponder = responder.handshake(msg);
        assertNotNull(resResponder);

        assertNull(resResponder.getCipherStatePair());
        assertNotNull(resResponder.getMessage());

        Noise.Response resInitiator = initiator.handshake(resResponder.getMessage());
        assertNotNull(resInitiator);

        CipherStatePair initiatorStatePair = resInitiator.getCipherStatePair();

        assertNotNull(initiatorStatePair); // done here initiator
        assertNotNull(resInitiator.getMessage());

        resResponder = responder.handshake(resInitiator.getMessage());
        assertNotNull(resResponder);

        CipherStatePair responderStatePair = resResponder.getCipherStatePair();
        assertNotNull(responderStatePair); // done here responder
        assertNull(resResponder.getMessage());


        CipherState initiatorSender = initiatorStatePair.getSender();
        assertNotNull(initiatorSender);
        CipherState initiatorReceiver = initiatorStatePair.getReceiver();
        assertNotNull(initiatorReceiver);


        CipherState responderReceiver = responderStatePair.getReceiver();
        assertNotNull(responderReceiver);
        CipherState responderSender = responderStatePair.getSender();
        assertNotNull(responderSender);


        // test 1 simple
        byte[] simpleData = "Moin Moin".getBytes();
        byte[] encrypted = Noise.encrypt(initiatorSender, simpleData);
        byte[] decrypted = Noise.decrypt(responderReceiver, encrypted);
        assertArrayEquals(simpleData, decrypted);


        // test 1 simple reverse
        encrypted = Noise.encrypt(responderSender, simpleData);
        decrypted = Noise.decrypt(initiatorReceiver, encrypted);
        assertArrayEquals(simpleData, decrypted);


        // test 2 mux stream
        byte[] muxData = DataHandler.encodeTokens(IPFS.STREAM_PROTOCOL);
        MuxId muxId = Mplex.generateMuxId();
        MuxFlag muxFlag = MuxFlag.OPEN;
        byte[] muxEncodedData = MuxedStream.getMuxEncodedData(muxData,
                initiatorSender, muxId, muxFlag);
        assertNotNull(muxEncodedData);
        byte[] muxDecodedData = MuxedStream.getMuxDecodedData(muxEncodedData, responderReceiver);
        assertArrayEquals(muxData, muxDecodedData);

        // test 2 mux stream reverse
        muxEncodedData = MuxedStream.getMuxEncodedData(muxData, responderSender, muxId, muxFlag);
        assertNotNull(muxEncodedData);
        muxDecodedData = MuxedStream.getMuxDecodedData(muxEncodedData, initiatorReceiver);
        assertArrayEquals(muxData, muxDecodedData);


        // test 3
        assertEquals(Mplex.defaultMuxId().getStreamId(), 0);

    }

}
