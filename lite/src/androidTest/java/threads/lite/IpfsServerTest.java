package threads.lite;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.core.Connection;
import threads.lite.host.PeerInfo;
import threads.lite.host.Session;
import threads.lite.utils.TimeoutCancellable;


@RunWith(AndroidJUnit4.class)
public class IpfsServerTest {

    private static final String TAG = IpfsServerTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test
    public void server_test() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        String text = "Hallo das ist ein Test";
        Cid cid = ipfs.storeText(text);
        assertNotNull(cid);
        Session session = ipfs.createSession(dataSupplier -> {
        }, false, false);

        try {
            DUMMY dummy = DUMMY.getInstance();
            Session dummySession = dummy.createSession();
            try {
                Multiaddr multiaddr = Multiaddr.getLocalHost(ipfs.getPort());

                PeerId host = ipfs.self();
                assertNotNull(host);

                Connection conn = dummy.getHost().dial(
                        dummySession, multiaddr,
                        IPFS.CONNECT_TIMEOUT, IPFS.GRACE_PERIOD,
                        IPFS.MAX_STREAMS, IPFS.MESSAGE_SIZE_MAX);
                Objects.requireNonNull(conn);
                dummySession.swarmEnhance(conn);
                assertTrue(dummySession.swarmContains(conn));


                PeerInfo info = dummy.getPeerInfo(conn).
                        get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
                assertNotNull(info);
                assertEquals(info.getAgent(), IPFS.AGENT);
                assertNotNull(info.getObserved());

                // simple push test
                String data = "moin";
                AtomicBoolean notified = new AtomicBoolean(false);
                ipfs.setIncomingPush((content) -> notified.set(content.getData().equals(data)));
                ipfs.notify(conn, data).get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);


                Thread.sleep(1000);
                assertTrue(notified.get());


                String cmpText = dummy.getText(dummySession, cid, new TimeoutCancellable(10));
                assertEquals(text, cmpText);

                conn.close();
            } finally {
                dummy.getHost().getServer().shutdown();
                dummySession.clear(true);
                dummy.clearDatabase();
            }
        } finally {
            session.clear(true);
        }
    }


    @Test
    public void server_multiple_dummy_conn() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        byte[] input = RandomStringUtils.randomAlphabetic(50000).getBytes();

        Cid cid = ipfs.storeData(input);
        assertNotNull(cid);

        ExecutorService executors = Executors.newFixedThreadPool(
                Runtime.getRuntime().availableProcessors());

        AtomicInteger finished = new AtomicInteger();
        int instances = 100;
        int timeSeconds = 200;
        for (int i = 0; i < instances; i++) {

            executors.execute(() -> {

                try {
                    DUMMY dummy = DUMMY.getInstance();
                    Session dummySession = dummy.createSession();
                    try {

                        PeerId server = ipfs.self();
                        LogUtils.debug(TAG, "Server :" + server.toBase58());
                        PeerId client = dummy.getPeerID();
                        LogUtils.debug(TAG, "Client :" + client.toBase58());

                        Multiaddr multiaddr = Multiaddr.getLocalHost(ipfs.getPort());


                        Connection conn = dummy.getHost().dial(dummySession, multiaddr,
                                IPFS.CONNECT_TIMEOUT, IPFS.GRACE_PERIOD, IPFS.MAX_STREAMS,
                                IPFS.MESSAGE_SIZE_MAX);
                        assertNotNull(conn);
                        dummySession.swarmEnhance(conn);

                        PeerInfo info = dummy.getPeerInfo(conn).
                                get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
                        assertNotNull(info);
                        assertEquals(info.getAgent(), IPFS.AGENT);
                        assertNotNull(info.getObserved());


                        byte[] output = dummy.getData(dummySession, cid,
                                new TimeoutCancellable(timeSeconds));
                        assertArrayEquals(input, output);

                        conn.close();

                        LogUtils.info(TAG, "finished " + finished.incrementAndGet());
                    } finally {
                        dummy.getHost().getServer().shutdown();
                        dummySession.clear(true);
                        dummy.clearDatabase();
                    }
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                    fail();
                }

            });
        }

        executors.shutdown();

        assertTrue(executors.awaitTermination(timeSeconds, TimeUnit.SECONDS));
        assertEquals(finished.get(), instances);


    }


}