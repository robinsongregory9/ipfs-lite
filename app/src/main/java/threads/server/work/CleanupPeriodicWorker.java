package threads.server.work;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.concurrent.TimeUnit;

import threads.lite.LogUtils;
import threads.server.core.DOCS;

public class CleanupPeriodicWorker extends Worker {
    private static final int DELAY = 5;
    private static final String TAG = CleanupPeriodicWorker.class.getSimpleName();

    @SuppressWarnings("WeakerAccess")
    public CleanupPeriodicWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }


    private static PeriodicWorkRequest getWork() {

        return new PeriodicWorkRequest.Builder(CleanupPeriodicWorker.class, DELAY, TimeUnit.HOURS)
                .addTag(TAG)
                .setInitialDelay(DELAY, TimeUnit.MINUTES)
                .build();

    }

    public static void cleanup(@NonNull Context context) {
        WorkManager.getInstance(context).enqueueUniquePeriodicWork(
                TAG, ExistingPeriodicWorkPolicy.KEEP, getWork());
    }


    @NonNull
    @Override
    public Result doWork() {

        long start = System.currentTimeMillis();

        LogUtils.error(TAG, " start ...");

        try {
            DOCS docs = DOCS.getInstance(getApplicationContext());
            docs.cleanup();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.error(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }
        return Result.success();
    }

}

