package threads.server.services;

import android.content.Context;
import android.net.nsd.NsdServiceInfo;

import androidx.annotation.NonNull;

import java.net.Inet6Address;
import java.net.InetAddress;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.cid.Protocol;
import threads.lite.core.Connection;
import threads.lite.host.Session;

public class LocalConnectService {

    private static final String TAG = LocalConnectService.class.getSimpleName();

    public static void connect(@NonNull Session session, @NonNull Context context,
                               @NonNull NsdServiceInfo serviceInfo) {

        AtomicBoolean regularNode = new AtomicBoolean(false);
        try {
            PeerId.decodeName(serviceInfo.getServiceName());
        } catch (Throwable throwable) {
            regularNode.set(true);
        }

        Executors.newSingleThreadExecutor().execute(() -> {
            try {
                IPFS ipfs = IPFS.getInstance(context);
                if (!regularNode.get()) {
                    InetAddress inetAddress = serviceInfo.getHost();

                    String pre = "/ip4";
                    if (inetAddress instanceof Inet6Address) {
                        pre = "/ip6";
                    }

                    String multiAddress = pre + inetAddress + "/udp/" + serviceInfo.getPort() + "/quic";
                    Multiaddr multiaddr = Multiaddr.create(multiAddress);

                    if (!session.swarmHas(multiaddr)) {
                        Connection conn = ipfs.dial(session, multiaddr, IPFS.CONNECT_TIMEOUT,
                                IPFS.GRACE_PERIOD, IPFS.MAX_STREAMS, IPFS.MESSAGE_SIZE_MAX);
                        session.swarmEnhance(conn);

                        LogUtils.error(TAG, "Success adding to swarm " + multiaddr);
                    }
                } else {
                    Map<String, byte[]> maps = serviceInfo.getAttributes();
                    byte[] value = maps.get(Protocol.DNSADDR.getType());
                    Multiaddr multiaddr = Multiaddr.create(new String(value));

                    PeerId peer = PeerId.decodeName(Objects.requireNonNull(
                            multiaddr.getStringComponent(Protocol.P2P).get(0)));
                    AtomicBoolean connected = new AtomicBoolean(false);
                    ipfs.findPeer(session, peer, multiaddr1 -> {
                        try {
                            if (session.swarmHas(multiaddr1)) {
                                connected.set(true);
                            } else {
                                if (!multiaddr1.isCircuitAddress()) {
                                    try {
                                        Connection conn = ipfs.dial(session, multiaddr1,
                                                IPFS.CONNECT_TIMEOUT, IPFS.GRACE_PERIOD,
                                                IPFS.MAX_STREAMS, IPFS.MESSAGE_SIZE_MAX);
                                        session.swarmEnhance(conn);
                                        connected.set(true);
                                        LogUtils.error(TAG, "Success adding to swarm " +
                                                conn.getRemoteAddress());

                                    } catch (Throwable ignore) {
                                    }
                                }
                            }
                        } catch (Throwable throwable) {
                            LogUtils.error(TAG, throwable);
                        }
                    }, connected::get).get(30, TimeUnit.SECONDS);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });
    }

}

