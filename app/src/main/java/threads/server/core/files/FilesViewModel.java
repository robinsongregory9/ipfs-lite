package threads.server.core.files;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class FilesViewModel extends AndroidViewModel {

    private final FilesDatabase filesDatabase;

    public FilesViewModel(@NonNull Application application) {
        super(application);
        filesDatabase = FILES.getInstance(
                application.getApplicationContext()).getFilesDatabase();
    }


    public LiveData<List<Proxy>> getLiveDataFiles(long parent) {
        return filesDatabase.proxyDao().getLiveDataFiles(parent);
    }

}