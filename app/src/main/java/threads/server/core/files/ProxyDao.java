package threads.server.core.files;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.TypeConverters;

import java.util.List;

import threads.lite.cid.Cid;

@Dao
public interface ProxyDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertProxy(Proxy proxy);

    @Query("SELECT cid FROM Proxy WHERE idx = :idx")
    @TypeConverters(Cid.class)
    Cid getContent(long idx);

    @Query("UPDATE Proxy SET leaching = 1 WHERE idx = :idx")
    void setLeaching(long idx);

    @Query("UPDATE Proxy SET leaching = 0 WHERE idx = :idx")
    void resetLeaching(long idx);

    @Query("UPDATE Proxy SET deleting = 1 WHERE idx = :idx")
    void setDeleting(long idx);

    @Query("UPDATE Proxy SET deleting = 0 WHERE idx = :idx")
    void resetDeleting(long idx);

    @Query("SELECT * FROM Proxy WHERE name = :name AND parent = :parent AND deleting = 0")
    List<Proxy> getProxiesByNameAndParent(String name, long parent);

    @Query("SELECT COUNT(idx) FROM Proxy WHERE cid =:cid")
    @TypeConverters(Cid.class)
    int references(Cid cid);

    @Query("SELECT SUM(size) FROM Proxy WHERE parent =:parent AND deleting = 0")
    long getChildrenSummarySize(long parent);

    @Query("SELECT * FROM Proxy WHERE idx =:idx")
    Proxy getProxyByIdx(long idx);

    @Query("SELECT * FROM Proxy WHERE parent =:parent AND deleting = 0")
    LiveData<List<Proxy>> getLiveDataFiles(long parent);

    @Query("UPDATE Proxy SET cid =:cid  WHERE idx = :idx")
    @TypeConverters(Cid.class)
    void setContent(long idx, Cid cid);

    @Query("UPDATE Proxy SET name =:name WHERE idx = :idx")
    void setName(long idx, String name);

    @Query("SELECT * FROM Proxy WHERE parent = 0 AND deleting = 0 AND seeding = 1")
    List<Proxy> getPins();

    @Query("SELECT * FROM Proxy WHERE parent =:parent AND deleting = 0")
    List<Proxy> getChildren(long parent);

    @Query("UPDATE Proxy SET seeding = 1, leaching = 0 WHERE idx = :idx")
    void setDone(long idx);

    @Query("UPDATE Proxy SET cid =:cid, seeding = 1, leaching = 0 WHERE idx = :idx")
    @TypeConverters(Cid.class)
    void setDone(long idx, Cid cid);

    @Query("UPDATE Proxy SET size = :size WHERE idx = :idx")
    void setSize(long idx, long size);

    @Query("UPDATE Proxy SET work = :work WHERE idx = :idx")
    void setWork(long idx, String work);

    @Query("UPDATE Proxy SET work = null WHERE idx = :idx")
    void resetWork(long idx);

    @Query("SELECT * FROM Proxy WHERE deleting = 1 AND lastModified < :time")
    List<Proxy> getDeletedProxies(long time);

    @Query("UPDATE Proxy SET lastModified =:lastModified WHERE idx = :idx")
    void setLastModified(long idx, long lastModified);

    @Delete
    void removeProxy(Proxy proxy);

    @Query("SELECT parent FROM Proxy WHERE idx = :idx")
    long getParent(long idx);

    @Query("SELECT name FROM Proxy WHERE idx = :idx")
    String getName(long idx);

    @Query("UPDATE Proxy SET uri =:uri WHERE idx = :idx")
    void setUri(long idx, String uri);

}
